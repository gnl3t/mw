package com.gnl3t.mw;


import com.gnl3t.mw.util.LogUtil;
import com.gnl3t.mw.util.constants.Const;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;


public class TestFactorial1 {
    final static Logger log = Logger.getLogger(TestFactorial1.class);


    @Before
    public void before() throws Exception {
        DOMConfigurator.configure(Const.LOG4J_PROJECT_ABSOLUTE_PATH);
    }

//    @Ignore
    @Test
    public void t1() {
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());

//        long timeFactorial = calculateFactorial(1005);
        long timeFactorial = calculateFactorial(10, 1005001);
        dbg("timeFactorial=" + timeFactorial);
    }

    public long calculateFactorial(int base, int repeat) {
        long startTime = System.currentTimeMillis();
        for(int i = 1; i <= repeat; i++) {
            calculateFactorial(base);
        }

        return System.currentTimeMillis() - startTime;
    }

    public long calculateFactorial(int base) {
        long startTime = System.currentTimeMillis();
        int result = 1;
        for(int i = 1; i <= base; i++) {
            result = result * i;
//            dbg("i=" + i);
//            dbg("result=" + result);
        }
//        dbg("result=" + result);

        return System.currentTimeMillis() - startTime;
    }

    @Ignore
    @Test
    public void t0() {
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
        dbg("log=" + log);
    }


//==================================================================dbg_print

    private void dbg(String text) {
        log.debug(text);
    }

    private void dbg(Exception e) {
        dbg(LogUtil.buildStackTraceAsString(e));
    }

}
