package com.gnl3t.mw;

import com.gnl3t.mw.arena.udp.client.UdpClient2;
import com.gnl3t.mw.util.Log4jUtil;
import com.gnl3t.mw.util.LogUtil;
import com.gnl3t.mw.util.ThreadUtil;
import com.gnl3t.mw.util.constants.Const;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Date;


public class TestUdpClient2 {
    final static Logger log = Logger.getLogger(TestUdpClient2.class);


    @Before
    public void before() throws Exception {
        DOMConfigurator.configure(Const.LOG4J_PROJECT_ABSOLUTE_PATH);
    }


    @Ignore
    @Test
    public void testClientToClient1(){
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());

//        String host = "localhost";
		String host = "164.90.171.227";
        int port = 9001;
//		log.debug("host");
        try {
            InetAddress serverInetAddress = InetAddress.getByName(host);
            UdpClient2 udpClient2 = new UdpClient2(serverInetAddress, port);
			while(true) {
                udpClient2.sendMessage("Client E " + new Date());
                ThreadUtil.sleep(3000);
			}
        } catch (IOException e) {
            Log4jUtil.logException(log, e);
        }
    }

//    @Ignore
    @Test
    public void testClientToServer1() {
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());

//        String host = "localhost";
		String host = "164.90.171.227";
        int tcpPort = 9000;
        int port = 9001;
//		log.debug("host");
        try {
            InetAddress serverInetAddress = InetAddress.getByName(host);

//            Socket tcpSocket = new Socket(serverInetAddress, tcpPort); //tcp hole punch

            UdpClient2 udpClient2 = new UdpClient2(serverInetAddress, port);
            udpClient2.punchHole(tcpPort);
			while(true) {
//                udpClient2.sendUdpMessage("Client E " + new Date());
                udpClient2.sendMessage("Client E " + new Date());
//                udpClient2.sendUdpMessage("one");
                ThreadUtil.sleep(3000);
			}
        } catch (IOException e) {
            Log4jUtil.logException(log, e);
        }
    }

    @Ignore
    @Test
    public void test0(){
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
    }


//==================================================================dbg_print

    private void dbg(String text) {
        log.debug(text);
    }

    private void dbg(Exception e) {
        dbg(LogUtil.buildStackTraceAsString(e));
    }


}
