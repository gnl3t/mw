package com.gnl3t.mw;


import com.gnl3t.mw.util.LogUtil;
import com.gnl3t.mw.util.constants.Const;
import com.gnl3t.mw.util.constants.StringConst;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Map;
import java.util.TreeMap;


public class TreeMapRequestBuilder1Test {
    final static Logger log = Logger.getLogger(TreeMapRequestBuilder1Test.class);


    @Before
    public void before() throws Exception {
        DOMConfigurator.configure(Const.LOG4J_PROJECT_ABSOLUTE_PATH);
    }

    //    @Ignore
    @Test
    public void t1() {
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());

        TreeMap<String, String> request = new TreeMap<>();
        request.put("nickName", "abc");
        request.put("ping", "0");
        request.put("equipmentTotalSilver", "1500");

        String result = serializeRequest(request);
        dbg("result=" + result);
    }

    public String serializeRequest(TreeMap<String, String> src) {
        StringBuilder result = new StringBuilder();

        for (Map.Entry<String, String> entry : src.entrySet()) {
            dbg("key=" + entry.getKey() + ", value=" + entry.getValue());
            result.append(entry.getKey()).append(StringConst.EQUALS_SYMBOL).append(entry.getValue()).append(StringConst.BACK_SLASH_N);
        }

        return result.toString();
    }

    @Ignore
    @Test
    public void t0() {
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
    }


//==================================================================dbg_print

    private void dbg(String text) {
        log.debug(text);
    }

    private void dbg(Exception e) {
        dbg(LogUtil.buildStackTraceAsString(e));
    }

}
