package com.gnl3t.mw;


import com.gnl3t.mw.arena.http.client.CustomHttpClient;
import com.gnl3t.mw.util.LogUtil;
import com.gnl3t.mw.util.constants.StringConst;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;


public class TestHttpClient {
	private static final Logger log = Logger.getLogger(TestHttpClient.class);
	private static Connection connection;


	@Before
	public void before() throws Exception {
//		DOMConfigurator.configure(Const.LOG4J_PROJECT_ABSOLUTE_PATH);
	}

	@Ignore
	@Test
	public void testHttpURLConnection1() throws Exception {
		//URL url = new URL("http://example.com");
		//URL url = new URL("http://localhost:8000/movie/list/data");
		URL url = new URL("http://localhost:8000/hello/world");
		HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
		httpURLConnection.setRequestMethod("GET");

		BufferedReader in = new BufferedReader(
				new InputStreamReader(httpURLConnection.getInputStream()));
		String inputLine;
		StringBuffer content = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			content.append(inputLine);
		}
		in.close();
		dbg("content=" + content);

		httpURLConnection.disconnect();
	}

//	@Ignore
	@Test
	public void testCommand4() throws Exception {
		CustomHttpClient customHttpClient;
		String request = "type=command\n" +
				"commandName=clearClientAddresses";
		while(true) {
//			customHttpClient = new CustomHttpClient("http://localhost:8000/command", request);
			customHttpClient = new CustomHttpClient();
			String response = customHttpClient.sendRequest("http://localhost:8000/command", request);
			String[] subStrings = response.split(StringConst.BACK_SLASH_N);
			for (String subString : subStrings) {
				dbg(subString);
			}

			Thread.sleep(10000);
			log.debug("keep alive");
		}
	}

	@Ignore
	@Test
	public void testCommand3() throws Exception {
		CustomHttpClient customHttpClient;
		String request = "type=command\n" +
				"commandName=getClientAddresses";
		while(true) {
//			customHttpClient = new CustomHttpClient("http://localhost:8000/command", request);
			customHttpClient = new CustomHttpClient();
			String response = customHttpClient.sendRequest("http://localhost:8000/command", request);
			String[] subStrings = response.split(StringConst.BACK_SLASH_N);
			for (String subString : subStrings) {
				dbg(subString);
			}

			Thread.sleep(10000);
			log.debug("keep alive");
		}
	}

	@Ignore
	@Test
	public void testCommand2() throws Exception {
		CustomHttpClient customHttpClient;
		String request = "type=command\n" +
				"commandName=getClientAddresses";
		while(true) {
			customHttpClient = new CustomHttpClient("http://localhost:8000/command", request);
			Thread.sleep(10000);
			log.debug("keep alive");
		}
	}

	@Ignore
	@Test
	public void testCommand1() throws Exception {
		CustomHttpClient customHttpClient;
		String request = "type=command\n" +
				"commandName=clearQueue";
		while(true) {
			customHttpClient = new CustomHttpClient("http://localhost:8000/command", request);
			Thread.sleep(3000);
			log.debug("keep alive");
		}
	}

	@Ignore
	@Test
	public void testCustomHttpClient1() throws Exception {
		//CustomHttpClient customHttpClient = new CustomHttpClient("http://localhost:8000/movie/list");
		CustomHttpClient customHttpClient;
		while(true) {
			//customHttpClient = new CustomHttpClient("http://localhost:8000/movie/list");
//			customHttpClient = new CustomHttpClient("http://localhost:8000/movie/list/data");
			customHttpClient = new CustomHttpClient("http://localhost:8000/hello/world", "hello");
			Thread.sleep(3000);
			log.debug("keep alive");
		}
	}

	@After
	public void after() throws SQLException {
//		connection.close();
	}


//==================================================================dbg_print

	private void dbg(String text) {
		log.debug(text);
	}

	private void dbg(Exception e) {
		dbg(LogUtil.buildStackTraceAsString(e));
	}


}
