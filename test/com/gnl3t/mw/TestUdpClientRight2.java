package com.gnl3t.mw;


import com.gnl3t.mw.arena.engine.ArenaJFrameEngine;
import com.gnl3t.mw.arena.udp.client.UdpClient;
import com.gnl3t.mw.util.Log4jUtil;
import com.gnl3t.mw.util.LogUtil;
import com.gnl3t.mw.util.ThreadUtil;
import com.gnl3t.mw.util.constants.Const;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.Date;


public class TestUdpClientRight2 {
    final static Logger log = Logger.getLogger(TestUdpClientRight2.class);


    @Before
    public void before() throws Exception {
        DOMConfigurator.configure(Const.LOG4J_PROJECT_ABSOLUTE_PATH);
    }

//    @Ignore
    @Test
    public void test3(){
        dbg("new Date()=" + new Date());

        int sleepTime = ArenaJFrameEngine.localHostFlag ? 50 : 5;
//        String host = "localhost";
//		String host = "164.90.171.227";
        String host = ArenaJFrameEngine.localHostFlag ? "localhost" : "164.90.171.227";
        int tcpPort = 8090;
//        int udpPort = 9001;
        int udpPort = 9051;
        UdpClient udpClient = new UdpClient(host, udpPort);
        try {
            udpClient.punchHole(tcpPort);
            udpClient.connect();
			while(true) {
                udpClient.sendMessage("BEGINplayerRight\n" +
                        "request\n" +
                        "END");
                ThreadUtil.sleep(sleepTime);
			}
        } catch (IOException e) {
            dbg(e);
        }
    }

    @Ignore
    @Test
    public void test0(){
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
    }


//==================================================================dbg_print

    private void dbg(String text) {
        log.debug(text);
    }

    private void dbg(Exception e) {
        dbg(LogUtil.buildStackTraceAsString(e));
    }


}
