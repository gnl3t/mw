package com.gnl3t.mw;

import com.gnl3t.mw.arena.udp.client.UdpClient;
import com.gnl3t.mw.util.Log4jUtil;
import com.gnl3t.mw.util.LogUtil;
import com.gnl3t.mw.util.ThreadUtil;
import com.gnl3t.mw.util.constants.Const;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.Date;


public class TestUdpClientPlayer2Emulator {
    final static Logger log = Logger.getLogger(TestUdpClientPlayer2Emulator.class);


    @Before
    public void before() throws Exception {
        DOMConfigurator.configure(Const.LOG4J_PROJECT_ABSOLUTE_PATH);
    }

//    @Ignore
    @Test
    public void test1(){
//        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
        dbg("new Date()=" + new Date());

        String host = "localhost";
//		String host = "164.90.171.227";
//        int tcpPort = 9000;
        int tcpPort = 8091;
        int port = 9002;
//		log.debug("host");
        UdpClient udpClient = new UdpClient(host, port);
        try {
//            dbg("dbg1");
            udpClient.punchHole(tcpPort);
            udpClient.connect();
			while(true) {
//                dbg("dbg2");
//                udpClient.sendMessages("Client D " + new Date());
//                udpClient.sendMessages("BEGINplayerLeft\n" +
//                        "left2Released\n" +
//                        "END");
                udpClient.sendMessage("BEGINplayerRight\n" +
                        "request\n" +
                        "END");
//                dbg("dbg3");
//                ThreadUtil.sleep(3000);
//                ThreadUtil.sleep(100);
                ThreadUtil.sleep(30);
			}
        } catch (IOException e) {
            Log4jUtil.logException(log, e);
        }
    }

    @Ignore
    @Test
    public void test0(){
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
    }


//==================================================================dbg_print

    private void dbg(String text) {
        log.debug(text);
    }

    private void dbg(Exception e) {
        dbg(LogUtil.buildStackTraceAsString(e));
    }


}
