package com.gnl3t.mw;

import com.gnl3t.mw.util.LogUtil;
import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.sound.sampled.*;


public class TestMp3 {
    final static Logger log = Logger.getLogger(TestMp3.class);


    //TODO - not working because unit test finishes before sound played
    @Ignore
    @Test
    public void test3() throws Exception {
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
        log.debug("Working Directory = " + System.getProperty("user.dir"));

//        com.sun.javafx.application.PlatformImpl.startup(r);
        com.sun.javafx.application.PlatformImpl.startup(()->{});

//        File f = new File("E:\\malayalam good song\\01_ISHTAMANU.MP3");
        File f = new File("D:/work/home/code/idea/mw/shield_hit.mp3");
        Media hit = new Media(f.toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(hit);
        mediaPlayer.play();

        com.sun.javafx.application.PlatformImpl.exit();
    }

    //TODO - not working because unit test finishes before sound played
    @Ignore
    @Test
    public void test2() throws Exception {
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
        log.debug("Working Directory = " + System.getProperty("user.dir"));

//        String bip = "bip.mp3";
//        String bip = "./test_resources/sound/shield_hit.mp3";
//        String bip = "test_resources/sound/shield_hit.mp3";
        String bip = "shield_hit.mp3";
//        String bip = "D:\\work\\home\\code\\idea/resources/sound/shield_hit.mp3";
//        AudioInputStream audioIn = AudioSystem.getAudioInputStream(TestMp3.class.getResource("music.wav"));
//        AudioInputStream audioIn = AudioSystem.getAudioInputStream(TestMp3.class.getResource(bip));
//        AudioInputStream audioIn = AudioSystem.getAudioInputStream(new File(bip).toURI().toURL());

        URL fileURL = new File(bip).toURI().toURL();
        log.debug("fileURL=" + fileURL);
        AudioInputStream audioIn = AudioSystem.getAudioInputStream(fileURL);

        Clip clip = null;
        clip = AudioSystem.getClip();
        clip.open(audioIn);
        clip.start();
    }

    //TODO - not working because unit test finishes before sound played
    @Ignore
    @Test
    public void test1(){
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());

        com.sun.javafx.application.PlatformImpl.startup(()->{});

//        String bip = "bip.mp3";
        String bip = "./resources/sound/shield_hit.mp3";
        Media hit = new Media(new File(bip).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(hit);
        mediaPlayer.play();
        mediaPlayer.play();
        mediaPlayer.play();
        mediaPlayer.play();
        mediaPlayer.play();
        mediaPlayer.play();

        com.sun.javafx.application.PlatformImpl.exit();
    }

    @Ignore
    @Test
    public void testCurrentTimeMillis1() {
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());


        long currentTimeMillis = System.currentTimeMillis();
        long sessionEndMillis = System.currentTimeMillis() + (
                30L * //days
                        24L * //hours
                        3600L * //minutes * seconds
                        1000L //milliseconds
        );
        dbg("currentTimeMillis=" + currentTimeMillis);
        dbg(" sessionEndMillis=" + sessionEndMillis);
        dbg("             diff=" + (sessionEndMillis - currentTimeMillis));
    }


//==================================================================dbg_print

    private void dbg(String text) {
        log.debug(text);
    }

    private void dbg(Exception e) {
        dbg(LogUtil.buildStackTraceAsString(e));
    }


}
