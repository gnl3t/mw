package com.gnl3t.mw;

import com.gnl3t.mw.util.LogUtil;
import com.gnl3t.mw.util.RequestUtils;
import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Map;


public class TestRequestUtils {
    final static Logger log = Logger.getLogger(TestRequestUtils.class);


//    @Ignore
    @Test
    public void test1(){
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());

//        String srcString = "System.currentTimeMillis()=" + System.currentTimeMillis();
        String srcString = "client addresses 2021-09-02T19:28:06.747\n" +
                "udpServer1=/45.132.92.250:53555";
        Map<String, String> result = RequestUtils.getMap(srcString);
        dbg("result.get(\"key\")=" + result.get("key"));
        dbg("result.get(\"System.currentTimeMillis()\"=" + result.get("System.currentTimeMillis()"));


        for (Map.Entry<String, String> entry : result.entrySet()) {
            System.out.println(entry.getKey() + "=" + entry.getValue());
        }
    }

    @Ignore
    @Test
    public void test0(){
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
    }

    @Ignore
    @Test
    public void testCurrentTimeMillis1() {
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());


        long currentTimeMillis = System.currentTimeMillis();
        long sessionEndMillis = System.currentTimeMillis() + (
                30L * //days
                        24L * //hours
                        3600L * //minutes * seconds
                        1000L //milliseconds
        );
        dbg("currentTimeMillis=" + currentTimeMillis);
        dbg(" sessionEndMillis=" + sessionEndMillis);
        dbg("             diff=" + (sessionEndMillis - currentTimeMillis));
    }


//==================================================================dbg_print

    private void dbg(String text) {
        log.debug(text);
    }

    private void dbg(Exception e) {
        dbg(LogUtil.buildStackTraceAsString(e));
    }


}
