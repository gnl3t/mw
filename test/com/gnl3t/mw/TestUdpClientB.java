package com.gnl3t.mw;

import com.gnl3t.mw.arena.engine.ArenaJFrameEngine;
import com.gnl3t.mw.arena.http.client.CustomHttpClient;
import com.gnl3t.mw.arena.udp.client.UdpClient;
import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.*;
import com.gnl3t.mw.util.constants.Const;
import com.gnl3t.mw.util.constants.StringConst;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Properties;


public class TestUdpClientB {
    final static Logger log = Logger.getLogger(TestUdpClientB.class);


    @Before
    public void before() throws Exception {
        DOMConfigurator.configure(Const.LOG4J_PROJECT_ABSOLUTE_PATH);
    }

//    @Ignore
    @Test
    public void test1() throws Exception {
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());

        Properties config = PropertiesUtil.readConfig();
        Environment env = new Environment(config);
        env.gameControlsData.playerLeft = false;

        boolean receivedOtherClientAddressFlag = false;
//		String host = "localhost";
//		String host = "164.90.171.227";
        String host = ArenaJFrameEngine.localHostFlag ? "localhost" : "164.90.171.227";
//		int tcpPort = 8090;
//		int port = 9001;
        int tcpPort = env.gameControlsData.playerLeft ? 8090 : 8091;
        int port = env.gameControlsData.playerLeft ? 9001 : 9002;
        UdpClient udpClient = new UdpClient(host, port, env);
        udpClient.punchHole(tcpPort);
        udpClient.connect();
        udpClient.sendMessage("BEGINplayerLeft\n" +
                "getClientAddress\n" +
                "clientId=ClientB\n" +
                "END");

//			while(true) {
//				String message = udpClient.getSocketMessage();
//				log.debug("message=" + message);
//			}
//			String opponentId = "udpServer1";
        String opponentId = "ClientA";
        while(!receivedOtherClientAddressFlag) {
            String message = udpClient.getSocketMessage();
            log.debug("message=" + message);

            String opponentAddress = getOtherClientAddress(opponentId);
            if(opponentAddress == null) {
                ThreadUtil.sleep(5000);
            } else {
                dbg("opponentAddress=" + opponentAddress);
                if(opponentAddress == null) return;
                String[] subStrings = opponentAddress.split(StringConst.COLON_SYMBOL);
//                String opponentHost = subStrings[0].substring(opponentId.length() + 2);
                String opponentHost = subStrings[0].substring(1);
                String opponentPort = subStrings[1];
                dbg("opponentHost=" + opponentHost);
                dbg("opponentPort=" + opponentPort);
                udpClient.setWorkingSocketAddress(opponentHost, Integer.parseInt(opponentPort));
                receivedOtherClientAddressFlag = true;

//                startClient2(env, opponentHost, opponentPort);
            }
        }

        int counter = 0;
        while(true) {
            counter++;
            if(counter > 3) udpClient.checkResponsesFlag = true;
//            udpClient.serverKeepAlive();
            udpClient.sendMessage("BEGINClientB\n" +
                    "request\n" +
                    "END");
            ThreadUtil.sleep(5000);
        }


//        udpClient.startServer();
    }

//    private void startClient2(Environment env, String host, String port) throws Exception {
//        UdpClient udpClient = new UdpClient(host, Integer.parseInt(port), env);
//        udpClient.connect();
//
//        while(true) {
////            udpClient.serverKeepAlive();
//            udpClient.sendMessage("BEGINClientB\n" +
//                    "request\n" +
//                    "END");
//            ThreadUtil.sleep(5000);
//        }
//    }

    private String getOtherClientAddress(String opponentId) throws Exception {
        String response = httpRequestOtherClientAddress();
        Map<String, String> resultMap = RequestUtils.getMap(response);

        return resultMap.get(opponentId);
    }

    private String httpRequestOtherClientAddress() throws Exception {
        String httpString = ArenaJFrameEngine.localHostFlag ? "http://localhost:8000/command" : "http://164.90.171.227:8000/command";
//        String httpString = "http://localhost:8000/command";
//        String httpString = "http://164.90.171.227:8000/command";

        CustomHttpClient customHttpClient;
        String request = "type=command\n" +
                "commandName=getClientAddresses";
        customHttpClient = new CustomHttpClient();
//        String response = customHttpClient.sendRequest("http://localhost:8000/command", request);
        return customHttpClient.sendRequest(httpString, request);
    }

    @Ignore
    @Test
    public void test0(){
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
    }


//==================================================================dbg_print

    private void dbg(String text) {
        log.debug(text);
    }

    private void dbg(Exception e) {
        dbg(LogUtil.buildStackTraceAsString(e));
    }

}
