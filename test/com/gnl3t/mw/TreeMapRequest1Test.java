package com.gnl3t.mw;


import com.gnl3t.mw.arena.engine.LogEngine;
import com.gnl3t.mw.util.LogUtil;
import com.gnl3t.mw.util.RequestUtils;
import com.gnl3t.mw.util.constants.StringConst;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Map;
import java.util.TreeMap;


public class TreeMapRequest1Test {
    private final static LogEngine log = new LogEngine(TreeMapRequest1Test.class);


    @Before
    public void before() throws Exception {
        //DOMConfigurator.configure(Const.LOG4J_PROJECT_ABSOLUTE_PATH);
    }

//    @Ignore
    @Test
    public void t2() {
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());

//        String request = "type=command\n" +
//                "commandName=getOpponentInfo";
        String request = "duelHandlerHost=localhost\n" +
                "duelHandlerTcpPort1=8090\n" +
                "duelHandlerTcpPort2=8091\n" +
                "duelHandlerUdpPort1=9053\n" +
                "duelHandlerUdpPort2=9054\n" +
                "foundOpponent=true\n" +
                "opponentInfo=armorLevelType=GREY1\n" +
                "armorType=SWORDSMAN\n" +
                "commandName=getOpponentInfo\n" +
                "equipmentTotalCost=1500\n" +
                "helmetLevelType=GREY1\n" +
                "helmetType=SWORDSMAN\n" +
                "nFactorial=3000\n" +
                "nickName=abc\n" +
                "ping=123456\n" +
                "shieldLevelType=GREY1\n" +
                "shieldType=SWORDSMAN\n" +
                "type=command\n" +
                "weaponLevelType=GREY1\n" +
                "weaponType=SWORDSMAN\n" +
                "\n" +
                "playerLeft=true\n" +
                "repeatAfterMillis=3000\n" +
                "result=correct request";
        dbg("request=\n" + request);

        TreeMap<String, String> result = RequestUtils.parseToTreeMap(request);

        for (Map.Entry<String, String> entry : result.entrySet()) {
//            dbg("key=" + entry.getKey() + ", value=" + entry.getValue());
            dbg(entry.getKey() + "=" + entry.getValue());
        }
//        String nokeyValue = result.get("nokey");
//        dbg("nokeyValue=" + nokeyValue);
        dbg("result.get(\"nokey\")=" + result.get("nokey"));
        dbg("result.get(\"type\")=" + result.get("type"));
    }

    @Ignore
    @Test
    public void t1() {
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());

        String request = "type=command\n" +
                "commandName=getOpponentInfo";
        dbg("request=\n" + request);

        TreeMap<String, String> result = parseToTreeMap(request);

        for (Map.Entry<String, String> entry : result.entrySet()) {
            dbg("key=" + entry.getKey() + ", value=" + entry.getValue());
        }
//        String nokeyValue = result.get("nokey");
//        dbg("nokeyValue=" + nokeyValue);
        dbg("result.get(\"nokey\")=" + result.get("nokey"));
        dbg("result.get(\"type\")=" + result.get("type"));
    }

    private TreeMap<String, String> parseToTreeMap(String src) {
        TreeMap<String, String> result = new TreeMap<>();

        String[] subStrings = src.split(StringConst.BACK_SLASH_N);
        for (String subString : subStrings) {
            String[] values = subString.split(StringConst.EQUALS_SYMBOL);
            if(values.length > 0) result.put(values[0], values[1]);
        }

        return result;
    }

    @Ignore
    @Test
    public void t0(){
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
    }


//==================================================================dbg_print

    private void dbg(String text) {
        log.debug(text);
    }

    private void dbg(Exception e) {
        dbg(LogUtil.buildStackTraceAsString(e));
    }

}
