package com.gnl3t.mw;


import com.gnl3t.mw.util.LogUtil;
import com.gnl3t.mw.util.constants.Const;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;


public class TestMisc {
    final static Logger log = Logger.getLogger(TestMisc.class);


    @Before
    public void before() throws Exception {
        DOMConfigurator.configure(Const.LOG4J_PROJECT_ABSOLUTE_PATH);
    }

//    @Ignore
    @Test
    public void t1() {
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
    }

    @Ignore
    @Test
    public void t0() {
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
        dbg("log=" + log);
    }


//==================================================================dbg_print

    private void dbg(String text) {
        log.debug(text);
    }

    private void dbg(Exception e) {
        dbg(LogUtil.buildStackTraceAsString(e));
    }

}
