package com.gnl3t.mw;


import com.gnl3t.mw.util.LogUtil;
import com.gnl3t.mw.util.constants.Const;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


public class TestReadFile {
	private static final Logger log = Logger.getLogger(TestReadFile.class);


	@Before
	public void before() {
		DOMConfigurator.configure(Const.LOG4J_PROJECT_ABSOLUTE_PATH);
	}


//	@Ignore
	@Test
	public void testMismatchOnly1() throws IOException {
		dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
		String relativePath = "./output_files/";


		String filePathString = relativePath + "2021-08-10T18_36_12.089Z_right" + ".text";
		byte[] encoded = Files.readAllBytes(Paths.get(filePathString));
		String content = new String(encoded, StandardCharsets.UTF_8);
		String[] subStrings = content.split("response=");

		filePathString = relativePath + "2021-08-10T18_36_12.494Z_left" + ".text";
		encoded = Files.readAllBytes(Paths.get(filePathString));
		content = new String(encoded, StandardCharsets.UTF_8);
		String[] subStrings2 = content.split("response=");

		for(int i = 0; i < subStrings.length; i++) {
			if(!subStrings[i].equalsIgnoreCase(subStrings2[i])) {
				dbg("mismatch");
				dbg("subString=" + subStrings[i]);
				dbg("subString2=" + subStrings2[i]);
			}
		}
	}

	@Ignore
	@Test
	public void test1() throws IOException {
		dbg("System.currentTimeMillis()=" + System.currentTimeMillis());

		String relativePath = "./output_files/";
		String filePathString = relativePath + "2021-08-09T18_12_18.168Z_right" + ".text";
//		String filePathString = relativePath + "2021-08-08T14_39_42.374Z_left" + ".text";
		dbg("filePathString=" + filePathString);
//		Path filePath = Paths.get(filePathString);
//		String content = Files.readString(filePath, StandardCharsets.UTF_8);

		byte[] encoded = Files.readAllBytes(Paths.get(filePathString));
		String content = new String(encoded, StandardCharsets.UTF_8);
//		dbg("content=" + content);
		String[] subStrings = content.split("response=");

//		for (String subString : subStrings) {
//			dbg("subString=" + subString);
//		}

		filePathString = relativePath + "2021-08-09T18_12_18.729Z_left" + ".text";
		encoded = Files.readAllBytes(Paths.get(filePathString));
		content = new String(encoded, StandardCharsets.UTF_8);
		String[] subStrings2 = content.split("response=");

		for(int i = 0; i < subStrings.length; i++) {
//			dbg("subString=" + subStrings[i]);
//			dbg("subString2=" + subStrings2[i]);
			if(!subStrings[i].equalsIgnoreCase(subStrings2[i])) {
				dbg("mismatch");
			}
			dbg("right=" + subStrings[i]);
			dbg("left=" + subStrings2[i]);
		}
	}

	@Ignore
	@Test
	public void testCurrentTimeMillis1() {
		dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
	}

	@After
	public void after() throws SQLException {
//		connection.close();
	}


//==================================================================dbg_print

	private void dbg(String text) {
		log.debug(text);
	}

	private void dbg(Exception e) {
		dbg(LogUtil.buildStackTraceAsString(e));
	}


}
