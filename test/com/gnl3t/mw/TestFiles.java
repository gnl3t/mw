package com.gnl3t.mw;


import com.gnl3t.mw.util.LogUtil;
import com.gnl3t.mw.util.constants.Const;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


public class TestFiles {
	private static final Logger log = Logger.getLogger(TestFiles.class);


	@Before
	public void before() {
		DOMConfigurator.configure(Const.LOG4J_PROJECT_ABSOLUTE_PATH);
	}

//	@Ignore
	@Test
	public void testCreateFile3() throws IOException {
		//dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
		long currentTimeMillis = System.currentTimeMillis();
		dbg("currentTimeMillis=" + currentTimeMillis);
//		dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
//		dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
//		dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
//		dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
//		dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
//		dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
//		dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
//		dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
//		dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
//		dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
//		dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
//		dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
//		dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
//		dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
//		dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
//		dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
//		dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
//		dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
//		String dateString = ("" + new Date()).replaceAll(":", "_");
		String dateString = Instant.now().toString().replaceAll(":", "_");
		dbg("dateString=" + dateString);


		String relativePath = "./output_files/";
		String filePathString = relativePath + "test2_" + dateString + ".text";
		dbg("filePathString=" + filePathString);
		List<String> lines = Arrays.asList("hello", "world");
		Path filePath = Paths.get(filePathString);
		Files.write(filePath, lines, StandardCharsets.UTF_8);

		Files.write(filePath, "dbg\n".getBytes(), StandardOpenOption.APPEND);
		Files.write(filePath, "dbg2\n".getBytes(), StandardOpenOption.APPEND);
		Files.write(filePath, "dbg3\n".getBytes(), StandardOpenOption.APPEND);
		Files.write(filePath, (Instant.now().toString() + ": dbg4\n").getBytes(), StandardOpenOption.APPEND);
	}

	@Ignore
	@Test
	public void testCreateFile2() throws IOException {
		//dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
		long currentTimeMillis = System.currentTimeMillis();
		dbg("currentTimeMillis=" + currentTimeMillis);
		String dateString = ("" + new Date()).replaceAll(":", "_");
		dbg("dateString=" + dateString);


		String relativePath = "./output_files/";
		String filePathString = relativePath + "test2_" + dateString + ".text";
		dbg("filePathString=" + filePathString);
		List<String> lines = Arrays.asList("hello", "world");
		Path filePath = Paths.get(filePathString);
		Files.write(filePath, lines, StandardCharsets.UTF_8);

		Files.write(filePath, "dbg\n".getBytes(), StandardOpenOption.APPEND);
		Files.write(filePath, "dbg2\n".getBytes(), StandardOpenOption.APPEND);
		Files.write(filePath, "dbg3\n".getBytes(), StandardOpenOption.APPEND);
	}

	@Ignore
	@Test
	public void testCreateFile1() throws IOException {
		//dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
		long currentTimeMillis = System.currentTimeMillis();
		dbg("currentTimeMillis=" + currentTimeMillis);


		//TODO - move this code to java class and call its methods from here
		String daoRelativePath = "./src/";
		daoRelativePath += "com.gnl3t.code.generator.dao".replaceAll("\\.", "/") + "/";
		dbg("daoRelativePath=" + daoRelativePath);
		//String filePath = daoRelativePath + "hello_world.java";
		String filePath = daoRelativePath + "HelloWorldDAO" + currentTimeMillis + ".java";
		dbg("filePath=" + filePath);
		List<String> lines = Arrays.asList("hello", "world");
		Path file = Paths.get(filePath);
		Files.write(file, lines, StandardCharsets.UTF_8);

		//TODO - need connect to DB and get schema, create model, create db util
	}

	@Ignore
	@Test
	public void testCurrentTimeMillis1() {
		dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
	}

	@After
	public void after() throws SQLException {
//		connection.close();
	}


//==================================================================dbg_print

	private void dbg(String text) {
		log.debug(text);
	}

	private void dbg(Exception e) {
		dbg(LogUtil.buildStackTraceAsString(e));
	}


}
