package com.gnl3t.mw;

import com.gnl3t.mw.arena.udp.client.UdpClient;
import com.gnl3t.mw.util.Log4jUtil;
import com.gnl3t.mw.util.LogUtil;
import com.gnl3t.mw.util.ThreadUtil;
import com.gnl3t.mw.util.constants.Const;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.Date;


public class TestUdpClient {
    final static Logger log = Logger.getLogger(TestUdpClient.class);


    @Before
    public void before() throws Exception {
        DOMConfigurator.configure(Const.LOG4J_PROJECT_ABSOLUTE_PATH);
    }

    @Ignore
    @Test
    public void test4(){
//        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
        dbg("new Date()=" + new Date());

//        String host = "localhost";
		String host = "164.90.171.227";
//        int tcpPort = 9000;
//        int tcpPort = 8090;
        int tcpPort = 8051;
        int port = 9001;
//		log.debug("host");
        UdpClient udpClient = new UdpClient(host, port);
        try {
//            dbg("dbg1");
            udpClient.punchHole(tcpPort);
            udpClient.connect();
//                dbg("dbg2");
//                udpClient.sendMessages("Client D " + new Date());
//                udpClient.sendMessage("BEGINplayerLeft\n" +
//                        "left2Released\n" +
//                        "END");
            udpClient.sendMessage("BEGINplayerLeft\n" +
                    "request\n" +
                    "END");
			while(true) {

//                String received = udpClient.getUdpMessage();
                String received = udpClient.getSocketMessage();
                if(received.length() != 0) {
                    dbg("received='" + received + "'");
                }

//                dbg("dbg3");
//                ThreadUtil.sleep(3000);
//                ThreadUtil.sleep(1000);
			}
        } catch (IOException e) {
            Log4jUtil.logException(log, e);
        }
    }

//    @Ignore
    @Test
    public void test3(){
//        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
        dbg("new Date()=" + new Date());

        String host = "localhost";
//		String host = "164.90.171.227";
//        int tcpPort = 9000;
        int tcpPort = 8090;
        int port = 9001;
//		log.debug("host");
        UdpClient udpClient = new UdpClient(host, port);
        try {
//            dbg("dbg1");
            udpClient.punchHole(tcpPort);
            udpClient.connect();
            udpClient.sendMessage("BEGINplayerLeft\n" +
                    "getClientAddress\n" +
                    "clientId=left2\n" +
                    "END");
			while(true) {
//                dbg("dbg2");
//                udpClient.sendMessages("Client D " + new Date());
//                udpClient.sendMessage("BEGINplayerLeft\n" +
//                        "left2Released\n" +
//                        "END");
                udpClient.sendMessage("BEGINplayerLeft\n" +
                        "request\n" +
                        "END");

                String received = udpClient.getUdpMessage();
                dbg("received=" + received);
//                dbg("dbg3");
//                ThreadUtil.sleep(3000);
                ThreadUtil.sleep(10000);
			}
        } catch (IOException e) {
            Log4jUtil.logException(log, e);
        }
    }

    @Ignore
    @Test
    public void test2(){
//        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
        dbg("new Date()=" + new Date());

//        String host = "localhost";
		String host = "45.132.92.244";
        int tcpPort = 9000;
        int port = 60116;
//		log.debug("host");
        UdpClient udpClient = new UdpClient(host, port);
        try {
            dbg("dbg1");
            udpClient.punchHole(tcpPort);
            udpClient.connect();
			while(true) {
                dbg("dbg2");
                udpClient.sendMessage("Client C " + new Date());
                dbg("dbg3");
                ThreadUtil.sleep(3000);
			}
        } catch (IOException e) {
            Log4jUtil.logException(log, e);
        }
    }

    @Ignore
    @Test
    public void test1(){
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());

//        String host = "localhost";
		String host = "164.90.171.227";
        int port = 9001;
//		log.debug("host");
        UdpClient udpClient = new UdpClient(host, port);
        try {
            udpClient.connect();
			while(true) {
                udpClient.sendMessage("hello world mw1");
                ThreadUtil.sleep(3000);
			}
        } catch (IOException e) {
            Log4jUtil.logException(log, e);
        }
    }

    @Ignore
    @Test
    public void test0(){
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
    }


//==================================================================dbg_print

    private void dbg(String text) {
        log.debug(text);
    }

    private void dbg(Exception e) {
        dbg(LogUtil.buildStackTraceAsString(e));
    }


}
