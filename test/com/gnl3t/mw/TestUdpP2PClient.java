package com.gnl3t.mw;

import com.gnl3t.mw.arena.engine.ArenaJFrameEngine;
import com.gnl3t.mw.arena.http.client.CustomHttpClient;
import com.gnl3t.mw.arena.udp.client.UdpClient;
import com.gnl3t.mw.util.Log4jUtil;
import com.gnl3t.mw.util.LogUtil;
import com.gnl3t.mw.util.ThreadUtil;
import com.gnl3t.mw.util.constants.Const;
import com.gnl3t.mw.util.constants.StringConst;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.Date;


public class TestUdpP2PClient {
    final static Logger log = Logger.getLogger(TestUdpP2PClient.class);


    @Before
    public void before() throws Exception {
        DOMConfigurator.configure(Const.LOG4J_PROJECT_ABSOLUTE_PATH);
    }

//    @Ignore
    @Test
    public void test4() throws IOException {
//        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
        dbg("new Date()=" + new Date());


        String opponentId = "udpServer1";
        String httpString = ArenaJFrameEngine.localHostFlag ? "http://localhost:8000/command" : "http://164.90.171.227:8000/command";
//        String httpString = "http://localhost:8000/command";
//        String httpString = "http://164.90.171.227:8000/command";

        CustomHttpClient customHttpClient;
        String request = "type=command\n" +
                "commandName=getClientAddresses";
        customHttpClient = new CustomHttpClient();
//        String response = customHttpClient.sendRequest("http://localhost:8000/command", request);
        String response = customHttpClient.sendRequest(httpString, request);
//        String[] subStrings = response.split(StringConst.BACK_SLASH_N);
//        for (String subString : subStrings) {
//            dbg(subString);
//        }
        String opponentAddress = getOpponentAddress(opponentId, response);
        dbg("opponentAddress=" + opponentAddress);
        if(opponentAddress == null) return;
        String[] subStrings = opponentAddress.split(StringConst.COLON_SYMBOL);
        String opponentHost = subStrings[0].substring(opponentId.length() + 2);
        String opponentPort = subStrings[1];
        dbg("opponentHost=" + opponentHost);
        dbg("opponentPort=" + opponentPort);

//        String host = "localhost";
        String host = opponentHost;
//        String host = "127.0.0.1";
//		String host = "164.90.171.227";
//		String host = "45.132.92.111";
//		String host = "194.50.254.1";
//        int tcpPort = 9000;
        int tcpPort = 8090;
//        int port = 9001;
//        int port = 52582;
        int port = Integer.parseInt(opponentPort);
//		log.debug("host");
        UdpClient udpClient = new UdpClient(host, port);
        try {
//            udpClient.punchHole(tcpPort);
            udpClient.punchHole("164.90.171.227", 8091);
            udpClient.connect();
			while(true) {
                dbg("try send");
//                udpClient.sendMessages("Client D " + new Date());
//                udpClient.sendMessage("BEGINplayerLeft\n" +
//                        "left2Released\n" +
//                        "END");
                udpClient.sendMessage("BEGINplayerLeft\n" +
                        "request\n" +
                        "END");


                ThreadUtil.sleep(1000);
			}
        } catch (IOException e) {
            Log4jUtil.logException(log, e);
        }
    }


    public String getOpponentAddress(String opponentId, String addressesString) {
        String result = null;
        String[] subStrings = addressesString.split(StringConst.BACK_SLASH_N);
        for (String subString : subStrings) {
            dbg(subString);
            if(subString.contains(opponentId)) {
                result = subString;
            }
        }
        return result;
    }

    @Ignore
    @Test
    public void test3(){
//        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
        dbg("new Date()=" + new Date());

//        String host = "localhost";
//        String host = "127.0.0.1";
//		String host = "164.90.171.227";
//		String host = "45.132.92.111";
		String host = "194.50.254.1";
//        int tcpPort = 9000;
        int tcpPort = 8090;
//        int port = 9001;
        int port = 52582;
//		log.debug("host");
        UdpClient udpClient = new UdpClient(host, port);
        try {
//            dbg("dbg1");
//            udpClient.punchHole(tcpPort);
            udpClient.punchHole("164.90.171.227", 8091);
            udpClient.connect();
			while(true) {
//                dbg("dbg2");
//                udpClient.sendMessages("Client D " + new Date());
//                udpClient.sendMessage("BEGINplayerLeft\n" +
//                        "left2Released\n" +
//                        "END");
                udpClient.sendMessage("BEGINplayerLeft\n" +
                        "request\n" +
                        "END");



//                String received = udpClient.getUdpMessage();
//                dbg("received=" + received);
//                dbg("dbg3");
//                ThreadUtil.sleep(3000);
                ThreadUtil.sleep(5000);
			}
        } catch (IOException e) {
            Log4jUtil.logException(log, e);
        }
    }

    @Ignore
    @Test
    public void test2(){
//        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
        dbg("new Date()=" + new Date());

//        String host = "localhost";
		String host = "45.132.92.244";
        int tcpPort = 9000;
        int port = 60116;
//		log.debug("host");
        UdpClient udpClient = new UdpClient(host, port);
        try {
            dbg("dbg1");
            udpClient.punchHole(tcpPort);
            udpClient.connect();
			while(true) {
                dbg("dbg2");
                udpClient.sendMessage("Client C " + new Date());
                dbg("dbg3");
                ThreadUtil.sleep(3000);
			}
        } catch (IOException e) {
            Log4jUtil.logException(log, e);
        }
    }

    @Ignore
    @Test
    public void test1(){
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());

//        String host = "localhost";
		String host = "164.90.171.227";
        int port = 9001;
//		log.debug("host");
        UdpClient udpClient = new UdpClient(host, port);
        try {
            udpClient.connect();
			while(true) {
                udpClient.sendMessage("hello world mw1");
                ThreadUtil.sleep(3000);
			}
        } catch (IOException e) {
            Log4jUtil.logException(log, e);
        }
    }

    @Ignore
    @Test
    public void test0(){
        dbg("System.currentTimeMillis()=" + System.currentTimeMillis());
    }


//==================================================================dbg_print

    private void dbg(String text) {
        log.debug(text);
    }

    private void dbg(Exception e) {
        dbg(LogUtil.buildStackTraceAsString(e));
    }


}
