package com.gnl3t.mw.data.construct;


import com.gnl3t.mw.environment.Environment;

public class BasicData {
	public Environment env;
	public boolean revertedFlag;

	public int x;
	public int y;
	public int width;
	public int height;

	  
  public void resetPrimitives() {
	  x = 0;
	  y = 0;
	  width = 0;
	  height = 0;
  }

	public String getDebugPrint() {
		String debugPrint = "";
		debugPrint += "\n" + "x=" + x;
		debugPrint += "\n" + "y=" + y;
		debugPrint += "\n" + "width=" + width;
		debugPrint += "\n" + "height=" + height;
		return debugPrint;
	}
  
}
