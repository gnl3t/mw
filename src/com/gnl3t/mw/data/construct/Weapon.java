package com.gnl3t.mw.data.construct;


import java.awt.geom.*;

import com.gnl3t.mw.arena.engine.ArenaJFrameEngine;
import com.gnl3t.mw.util.constants.ArenaConst;
import org.apache.log4j.Logger;


public class Weapon extends BasicData {
	final static Logger logger = Logger.getLogger(Weapon.class);
	
	private Rectangle2D.Double weaponRectangle = new Rectangle2D.Double();
	
	public int weaponRectangleWidthStartValue;
	public int weaponRectangleWidth;
	
	public int weaponRectangleHeightStartValue;
	public int weaponRectangleHeight;
	
	public int weaponRectangleAttackExtendWidth = 5;
//	public int damageStartValue = 10;
//	public int damageStartValue = 1;
	public int damageStartValue = ArenaJFrameEngine.reduceDamageFlag ? 1 : 10;
	public int damage = damageStartValue;
	  
	
	public Weapon(Body body) {
		logger.trace("---");
		this.env = body.env;
		this.revertedFlag = body.revertedFlag;
		
		weaponRectangleWidthStartValue = body.halfBodyRectangleWidthStartValue;
		weaponRectangleWidth = body.halfBodyRectangleWidthStartValue;
		
		weaponRectangleHeightStartValue = body.halfBodyRectangleHeightStartValue;
		weaponRectangleHeight = body.halfBodyRectangleHeightStartValue;

		y = ArenaConst.SCENE_LEVEL;
		height = weaponRectangleHeight;
	}

	public Rectangle2D.Double getWeaponRectangle() {
		weaponRectangle.setFrame(x, y, width, height);
		return weaponRectangle;
	}
}
