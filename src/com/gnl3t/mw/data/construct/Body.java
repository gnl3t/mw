package com.gnl3t.mw.data.construct;


import java.awt.geom.*;

import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.constants.ArenaConst;
import org.apache.log4j.Logger;


public class Body extends BasicData {
	final static Logger logger = Logger.getLogger(Body.class);

	private Rectangle2D.Double bodyRectangle = new Rectangle2D.Double();
	

	//public int bodyRectangleWidthStartValue = 50;
//	public int bodyRectangleWidthStartValue = 16;
	public int bodyRectangleWidthStartValue = 60;
	public int bodyRectangleWidth = bodyRectangleWidthStartValue;
	
	public int bodyRectangleHeightStartValue = 50;
	public int bodyRectangleHeight = bodyRectangleHeightStartValue;
	
	public int halfBodyRectangleWidthStartValue = bodyRectangleWidthStartValue / 2;
	public int halfBodyRectangleHeightStartValue = bodyRectangleHeightStartValue / 2;
	  
	
	public Body(boolean revertedFlag, Environment env) {
		logger.trace("---");
		this.env= env;
		this.revertedFlag = revertedFlag;
		
		y = ArenaConst.SCENE_LEVEL;
//		width = bodyRectangleWidth;
		width = halfBodyRectangleWidthStartValue;
		height = bodyRectangleHeight;
	}

	public Rectangle2D.Double getBodyRectangle() {
		bodyRectangle.setFrame(x, y, width, height);
		return bodyRectangle;
	}
}
