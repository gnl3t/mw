package com.gnl3t.mw.data.construct;


import java.awt.geom.*;

import com.gnl3t.mw.util.ArenaScaleUtil;
import com.gnl3t.mw.util.constants.ArenaConst;
import org.apache.log4j.Logger;


public class HealthPointsBar extends BasicData {
	final static Logger logger = Logger.getLogger(HealthPointsBar.class);
	
	private Rectangle2D.Double healthPointsBar = new Rectangle2D.Double();
	private Rectangle2D.Double healthPointsBarScaled = new Rectangle2D.Double();
	private Rectangle2D.Double healthPointsBarBounds = new Rectangle2D.Double();

	public int healthPointsStartValue = 144;
	public int healthPoints = healthPointsStartValue;
	public int healthPointsRegenSpeed = 1;

	private boolean revertedFlag;


	public HealthPointsBar(Body body) {
		logger.trace("---");
		this.env = body.env;
		this.revertedFlag = body.revertedFlag;
		if(!revertedFlag) {
			x = 3;
			y = ArenaConst.HEALTH_POINTS_BAR_LEVEL;
			width = healthPoints;
			height = ArenaConst.HEALTH_POINTS_BAR_HEIGHT;
		} else {
			x = ArenaConst.ARENA_WIDTH - healthPoints - 12;
			y = ArenaConst.HEALTH_POINTS_BAR_LEVEL;
			width = healthPoints;
			height = ArenaConst.HEALTH_POINTS_BAR_HEIGHT;
		}
		healthPointsBarBounds.setFrame(x, y, width, height);
		healthPointsBarBounds = ArenaScaleUtil.reScale(healthPointsBarBounds, env);
	}

	public void reCalcData() {
		if(!revertedFlag) {
			x = 3;
			y = ArenaConst.HEALTH_POINTS_BAR_LEVEL;
			width = healthPoints;
			height = ArenaConst.HEALTH_POINTS_BAR_HEIGHT;
		} else {
			x = ArenaConst.ARENA_WIDTH - healthPoints - 12;
			y = ArenaConst.HEALTH_POINTS_BAR_LEVEL;
			width = healthPoints;
			height = ArenaConst.HEALTH_POINTS_BAR_HEIGHT;
		}
	}

	public Rectangle2D.Double getHealthPointsBar() {
		reCalcData();
		healthPointsBar.setFrame(x, y, width, height);
		return healthPointsBar;
	}

	public Rectangle2D.Double getHealthPointsBarScaled() {
		reCalcData();
		healthPointsBarScaled.setFrame(x, y, width, height);
		return ArenaScaleUtil.reScale(healthPointsBarScaled, env);
	}

	public Rectangle2D.Double getHealthPointsBarBoundsScaled() {
		return healthPointsBarBounds;
	}
	
}
