package com.gnl3t.mw.data.construct;


import java.awt.geom.*;

import com.gnl3t.mw.util.ArenaScaleUtil;
import com.gnl3t.mw.util.constants.ArenaConst;
import org.apache.log4j.Logger;


public class StaminaBar extends BasicData {
	final static Logger logger = Logger.getLogger(StaminaBar.class);
	
	private Rectangle2D.Double staminaBar = new Rectangle2D.Double();
	private Rectangle2D.Double staminaBarScaled = new Rectangle2D.Double();
	private Rectangle2D.Double staminaBarBounds = new Rectangle2D.Double();

	public int staminaPointsStartValue = 144;
	public int staminaPoints = staminaPointsStartValue;
	public int staminaPointsRegenSpeed = 20;
	
	public int staminaLossStartValue = 20;
	public int staminaLoss = staminaLossStartValue;

	private boolean revertedFlag;
	  

	public StaminaBar(Body body) {
		logger.trace("---");
		this.env = body.env;
		this.revertedFlag = body.revertedFlag;

		if(!revertedFlag){
			x = 3;
			y = ArenaConst.STAMINA_BAR_LEVEL;
			width = staminaPoints;
			height = ArenaConst.STAMINA_BAR_HEIGHT;
		} else {
			x = ArenaConst.ARENA_WIDTH - staminaPoints - 12;
			y = ArenaConst.STAMINA_BAR_LEVEL;
			width = staminaPoints;
			height = ArenaConst.STAMINA_BAR_HEIGHT;
		}
		staminaBarBounds.setFrame(x, y, width, height);
		staminaBarBounds = ArenaScaleUtil.reScale(staminaBarBounds, env);
	}

	public void reCalcData() {
		if(!revertedFlag){
			x = 3;
			y = ArenaConst.STAMINA_BAR_LEVEL;
			width = staminaPoints;
			height = ArenaConst.STAMINA_BAR_HEIGHT;
		} else {
			x = ArenaConst.ARENA_WIDTH - staminaPoints - 12;
			y = ArenaConst.STAMINA_BAR_LEVEL;
			width = staminaPoints;
			height = ArenaConst.STAMINA_BAR_HEIGHT;
		}
	}

	public Rectangle2D.Double getStaminaBar() {
		reCalcData();
		staminaBar.setFrame(x, y, width, height);
		return staminaBar;
	}

	public Rectangle2D.Double getStaminaBarScaled() {
		reCalcData();
		staminaBarScaled.setFrame(x, y, width, height);
		return ArenaScaleUtil.reScale(staminaBarScaled, env);
	}

	public Rectangle2D.Double getStaminaBarBoundsScaled() {
		return staminaBarBounds;
	}
	
}
