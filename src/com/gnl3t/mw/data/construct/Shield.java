package com.gnl3t.mw.data.construct;


import java.awt.geom.*;

import com.gnl3t.mw.util.constants.ArenaConst;
import org.apache.log4j.Logger;


public class Shield extends BasicData {
	final static Logger logger = Logger.getLogger(Shield.class);

	private Rectangle2D.Double shieldRectangle = new Rectangle2D.Double();

	public int shieldRectangleWidthStartValue;
	public int shieldRectangleWidth;
	
	public int shieldRectangleHeightStartValue;
	public int shieldRectangleHeight;

	public int armor = ArenaConst.SHIELD_ARMOR;
	  
	
	public Shield(Body body) {
		logger.trace("---");
		this.env = body.env;
		this.revertedFlag = body.revertedFlag;

		shieldRectangleWidthStartValue = body.halfBodyRectangleWidthStartValue;
		shieldRectangleWidth = body.halfBodyRectangleWidthStartValue;
		
		shieldRectangleHeightStartValue = body.halfBodyRectangleHeightStartValue;
		shieldRectangleHeight = body.halfBodyRectangleHeightStartValue;

		width = shieldRectangleWidthStartValue;
		height = shieldRectangleHeight;
	}

	public Rectangle2D.Double getShieldRectangle() {
		shieldRectangle.setFrame(x, y, width, height);
		return shieldRectangle;
	}
	
}
