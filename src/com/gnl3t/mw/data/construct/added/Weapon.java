package com.gnl3t.mw.data.construct.added;


import com.gnl3t.mw.arena.engine.ArenaJFrameEngine;
import com.gnl3t.mw.data.enums.KnightType;


//public class Weapon extends BasicData {
public class Weapon {
//	private ArenaEnvironment aenv;


//	public int damageStartValue = 10;
	public int damageStartValue = ArenaJFrameEngine.reduceDamageFlag ? 1 : 10;
	public int damage = damageStartValue;

	public KnightType knightType = KnightType.SWORDSMAN;
	public KnightLevelType knightLevelType = KnightLevelType.GREY1;
	  
	
//	public Weapon(ArenaEnvironment aenv) {
////		this.aenv = aenv;
//	}
	
}
