package com.gnl3t.mw.data.construct.added;


public enum KnightLevelType {
	GREY1,
	BLUE1,
	RED1,
	GREY2,
	BLUE2,
	RED2,
	GREY3,
	BLUE3,
	RED3,
	GREY4,
	BLUE4,
	RED4;

	public static KnightLevelType[] values = KnightLevelType.values();


}
