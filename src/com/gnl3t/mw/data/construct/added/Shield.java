package com.gnl3t.mw.data.construct.added;


import com.gnl3t.mw.data.enums.KnightType;
import com.gnl3t.mw.util.constants.ArenaConst;


//public class Shield extends BasicData {
public class Shield {
//	private ArenaEnvironment aenv;


	public int armor = ArenaConst.SHIELD_ARMOR;

	public KnightType knightType = KnightType.SWORDSMAN;
	public KnightLevelType knightLevelType = KnightLevelType.GREY1;
	  
	
//	public Shield(ArenaEnvironment aenv) {
////		this.aenv = aenv;
//	}
}
