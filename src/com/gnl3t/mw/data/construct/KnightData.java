package com.gnl3t.mw.data.construct;


import com.gnl3t.mw.environment.Environment;
import org.apache.log4j.Logger;


public class KnightData extends BasicData {
	private final static Logger log = Logger.getLogger(KnightData.class);


//==================================================================variables
	public double xSpeed;
	public int stepNumber = 1;
	public double traveledDistance;

//==================================================================flags
	//movement
	public boolean collisionFlag;

	//attack
	public boolean attackAttemptStartFlag;
	public boolean damageDone;

	//stamina
	public boolean staminaUsed;

	//shield
	public boolean shieldCoverFlag;

	//misc
	public boolean playerDead;

//==================================================================data
	public Body body;
	public HealthPointsBar healthPointsBar;
	public StaminaBar staminaBar;
	public Weapon weapon;
	public Shield shield;


	public KnightData(boolean revertedFlag, Environment env) {
		log.trace("---");
		this.env = env;
		this.revertedFlag = revertedFlag;


		body = new Body(revertedFlag, env);
		healthPointsBar = new HealthPointsBar(body);
		staminaBar = new StaminaBar(body);
		weapon = new Weapon(body);
		shield = new Shield(body);
	}
}
