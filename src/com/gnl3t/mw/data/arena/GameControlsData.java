package com.gnl3t.mw.data.arena;


public class GameControlsData {


//==================================================================system
	public boolean playerLeft = true;

//==================================================================pressed
	public boolean left1Pressed = false;
	public boolean right1Pressed = false;
	public boolean weapon1Pressed = false;
	public boolean shield1Pressed = false;
	public boolean left2Pressed = false;
	public boolean right2Pressed = false;
	public boolean weapon2Pressed = false;
	public boolean shield2Pressed = false;

//==================================================================released
	public boolean left1Released = false;
	public boolean right1Released = false;
	public boolean weapon1Released = false;
	public boolean shield1Released = false;
	public boolean left2Released = false;
	public boolean right2Released = false;
	public boolean weapon2Released = false;
	public boolean shield2Released = false;


}
