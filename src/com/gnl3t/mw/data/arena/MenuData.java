package com.gnl3t.mw.data.arena;


import com.gnl3t.mw.data.enums.MenuType;
import org.apache.log4j.Logger;


public class MenuData {
	private final static Logger log = Logger.getLogger(MenuData.class);


//==================================================================variables
	public MenuType currentMenuType = MenuType.MAIN;
	public MenuType previousMenuType = MenuType.MAIN;


}
