package com.gnl3t.mw.data.arena;


import com.gnl3t.mw.data.construct.BasicData;
import org.apache.log4j.Logger;


public class ArenaProgressData extends BasicData {
	private final static Logger log = Logger.getLogger(ArenaProgressData.class);


//==================================================================variables
	public int victoriesInARow;
	public int opponentLevel;


	public ArenaProgressData() {
		init();
	}

	public void init() {
		victoriesInARow = 0;
		opponentLevel = 1;
	}
}
