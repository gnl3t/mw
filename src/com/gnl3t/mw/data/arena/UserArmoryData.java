package com.gnl3t.mw.data.arena;


import com.gnl3t.mw.data.construct.added.Armor;
import com.gnl3t.mw.data.construct.added.Helmet;
import com.gnl3t.mw.data.construct.added.Shield;
import com.gnl3t.mw.data.construct.added.Weapon;

import java.util.List;


public class UserArmoryData {
	public int equippedHelmetIndex = 0;
	public int equippedWeaponIndex = 0;
	public int equippedShieldIndex = 0;
	public int equippedArmorIndex = 0;


	public List<Helmet> helmets;
	public List<Weapon> weapons;
	public List<Shield> shields;
	public List<Armor> armors;

}
