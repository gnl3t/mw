package com.gnl3t.mw.data.enums;


public enum MenuType {
	MAIN,
	ARENA_BUTTONS,
//	ARENA_BUTTONS_ONLINE_BETA,
	ARENA_PAUSE,
	VICTORY,
	DEFEAT,
	SETTINGS,
	ONLINE_BETA,
	SOUND_SETTINGS;

	public static MenuType[] values = MenuType.values();


}
