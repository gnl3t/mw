package com.gnl3t.mw.data.enums;


public enum KnightType {
	SWORDSMAN,
	VIKING,
	SARACEN,
	LEGIONER,
	SPARTAN,
	LONGSWORDSMAN;
//	SAMURAI,
//	FULL_ARMOR_KNIGHT;

	public static KnightType[] values = KnightType.values();


}
