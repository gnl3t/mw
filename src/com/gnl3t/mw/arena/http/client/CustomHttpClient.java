package com.gnl3t.mw.arena.http.client;


import com.gnl3t.mw.util.Log4jUtil;
import com.gnl3t.mw.util.constants.StringConst;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;


public class CustomHttpClient {
	private static final Logger log = Logger.getLogger(CustomHttpClient.class);
//	private ObjectMapper mapper = new ObjectMapper();


	public CustomHttpClient() {
	}

	public CustomHttpClient(String urlString, String request) throws IOException {
		sendRequest(urlString, request);
	}

	public String sendRequest(String urlString, String request) throws IOException {
		String response;
		try {
			//URL url = new URL("http://example.com");
			//URL url = new URL("http://localhost:8000/movie/list/data");
			//URL url = new URL("http://localhost:8000/movie/list");
			URL url = new URL(urlString);
			HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
			//httpURLConnection.setRequestMethod("GET");
			httpURLConnection.setRequestMethod("POST");
			httpURLConnection.setRequestProperty("Content-Type", "application/json; utf-8");
			httpURLConnection.setRequestProperty("Accept", "application/json");
			httpURLConnection.setDoOutput(true);
//			MovieDataRequest c = new MovieDataRequest();
//			c.deleted = false;
//			c.pageNumber = 0;
//			c.pageSize = 1;
//			c.movieListName = "Modified";
			//String jsonInputString = "{\"name\": \"John\", \"job\": \"Programmer\"}";
			//String jsonInputString = "{\"id\":null,\"movieName\":null,\"movieListName\":\"Modified\",\"year\":null,\"yearFrom\":null,\"yearTo\":null,\"director\":null,\"ratingKinopoisk\":null,\"ratingImdb\":null,\"ratingMy\":null,\"movieType\":null,\"movieOriginalLanguage\":null,\"linkKinopoisk\":null,\"linkImdb\":null,\"description\":null,\"addToWatchListFlag\":null,\"addToHideListFlag\":null,\"alreadyWatchedFlag\":null,\"onlyForPersonalUsageFlag\":null,\"createDate\":null,\"modifyDate\":null,\"deleted\":false,\"pageNumber\":0,\"pageSize\":20}";
//			String request = mapper.writeValueAsString(c);
			//log.debug("request=" + request);
			try(OutputStream os = httpURLConnection.getOutputStream()) {
//				byte[] input = mapper.writeValueAsString(c).getBytes(StandardCharsets.UTF_8);
				//byte[] input = jsonInputString.getBytes(StandardCharsets.UTF_8);
				byte[] input = request.getBytes(StandardCharsets.UTF_8);
				os.write(input, 0, input.length);
			}


			response = readResponse(httpURLConnection);

			httpURLConnection.disconnect();
		} catch(Exception e) {
			Log4jUtil.logException(log, e);
			throw e;
		}
		return response;
	}

	private String readResponse(HttpURLConnection httpURLConnection) throws IOException {
		String response;
		try {
			BufferedReader in = new BufferedReader(
					new InputStreamReader(httpURLConnection.getInputStream()));
			String inputLine;
			StringBuffer content = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				content.append(inputLine).append(StringConst.BACK_SLASH_N);
			}
			in.close();
//			log.debug("content=\n" + content);
			response = content.toString();
		} catch(Exception e) {
			Log4jUtil.logException(log, e);
			throw e;
		}
		return response;
	}

}
