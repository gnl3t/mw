package com.gnl3t.mw.arena.udp.client;


import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.DataPacketValidatorUtil;
import com.gnl3t.mw.util.Log4jUtil;
import com.gnl3t.mw.util.ThreadUtil;
import com.gnl3t.mw.util.constants.StringConst;
import org.apache.log4j.Logger;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;


public class UdpClient {
	private Logger log = Logger.getLogger(UdpClient.class);
	private Environment env;

	boolean debugFlag = true;
	boolean outputFileWriteFlag = false;
	boolean initializedServerAddressFlag = false;
	public boolean checkResponsesFlag = true;

	private ByteBuffer byteBuffer;
	private DatagramChannel datagramChannel;
	private InetSocketAddress initialSocketAddress;
	private DatagramSocket datagramSocket;
	private InetSocketAddress workingSocketAddress;
	private InetSocketAddress serverSocketAddress;
	private SocketAddress clientSocketAddress;


	public int requestsHandledNumber = 0;
	public final int REQUESTS_HANDLED_NUMBER_LIMIT = 500;
//	public static int byteBufferSize = 128000;
	public int byteBufferSize = 8192;
//	public static int byteBufferSize = 64096;
//	public static int byteBufferSize = 60000;


	private String serverMessage;
	private String response;
	private Charset UTF8_CHARSET = Charset.forName("UTF-8");
	String playerLeft = "\n===playerLeft===";
	String playerRight = "\n===playerRight===";
	private long messageId = 0;
	private long lastTimeMark = 0;

	Path filePath;

    public String HOST = "localhost";
	public int PORT = 9001;
	public int tcpPort = 8090;

	public UdpClient(String host, int port, Environment env) {
		HOST = host;
		PORT = port;
		this.env = env;

		if(outputFileWriteFlag) {
			try {
				createOutputFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public UdpClient(String host, int port) {
		HOST = host;
		PORT = port;
	}

	public void punchHole(int tcpPort) throws IOException {
		this.tcpPort = tcpPort;
		punchHole();
	}

	public void punchHole() throws IOException {
//		Socket socket = new Socket(InetAddress.getByName(HOST), tcpPort); //tcp hole punch
//
//		DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
////		dataOutputStream.write("keepAlive".getBytes());
//		dataOutputStream.writeUTF("keepAlive");
//		dataOutputStream.flush(); // Send off the data

		punchHole(HOST, tcpPort);
	}

	public void punchHole(String host, int port) throws IOException {
		dbg("punch hole to " + host + ":" + port);
		Socket socket = new Socket(InetAddress.getByName(host), port); //tcp hole punch

		DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
//		dataOutputStream.write("keepAlive".getBytes());
		dataOutputStream.writeUTF("keepAlive");
		dataOutputStream.flush(); // Send off the data
	}

	public void setWorkingSocketAddress(String host, int port) {
        workingSocketAddress = new InetSocketAddress(host, port);
	}

	public void createOutputFile() throws IOException {
//		String dateString = ("" + new Date()).replaceAll(":", "_");
		String dateString = Instant.now().toString().replaceAll(":", "_");
		dbg("dateString=" + dateString);

		if(env.gameControlsData.playerLeft) {
			log.debug(playerLeft);
		} else {
			log.debug(playerRight);
		}

		String fileMark = env.gameControlsData.playerLeft ? "_left" : "_right";

		String relativePath = "./output_files/";
		String filePathString = relativePath + dateString + fileMark + ".text";
		dbg("filePathString=" + filePathString);
		List<String> lines = Arrays.asList(fileMark, dateString);
		filePath = Paths.get(filePathString);
		Files.write(filePath, lines, StandardCharsets.UTF_8);
	}

	public void stop() {
		try {
			datagramChannel.close();
		} catch (Exception e) {
			Log4jUtil.logException(log, e);
		}
		byteBuffer = null;
	}

	public String getHostPortString() {
		return HOST + ":" + PORT;
	}

	public void connect() throws IOException {
		dbg("connecting to " + getHostPortString());

		datagramChannel = DatagramChannel.open();
		initialSocketAddress = new InetSocketAddress(0); // port 0 selects any available port
		datagramSocket = datagramChannel.socket();
		datagramSocket.setSoTimeout(5000); //TODO - can be a bottleneck
//		datagramSocket.setSoTimeout(1000); //TODO - can be a bottleneck
		datagramSocket.bind(initialSocketAddress);

		serverSocketAddress = new InetSocketAddress(HOST, PORT);
		workingSocketAddress = new InetSocketAddress(HOST, PORT);

		byteBuffer = ByteBuffer.allocate(byteBufferSize);

		dbg("connected to " + getHostPortString());
	}

	public synchronized String sendMessage(String message) {
		try {
			dbg("workingSocketAddress=" + workingSocketAddress);
			dbg("message=\n" + message);
			sendUdpMessage(message);
//			response = getUdpMessage();
			if(checkResponsesFlag) response = getUdpMessage();
//			dbg("response=" + response);
			dbg("response=\n" + response);
//			if(response != null && !response.equalsIgnoreCase("    ==================================================================")) dbg("response=\n" + response);
		} catch (Exception e) {
			Log4jUtil.logException(log, e);
		}
		callGarbageCollector();
		return response;
	}

	public String getSocketMessage() {
		byteBuffer.flip();
		String message = StandardCharsets.UTF_8.decode(byteBuffer).toString().trim();
		byteBuffer = ByteBuffer.allocate(byteBufferSize);
		return message;
	}

	public String startServer() {
        dbg("startServer");
		while (true) {
			try {
				processByteBuffer();
//				keepAlive();
			} catch (Exception e) {
				Log4jUtil.logException(log, e);
			}
		}
	}






//==================================================================protocol methods

	private synchronized void sendUdpMessage(String message) throws Exception {
		byteBuffer = ByteBuffer.wrap(message.getBytes());
		datagramChannel.send(byteBuffer, workingSocketAddress);
	}

	private synchronized void sendUdpMessageToServer(String message) throws Exception {
		byteBuffer = ByteBuffer.wrap(message.getBytes());
		datagramChannel.send(byteBuffer, serverSocketAddress);
	}

	public synchronized String getUdpMessage() throws IOException {
		byteBuffer = ByteBuffer.allocate(byteBufferSize);
		byteBuffer.put("    ".getBytes());
		datagramChannel.receive(byteBuffer);
		byteBuffer.flip();

		return StandardCharsets.UTF_8.decode(byteBuffer).toString();
//		return new String(byteBuffer.array());
//		return new String(byteBuffer.array(), "UTF-8");
//		return new String(byteBuffer.array(), "UTF-8");
//		return new String(byteBuffer.array(), UTF8_CHARSET);
	}

	private void processByteBuffer() throws Exception {
        keepAlive2();

		byteBuffer = ByteBuffer.allocate(byteBufferSize);
		clientSocketAddress = datagramChannel.receive(byteBuffer);
//		dbg("clientSocketAddress=" + clientSocketAddress);
		workingSocketAddress = (InetSocketAddress) clientSocketAddress;
//		if(!initializedServerAddressFlag) {
//			serverSocketAddress = new InetSocketAddress(workingSocketAddress.getHostName(), workingSocketAddress.getPort());
//			initializedServerAddressFlag = true;
//		}

		serverMessage = getSocketMessage();
		if (!serverMessage.equals(StringConst.EMPTY_STRING)) {
			log.debug("serverMessage='" + serverMessage + "'");
		} else {
			return;
		}


		if(serverMessage.contains(StringConst.BEGIN)) {
//			log.debug("DataPacketValidatorUtil.validate(serverMessage)=" + DataPacketValidatorUtil.validate(serverMessage));
			if (DataPacketValidatorUtil.validate(serverMessage)) {
				log.debug("DataPacketValidatorUtil.validate(serverMessage)=true");
				dbg("clientSocketAddress=" + clientSocketAddress);

				sendUdpMessage(makePacket(serverMessage));
			} else {
				log.debug("DataPacketValidatorUtil.validate(serverMessage)=false");
				sendUdpMessage(makePacket(StringConst.EMPTY_STRING));
			}
		} else {
			sendUdpMessage(makePacket("dbg"));
//			echoResponse();
		}
//		ThreadUtil.sleep(5000);

		callGarbageCollector();
	}

	private String makePacket(String contentString) {
		log.trace("---");
		return StringConst.BEGIN + "udpServer1" + contentString + StringConst.END;
	}

	private void keepAlive() {
		log.trace("---");
		if(System.currentTimeMillis() - lastTimeMark > 1000) {
			if(serverSocketAddress == null) return;
			workingSocketAddress = new InetSocketAddress(serverSocketAddress.getHostName(), serverSocketAddress.getPort());
//			sendMessage("keepAlive");
			sendMessage(makePacket("keepAlive"));
			lastTimeMark = System.currentTimeMillis();
		}
	}

	private void keepAlive2() {
		log.trace("---");
		if(System.currentTimeMillis() - lastTimeMark > 1000) {
			if(serverSocketAddress == null) return;
//			workingSocketAddress = new InetSocketAddress(serverSocketAddress.getHostName(), serverSocketAddress.getPort());
//			sendMessage("keepAlive");
			sendMessage(makePacket("keepAlive"));
			lastTimeMark = System.currentTimeMillis();
		}
	}

	public void serverKeepAlive() {
		log.trace("---");

        try {
            String message = "keepAlive";
            dbg("serverSocketAddress=" + serverSocketAddress);
            dbg("message=\n" + message);
            sendUdpMessageToServer(message);
//            response = getUdpMessage();
//			dbg("response=" + response);
            dbg("response=\n" + response);
        } catch (Exception e) {
            Log4jUtil.logException(log, e);
        }
	}


//==================================================================util methods

	private void callGarbageCollector() {
		if (++requestsHandledNumber > REQUESTS_HANDLED_NUMBER_LIMIT) {
			requestsHandledNumber = 0;
			System.gc();
		}
	}

	public void dbg(String sourceString) {
		log.debug(sourceString);
		if(!outputFileWriteFlag) return;
//		++messageId;
////		if(debugFlag) log.debug(sourceString);
//		if(debugFlag) {
//			if(env.gameControlsData.playerLeft) {
////				log.debug(playerLeft);
//				log.debug(playerLeft + messageId);
//			} else {
////				log.debug(playerRight);
//				log.debug(playerRight + messageId);
//			}
//			log.debug(sourceString);
////			log.debug(messageId + StringConst.BACK_SLASH_N + sourceString);
//
//			try {
////				if(filePath != null) Files.write(filePath, (sourceString + StringConst.BACK_SLASH_N).getBytes(), StandardOpenOption.APPEND);
////				if(filePath != null) Files.write(filePath, (Instant.now().toString() + ": " + sourceString + StringConst.BACK_SLASH_N).getBytes(), StandardOpenOption.APPEND);
//
//
//				if(filePath != null) Files.write(filePath, (messageId + ": " + sourceString + StringConst.BACK_SLASH_N).getBytes(), StandardOpenOption.APPEND);
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
	}
}
