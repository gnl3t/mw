package com.gnl3t.mw.arena.udp.client;


import com.gnl3t.mw.util.Log4jUtil;
import org.apache.log4j.Logger;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.*;


public class UdpClient2 {
	final static Logger log = Logger.getLogger(UdpClient2.class);

	boolean debugFlag = true;

	static int requestsHandledNumber = 0;
	static final int REQUESTS_HANDLED_NUMBER_LIMIT = 50;

	private static InetAddress serverInetAddress;
	private static int PORT;
	private Socket socket;
	private BufferedReader bufferedReader;
	private BufferedOutputStream bufferedOutputStream;
	private DatagramSocket datagramSocket;
	private DatagramPacket datagramPacket;
	private DatagramPacket receiveDatagramPacket;
	private String response = "";
	private byte[] byteArray;

	public UdpClient2(InetAddress inetAddress, int port) throws IOException {
		init(inetAddress, port);
	}

	private void init(InetAddress inetAddress, int port) throws IOException {
		serverInetAddress = inetAddress;
		PORT = port;

		datagramSocket = new DatagramSocket();
	}

	public void punchHole( int port) throws IOException {
		new Socket(serverInetAddress, port); //tcp hole punch
	}

//	public void sendUdpMessage(String message) throws IOException {
//		dbg("message=" + message);
//		byteArray = message.getBytes();
//		datagramPacket = new DatagramPacket(byteArray, byteArray.length, serverInetAddress, PORT);
//		datagramSocket.sendMessages(datagramPacket);
//
//		dbg("Sent message");
//		callGarbageCollector();
//	}

	public synchronized String sendMessage(String message) {
		try {
			dbg("message=" + message);
			sendUdpMessage(message);
//			response = getUdpMessage();
			dbg("response=\n" + response);
		} catch (Exception e) {
			Log4jUtil.logException(log, e);
		}
		callGarbageCollector();
		return response;
	}






//==================================================================protocol methods

	private void sendUdpMessage(String message) throws IOException {
		byteArray = message.getBytes();
		datagramPacket = new DatagramPacket(byteArray, byteArray.length, serverInetAddress, PORT);
		datagramSocket.send(datagramPacket);
	}

//	private synchronized String getUdpMessage() throws IOException {
//		byteBuffer = ByteBuffer.allocate(byteBufferSize);
//		byteBuffer.put("    ".getBytes());
////		socketChannel.read(byteBuffer);
////		return new String(byteBuffer.array()).trim();
//		datagramChannel.receive(byteBuffer);
//		byteBuffer.flip();
//
//		return StandardCharsets.UTF_8.decode(byteBuffer).toString();
//	}


//==================================================================util methods

	private void callGarbageCollector() {
		if (++requestsHandledNumber > REQUESTS_HANDLED_NUMBER_LIMIT) {
			requestsHandledNumber = 0;
			System.gc();
		}
	}

	public void dbg(String sourceString) {
		if(debugFlag) log.debug(sourceString);
	}
}
