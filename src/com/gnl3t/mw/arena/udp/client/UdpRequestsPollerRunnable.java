package com.gnl3t.mw.arena.udp.client;


import com.gnl3t.mw.arena.engine.ArenaJFrameEngine;
import com.gnl3t.mw.arena.http.client.CustomHttpClient;
import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.Log4jUtil;
import com.gnl3t.mw.util.LogUtil;
import com.gnl3t.mw.util.ThreadUtil;
import org.apache.log4j.Logger;

import java.io.IOException;


public class UdpRequestsPollerRunnable implements Runnable {
	private final static Logger log = Logger.getLogger(UdpRequestsPollerRunnable.class);
	private Environment env;
	private int sleepTime = ArenaJFrameEngine.localHostFlag ? 10 : 1;
//	private int sleepTime = ArenaJFrameEngine.localHostFlag ? 9000 : 5;

	private int duplicateLimit = 6;
	private int duplicateCounter = duplicateLimit;

	private boolean sentLastMessageFlag = false;
	public boolean continueWorkFlag = true;


	public UdpRequestsPollerRunnable(Environment env) {
		init(env);
	}

	public void init(Environment env) {
		log.trace("---");
		this.env = env;
	}

	@Override
	public void run() {
		log.trace("---");

		clearServerQueue(env);
		registerClientAddress(env);

		while(continueWorkFlag) {

			if(duplicateCounter < duplicateLimit) {
				env.arenaEngine.stepCadre();
				++duplicateCounter;
				env.arenaRender.preparedNextActionFlag = true;
			} else {
				if(sentLastMessageFlag) return;
				duplicateCounter = 0;
				env.arenaRender.preparedNextActionFlag = false;


				env.arenaOnlinePacketsEngine.putMessages();

				dbg("send for client " + env.playerLeftString);
				dbg("env.gameControlsData.playerLeft=" + env.gameControlsData.playerLeft);
				env.arenaUdpClientEngine.sendMessages();
				env.arenaUdpClientEngine.sendMessage("request");

				if(env.arenaRender.playedLastActionFlag && !env.arenaRender.preparedNextActionFlag) {
					env.arenaRender.playedLastActionFlag = false;
//					clearReceivedData(env);
					env.arenaOnlinePacketsEngine.handlePackets();
					env.arenaRender.preparedNextActionFlag = true;
				}
				if(env.arenaRender.preparedNextActionFlag) env.arenaEngine.stepCadre();
//				if(env.arenaRender.preparedNextActionFlag) {
//					env.arenaEngine.stepCadre();
//				}

				if(env.k1.playerDead || env.k2.playerDead) finalizeData(env);
			}

			if(ArenaJFrameEngine.localHostFlag) {
				try {
					Thread.sleep(sleepTime);
				} catch (InterruptedException e) {
					dbg(e);
				}
			}


//			if(ArenaJFrameEngine.localHostFlag) {
//				try {
////				wait(10);
////				Thread.sleep(30);
////				Thread.sleep(1);
////				Thread.sleep(100);
////				Thread.sleep(3000);
//
////				Thread.sleep(30); //local
////				Thread.sleep(1); //cloud
//					Thread.sleep(sleepTime); //cloud
//				} catch (InterruptedException e) {
////				e.printStackTrace();
//					Log4jUtil.logException(log, e);
//				}
//			}
		}
	}

	private void clearReceivedData(Environment env) {
//		env.gameControlsData.right1Pressed = false;
//		env.gameControlsData.left1Pressed = false;
//		env.gameControlsData.weapon1Pressed = false;
//		env.gameControlsData.shield1Pressed = false;
//
//		env.gameControlsData.right2Pressed = false;
//		env.gameControlsData.left2Pressed = false;
//		env.gameControlsData.weapon2Pressed = false;
//		env.gameControlsData.shield2Pressed = false;

		env.gameControlsReceivedData.right1Released = false;
		env.gameControlsReceivedData.left1Released = false;
		env.gameControlsReceivedData.weapon1Released = false;
		env.gameControlsReceivedData.shield1Released = false;

		env.gameControlsReceivedData.right2Released = false;
		env.gameControlsReceivedData.left2Released = false;
		env.gameControlsReceivedData.weapon2Released = false;
		env.gameControlsReceivedData.shield2Released = false;
	}

	private void registerClientAddress(Environment env) {
		String clientId = env.gameControlsData.playerLeft ? "left123" : "right456";

		env.arenaUdpClientEngine.sendMessage("getClientAddress\n" +
				"clientId=" + clientId + "\n");
	}

	private void clearServerQueue(Environment env) {
		try {
			String request = "type=command\n" +
					"commandName=clearQueue";
			new CustomHttpClient("http://localhost:8000/command", request);

//			releaseButtons(env);
//			env.arenaOnlinePacketsEngine.handleResponse("right1Released" +
//					"left1Released" +
//					"weapon1Released" +
//					"shield1Released" +
//					"right2Released" +
//					"left2Released" +
//					"weapon2Released" +
//					"shield2Released");
			ThreadUtil.sleep(500);
		} catch (IOException e) {
			Log4jUtil.logException(log, e);
		}
	}

	private void finalizeData(Environment env) {
		env.arenaOnlinePacketsEngine.putMessages();
		env.arenaUdpClientEngine.sendMessages();
		env.arenaUdpClientEngine.sendMessage("request");
		env.arenaOnlinePacketsEngine.putMessages();
		env.arenaUdpClientEngine.sendMessages();
		env.arenaUdpClientEngine.sendMessage("request");
		env.arenaOnlinePacketsEngine.putMessages();
		env.arenaUdpClientEngine.sendMessages();
		env.arenaUdpClientEngine.sendMessage("request");

		env.arenaUdpClientEngine.sendMessage("finalized");

		sentLastMessageFlag = true;
		if(env.udpRequestsPollerEmulatorRunnable != null) env.arenaOnlineBetaEngine.notifyServerMatchIsOver(env.udpRequestsPollerEmulatorRunnable.playerLeftString);
		env.arenaOnlineBetaEngine.notifyServerMatchIsOver(env.playerLeftString);
		finalizeRun(env);
	}

	public void finalizeRun(Environment env) {
		continueWorkFlag = false;
		if(env.udpRequestsPollerEmulatorRunnable != null) env.udpRequestsPollerEmulatorRunnable.continueWorkFlag = false;
	}

	private void releaseButtons(Environment env) {
		env.gameControlsData.right1Pressed = false;
		env.gameControlsData.left1Pressed = false;
		env.gameControlsData.weapon1Pressed = false;
		env.gameControlsData.shield1Pressed = false;

		env.gameControlsData.right2Pressed = false;
		env.gameControlsData.left2Pressed = false;
		env.gameControlsData.weapon2Pressed = false;
		env.gameControlsData.shield2Pressed = false;

		env.gameControlsReceivedData.right1Pressed = false;
		env.gameControlsReceivedData.left1Pressed = false;
		env.gameControlsReceivedData.weapon1Pressed = false;
		env.gameControlsReceivedData.shield1Pressed = false;

		env.gameControlsReceivedData.right2Pressed = false;
		env.gameControlsReceivedData.left2Pressed = false;
		env.gameControlsReceivedData.weapon2Pressed = false;
		env.gameControlsReceivedData.shield2Pressed = false;


		env.gameControlsData.right1Released = true;
		env.gameControlsData.left1Released = true;
		env.gameControlsData.weapon1Released = true;
		env.gameControlsData.shield1Released = true;

		env.gameControlsData.right2Released = true;
		env.gameControlsData.left2Released = true;
		env.gameControlsData.weapon2Released = true;
		env.gameControlsData.shield2Released = true;

		env.gameControlsReceivedData.right1Released = true;
		env.gameControlsReceivedData.left1Released = true;
		env.gameControlsReceivedData.weapon1Released = true;
		env.gameControlsReceivedData.shield1Released = true;

		env.gameControlsReceivedData.right2Released = true;
		env.gameControlsReceivedData.left2Released = true;
		env.gameControlsReceivedData.weapon2Released = true;
		env.gameControlsReceivedData.shield2Released = true;
	}


//==================================================================dbg_print

	private void dbg(String text) {
		log.debug(text);
	}

	private void dbg(Exception e) {
		dbg(LogUtil.buildStackTraceAsString(e));
	}

}
