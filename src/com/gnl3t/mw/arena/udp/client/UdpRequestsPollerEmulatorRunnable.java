package com.gnl3t.mw.arena.udp.client;


import com.gnl3t.mw.arena.engine.ArenaJFrameEngine;
import com.gnl3t.mw.arena.http.client.CustomHttpClient;
import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.LogUtil;
import com.gnl3t.mw.util.RequestUtils;
import com.gnl3t.mw.util.ThreadUtil;
import com.gnl3t.mw.util.constants.StringConst;
import org.apache.log4j.Logger;

import java.util.TreeMap;


public class UdpRequestsPollerEmulatorRunnable implements Runnable {
	private final static Logger log = Logger.getLogger(UdpRequestsPollerEmulatorRunnable.class);
	private Environment env;
	private int sleepTime = ArenaJFrameEngine.localHostFlag ? 10 : 1;
//	private int sleepTime = ArenaJFrameEngine.localHostFlag ? 9000 : 5;

	private boolean sentLastMessageFlag = false;
	public boolean continueWorkFlag = true;


	public String playerLeftString = "playerLeft";
	public String host = "localhost";
	public int tcpPort = -1;
	public int udpPort = -1;


	public UdpRequestsPollerEmulatorRunnable(Environment env) {
		init(env);
	}

	public void init(Environment env) {
		log.trace("---");
		this.env = env;
		getOpponentForEmulator();
	}

	public void initRequest(String response) {
		if(!response.contains("foundOpponent=true")) {
			continueWorkFlag = false;
			return;
		}

		TreeMap<String, String> responseTree = RequestUtils.parseToTreeMap(response);
//		for (Map.Entry<String, String> entry : responseTree.entrySet()) {
////			dbg("key=" + entry.getKey() + ", value=" + entry.getValue());
//			dbg(entry.getKey() + "=" + entry.getValue());
//		}
		boolean playerLeft = Boolean.parseBoolean(responseTree.get("playerLeft"));
		playerLeftString = playerLeft ? "playerLeft" : "playerRight";
		host = responseTree.get("duelHandlerHost");
		if(playerLeft) {
			tcpPort = Integer.parseInt(responseTree.get("duelHandlerTcpPort1"));
		} else {
			tcpPort = Integer.parseInt(responseTree.get("duelHandlerTcpPort2"));
		}
		udpPort = Integer.parseInt(responseTree.get("duelHandlerUdpPort"));
	}

	public void initRequest1(String response) {
		if(!response.contains("foundOpponent=true")) {
			continueWorkFlag = false;
			return;
		}

		TreeMap<String, String> responseTree = RequestUtils.parseToTreeMap(response);
//		for (Map.Entry<String, String> entry : responseTree.entrySet()) {
////			dbg("key=" + entry.getKey() + ", value=" + entry.getValue());
//			dbg(entry.getKey() + "=" + entry.getValue());
//		}
		boolean playerLeft = Boolean.parseBoolean(responseTree.get("playerLeft"));
		playerLeftString = !playerLeft ? "playerLeft" : "playerRight";
		int correction = playerLeft ? 1 : -1;
		host = responseTree.get("duelHandlerHost");
		if(!playerLeft) {
			tcpPort = Integer.parseInt(responseTree.get("duelHandlerTcpPort1"));
		} else {
			tcpPort = Integer.parseInt(responseTree.get("duelHandlerTcpPort2"));
		}
		udpPort = Integer.parseInt(responseTree.get("duelHandlerUdpPort")) + correction;
	}

	private void getOpponentForEmulator() {
		dbg("getOpponentForEmulator");
		try {
			String pathString = "/command";
			String httpString = ArenaJFrameEngine.localHostFlag ? "http://localhost:8000" + pathString : "http://164.90.171.227:8000" + pathString;
			CustomHttpClient customHttpClient = new CustomHttpClient();

			TreeMap<String, String> messageTree = RequestUtils.createEmptyMessageTree();
			messageTree.put("type", "command");
			messageTree.put("commandName", "getOpponentInfo");
//			messageTree.put("nickName", env.arenaJFrameEngine.nickNameTextArea.getText());
			messageTree.put("nickName", "abc");

			messageTree.put("weaponType", StringConst.EMPTY_STRING + env.userArmoryData.weapons.get(env.userArmoryData.equippedWeaponIndex).knightType);
			messageTree.put("weaponLevelType", StringConst.EMPTY_STRING + env.userArmoryData.weapons.get(env.userArmoryData.equippedWeaponIndex).knightLevelType);
			messageTree.put("armorType", StringConst.EMPTY_STRING + env.userArmoryData.armors.get(env.userArmoryData.equippedArmorIndex).knightType);
			messageTree.put("armorLevelType", StringConst.EMPTY_STRING + env.userArmoryData.armors.get(env.userArmoryData.equippedArmorIndex).knightLevelType);
			messageTree.put("helmetType", StringConst.EMPTY_STRING + env.userArmoryData.helmets.get(env.userArmoryData.equippedHelmetIndex).knightType);
			messageTree.put("helmetLevelType", StringConst.EMPTY_STRING + env.userArmoryData.helmets.get(env.userArmoryData.equippedHelmetIndex).knightLevelType);
			messageTree.put("shieldType", StringConst.EMPTY_STRING + env.userArmoryData.shields.get(env.userArmoryData.equippedShieldIndex).knightType);
			messageTree.put("shieldLevelType", StringConst.EMPTY_STRING + env.userArmoryData.shields.get(env.userArmoryData.equippedShieldIndex).knightLevelType);

			messageTree.put("equipmentTotalCost", "1500");
			messageTree.put("ping", "123456");
			messageTree.put("nFactorial", "-1");

			String request = RequestUtils.serializeMessageTree(messageTree);


			dbg("httpString=" + httpString);
			dbg("request=\n" + request);

			String response = customHttpClient.sendRequest(httpString, request);
			dbg("response=\n" + response);

			initRequest(response);
		} catch (Exception e) {
			dbg(e);
		}
	}

	@Override
	public void run() {
		log.trace("---");
		if(!continueWorkFlag) return;


		UdpClient udpClient = new UdpClient(host, udpPort);
		try {
			udpClient.punchHole(tcpPort);
            dbg("connect udp emulator");
			udpClient.connect();
			while(continueWorkFlag) {
				dbg("send for emulator " + playerLeftString);
				udpClient.sendMessage("BEGIN" + playerLeftString + "\n" +
						"matchId=" + env.matchId + "\n" +
						"request\n" +
						"END");


				if(ArenaJFrameEngine.localHostFlag) {
					try {
						Thread.sleep(sleepTime);
					} catch (InterruptedException e) {
						dbg(e);
					}
				}
			}
		} catch (Exception e) {
			dbg(e);
		}
	}


//==================================================================dbg_print

	private void dbg(String text) {
		log.debug(text);
	}

	private void dbg(Exception e) {
		dbg(LogUtil.buildStackTraceAsString(e));
	}

}
