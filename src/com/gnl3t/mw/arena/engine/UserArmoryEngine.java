package com.gnl3t.mw.arena.engine;


import com.gnl3t.mw.data.construct.added.*;
import com.gnl3t.mw.data.enums.KnightType;
import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.constants.Const;

import java.util.List;


public class UserArmoryEngine {
	private Environment env;


	public void init(Environment aenv) {
		this.env = aenv;
	}


	public void setDefaultArmoryData() {
		List<Helmet> helmetsList = Const.getNewArrayList();
		Helmet helmet1 = new Helmet();
		helmet1.knightType = KnightType.SWORDSMAN;
		helmet1.knightLevelType = KnightLevelType.GREY1;
		helmetsList.add(helmet1);
		env.userArmoryData.helmets = helmetsList;

		List<Weapon> weaponsList = Const.getNewArrayList();
		Weapon weapon1 = new Weapon();
		weapon1.knightType = KnightType.SWORDSMAN;
		weapon1.knightLevelType = KnightLevelType.GREY1;
		weaponsList.add(weapon1);
		env.userArmoryData.weapons = weaponsList;

		List<Shield> shieldList = Const.getNewArrayList();
		Shield shield1 = new Shield();
		shield1.knightType = KnightType.SWORDSMAN;
		shield1.knightLevelType = KnightLevelType.GREY1;
		shieldList.add(shield1);
		env.userArmoryData.shields = shieldList;

		List<Armor> armorsList = Const.getNewArrayList();
		Armor armor1 = new Armor();
		armor1.knightType = KnightType.SWORDSMAN;
		armor1.knightLevelType = KnightLevelType.GREY1;
		armorsList.add(armor1);
		env.userArmoryData.armors = armorsList;
	}

	public Helmet getEquippedHelmet() {
		return env.userArmoryData.helmets.get(env.userArmoryData.equippedHelmetIndex);
	}

	public Weapon getEquippedWeapon() {
		return env.userArmoryData.weapons.get(env.userArmoryData.equippedWeaponIndex);
	}

//	public void equipPurchasedWeapon() {
//		env.userArmoryData.equippedWeaponIndex = env.userArmoryData.weapons.size() - 1;
//	}

	public Shield getEquippedShield() {
		return env.userArmoryData.shields.get(env.userArmoryData.equippedShieldIndex);
	}

	public Armor getEquippedArmor() {
		return env.userArmoryData.armors.get(env.userArmoryData.equippedArmorIndex);
	}

}
