package com.gnl3t.mw.arena.engine;


import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.Log4jUtil;
import com.gnl3t.mw.util.constants.StringConst;
import org.apache.log4j.Logger;

import java.util.concurrent.TimeUnit;


public class ArenaOnlinePacketsEngine {
	private final static Logger log = Logger.getLogger(ArenaOnlinePacketsEngine.class);
	private Environment env;
	private String message = StringConst.EMPTY_STRING;
	private String receivedMessage = StringConst.EMPTY_STRING;
	private String[] parsedReceivedMessages;
	private String[] splitStrings;
	private String[] subStrings;
	private int duplicateLimit = 3;
    private int duplicateCounter = duplicateLimit;


	public void init(Environment env) {
		log.trace("---");
		this.env = env;
	}

	public void putMessages() {
		log.trace("---");

        putMessage();
	}

    private void putMessage() {
		log.trace("---");

        message = StringConst.SPACE_SYMBOL + StringConst.BACK_SLASH_N;
//        if(env.gameControlsData.playerLeft) {
        if(true) {
            message += "x1=" + env.k1.x + StringConst.BACK_SLASH_N;
            message += "x2=" + env.k2.x + StringConst.BACK_SLASH_N;
            message += "hp1=" + env.k1.healthPointsBar.healthPoints + StringConst.BACK_SLASH_N;
            message += "hp2=" + env.k2.healthPointsBar.healthPoints + StringConst.BACK_SLASH_N;
            message += "st1=" + env.k1.staminaBar.staminaPoints + StringConst.BACK_SLASH_N;
            message += "st2=" + env.k2.staminaBar.staminaPoints + StringConst.BACK_SLASH_N;
        }

        if(env.gameControlsData.playerLeft) {
//        if(true) {
            if(env.gameControlsData.right1Released) {
                env.gameControlsData.right1Pressed = false;
                env.gameControlsData.right1Released = false;
                message += "right1Released" + StringConst.BACK_SLASH_N;
            } else {
                if(env.gameControlsData.right1Pressed) message += "right1Pressed" + StringConst.BACK_SLASH_N;
            }

            if(env.gameControlsData.left1Released) {
                env.gameControlsData.left1Pressed = false;
                env.gameControlsData.left1Released = false;
                message += "left1Released" + StringConst.BACK_SLASH_N;
            } else {
                if(env.gameControlsData.left1Pressed) message += "left1Pressed" + StringConst.BACK_SLASH_N;
            }

            if(env.gameControlsData.weapon1Released) {
                env.gameControlsData.weapon1Pressed = false;
                env.gameControlsData.weapon1Released = false;
                message += "weapon1Released" + StringConst.BACK_SLASH_N;
            } else {
                if(env.gameControlsData.weapon1Pressed) message += "weapon1Pressed" + StringConst.BACK_SLASH_N;
            }

            if(env.gameControlsData.shield1Released) {
                env.gameControlsData.shield1Pressed = false;
                env.gameControlsData.shield1Released = false;
                message += "shield1Released" + StringConst.BACK_SLASH_N;
            } else {
                if(env.gameControlsData.shield1Pressed) message += "shield1Pressed" + StringConst.BACK_SLASH_N;
            }
        } else {
            if(env.gameControlsData.right2Released) {
                env.gameControlsData.right2Released = false;
                message += "right2Released" + StringConst.BACK_SLASH_N;
            } else {
                if(env.gameControlsData.right2Pressed) message += "right2Pressed" + StringConst.BACK_SLASH_N;
            }
            if(env.gameControlsData.left2Released) {
                env.gameControlsData.left2Released = false;
                message += "left2Released" + StringConst.BACK_SLASH_N;
            } else {
                if(env.gameControlsData.left2Pressed) message += "left2Pressed" + StringConst.BACK_SLASH_N;
            }
            if(env.gameControlsData.weapon2Released) {
                env.gameControlsData.weapon2Released = false;
                message += "weapon2Released" + StringConst.BACK_SLASH_N;
            } else {
                if(env.gameControlsData.weapon2Pressed) message += "weapon2Pressed" + StringConst.BACK_SLASH_N;
            }
            if(env.gameControlsData.shield2Released) {
                env.gameControlsData.shield2Released = false;
                message += "shield2Released" + StringConst.BACK_SLASH_N;
            } else {
                if(env.gameControlsData.shield2Pressed) message += "shield2Pressed" + StringConst.BACK_SLASH_N;
            }
        }

        env.arenaUdpClientEngine.putMessage(message);
	}

    public void handleResponse(String response) {
        if(response == null) return;

        env.gameControlsReceivedData.right1Pressed = response.contains("right1Pressed");
        env.gameControlsReceivedData.left1Pressed = response.contains("left1Pressed");
        env.gameControlsReceivedData.weapon1Pressed = response.contains("weapon1Pressed");
        env.gameControlsReceivedData.shield1Pressed = response.contains("shield1Pressed");

        env.gameControlsReceivedData.right2Pressed = response.contains("right2Pressed");
        env.gameControlsReceivedData.left2Pressed = response.contains("left2Pressed");
        env.gameControlsReceivedData.weapon2Pressed = response.contains("weapon2Pressed");
        env.gameControlsReceivedData.shield2Pressed = response.contains("shield2Pressed");

        if(response.contains("right1Released")) {
            env.gameControlsReceivedData.right1Pressed = false;
            env.arenaKeyHandler.handleVK_FReleased();
        }
        if(response.contains("left1Released")) {
            env.gameControlsReceivedData.left1Pressed = false;
            env.arenaKeyHandler.handleVK_DReleased();
        }
        if(response.contains("weapon1Released")) {
            env.gameControlsReceivedData.weapon1Pressed = false;
            env.arenaKeyHandler.handleVK_SPACEReleased();
        }
        if(response.contains("shield1Released")) {
            env.gameControlsReceivedData.shield1Pressed = false;
            env.arenaKeyHandler.handleVK_BReleased();
        }

        if(response.contains("right2Released")) {
            env.gameControlsReceivedData.right2Pressed = false;
            env.arenaKeyHandler.handleVK_RIGHTReleased();
        }
        if(response.contains("left2Released")) {
            env.gameControlsReceivedData.left2Pressed = false;
            env.arenaKeyHandler.handleVK_LEFTReleased();
        }
        if(response.contains("weapon2Released")) {
            env.gameControlsReceivedData.weapon2Pressed = false;
            env.arenaKeyHandler.handleVK_ENTERReleased();
        }
        if(response.contains("shield2Released")) {
            env.gameControlsReceivedData.shield2Pressed = false;
            env.arenaKeyHandler.handleVK_BACK_SPACEReleased();
        }

        if(env.gameControlsReceivedData.right1Pressed) env.arenaKeyHandler.handleVK_FPressed();
        if(env.gameControlsReceivedData.left1Pressed) env.arenaKeyHandler.handleVK_DPressed();
        if(env.gameControlsReceivedData.weapon1Pressed) env.arenaKeyHandler.handleVK_SPACEPressed();
        if(env.gameControlsReceivedData.shield1Pressed) env.arenaKeyHandler.handleVK_BPressed();

        if(env.gameControlsReceivedData.right2Pressed) env.arenaKeyHandler.handleVK_RIGHTPressed();
        if(env.gameControlsReceivedData.left2Pressed) env.arenaKeyHandler.handleVK_LEFTPressed();
        if(env.gameControlsReceivedData.weapon2Pressed) env.arenaKeyHandler.handleVK_ENTERPressed();
        if(env.gameControlsReceivedData.shield2Pressed) env.arenaKeyHandler.handleVK_BACK_SPACEPressed();

//        if(!env.gameControlsData.playerLeft) {
//        if(true) {
//            if(response.contains("x1")) {
//                splitStrings = response.split(StringConst.BACK_SLASH_N);
//                for (String splitString : splitStrings) {
//                    if(splitString.contains("x1")) {
//                        subStrings = splitString.split("=");
//                        env.gameControlsReceivedData.x1 = Integer.parseInt(subStrings[1]);
//                    }
//                    if(splitString.contains("x2")) {
//                        subStrings = splitString.split("=");
//                        env.gameControlsReceivedData.x2 = Integer.parseInt(subStrings[1]);
//                    }
//                    if(splitString.contains("hp1")) {
//                        subStrings = splitString.split("=");
//                        env.gameControlsReceivedData.hp1 = Integer.parseInt(subStrings[1]);
//                    }
//                    if(splitString.contains("hp2")) {
//                        subStrings = splitString.split("=");
//                        env.gameControlsReceivedData.hp2 = Integer.parseInt(subStrings[1]);
//                    }
//                    if(splitString.contains("st1")) {
//                        subStrings = splitString.split("=");
//                        env.gameControlsReceivedData.st1 = Integer.parseInt(subStrings[1]);
//                    }
//                    if(splitString.contains("st2")) {
//                        subStrings = splitString.split("=");
//                        env.gameControlsReceivedData.st2 = Integer.parseInt(subStrings[1]);
//                    }
//                }
//            }
////            message += "x1=" + env.k1.x + StringConst.BACK_SLASH_N;
////            message += "x2=" + env.k2.x + StringConst.BACK_SLASH_N;
////            message += "hp1=" + env.k1.healthPointsBar.healthPoints + StringConst.BACK_SLASH_N;
////            message += "hp2=" + env.k2.healthPointsBar.healthPoints + StringConst.BACK_SLASH_N;
//        }
    }

    public void handlePackets() {
        log.trace("---");

//        if(duplicateCounter == duplicateLimit) {
//            duplicateCounter = 0;
//            getPackets();
//        }

        getPackets();
        handleResponse(receivedMessage);
//        if(receivedMessage != null && receivedMessage.contains("hp1=0")) dbg("hp1=0");
//        if(receivedMessage != null && receivedMessage.contains("hp2=0")) dbg("hp2=0");
//        ++duplicateCounter;
    }

    public void getPackets() {
        log.trace("---");

        try {
            receivedMessage = env.arenaUdpClientEngine.inputPacketsQueue.poll(1, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            Log4jUtil.logException(log, e);
        }

    }

    public void duplicateMovement() {
        log.trace("---");
    }

    public void dbg(String sourceString) {
		log.debug(sourceString);
    }
}
