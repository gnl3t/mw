package com.gnl3t.mw.arena.engine;


import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.constants.StringConst;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class LogEngine {
	private Environment env;
	private boolean enableTraceFlag = false;
	private boolean enableDebugPrintFlag = true;
	private String className = StringConst.SPACE_SYMBOL + StringConst.COLON_SYMBOL + StringConst.SPACE_SYMBOL;
	private String errorTag = "[ERROR]";
	private String debugTag = "[DEBUG]";
	private String traceTag = "[TRACE]";
	private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("HH:mm:ss.SSS ");


	public LogEngine(Class c) {
		this.className = StringConst.SPACE_SYMBOL + c.getCanonicalName() + StringConst.COLON_SYMBOL + StringConst.SPACE_SYMBOL;
	}

	public void init(Environment env) {
		this.env = env;
	}


//==================================================================public

	public void t1() {
//		11
	}

	public void error(String text) {
		err(text);
	}

	public void error(Exception e) {
		err(e);
	}

	public void trace(String text) {
		trace1(text);
	}

	public void debug(String text) {
		dbg(text);
	}

	public void debug(Exception e) {
		dbg(e);
	}

	public String buildStackTraceAsString(Exception e) {
		String stackTrace = "";
		for(StackTraceElement stackTraceElement : e.getStackTrace()) {
			stackTrace += "\n" + stackTraceElement.toString();
		}
		return stackTrace;
	}

	public String getCurrentDateString() {
		return LocalDateTime.now().format(dateFormatter);
	}



//==================================================================private

	private void t2() {
//		11
	}



//==================================================================dbg_print

	private void err(String text) {
		System.out.println(errorTag + getCurrentDateString() + className + text);
	}

	private void err(Exception e) {
		err(buildStackTraceAsString(e));
	}

	private void trace1(String text) {
		if(!enableTraceFlag) return;
		System.out.println(traceTag + getCurrentDateString() + className + text);
	}

	private void dbg(String text) {
		if(!enableDebugPrintFlag) return;
		System.out.println(debugTag + getCurrentDateString() + className + text);
	}

	private void dbg(Exception e) {
		dbg(buildStackTraceAsString(e));
	}

}
