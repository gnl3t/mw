package com.gnl3t.mw.arena.engine;


import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.ArenaScaleUtil;
import org.apache.log4j.Logger;


public class ArenaBotEngine {
	private final static Logger log = Logger.getLogger(ArenaBotEngine.class);
	private Environment env;


	public void init(Environment env) {
		log.trace("---");
		this.env = env;
	}

	public void move() {
		log.trace("---");

		if(!env.arenaJFrameEngine.enableBotFlag) return;

			//handle stamina
		if(env.botData.staminaNeedRegen) {
			handleBotBackForStaminaRegen();
			return;
		}

		//handle movement to player
//		if(!env.k1.collisionFlag) {
		if(checkActiveCollision()) {
			env.arenaKeyListener.handleVK_LEFTReleased();
		} else {
			env.arenaKeyListener.handleVK_ENTERReleased();
			env.arenaKeyListener.handleVK_LEFTPressed();
		}
	}

	private boolean checkActiveCollision() {
		return (env.k2.x - env.k1.x) <= ArenaScaleUtil.reScale(17, env);
	}

	public void attack() {
		log.trace("---");

		if(!env.arenaJFrameEngine.enableBotFlag) return;

//		if(!env.k1.collisionFlag) {
		if(checkActiveCollision()) {
			handleBotAttack();
		} else {
			env.arenaKeyListener.handleVK_ENTERReleased();
		}
	}

	private void handleBotAttack() {
		log.trace("---");

		//handle stamina
		if(env.k2.staminaBar.staminaPoints <= 0) {
			env.botData.staminaNeedRegen = true;
		}

		//handle attack
		if(!env.k2.damageDone) {
			env.arenaKeyListener.handleVK_ENTERPressed();
		} else {
			env.arenaKeyListener.handleVK_ENTERReleased();
		}
	}

	private void handleBotBackForStaminaRegen() {
		log.trace("---");
		if(env.k2.staminaBar.staminaPoints < env.k2.staminaBar.staminaPointsStartValue / 3) {
			env.arenaKeyListener.handleVK_BACK_SPACEPressed();
			env.arenaKeyListener.handleVK_RIGHTPressed();
		} else {
			env.botData.staminaNeedRegen = false;
			env.arenaKeyListener.handleVK_BACK_SPACEReleased();
			env.arenaKeyListener.handleVK_RIGHTReleased();
		}
	}
}
