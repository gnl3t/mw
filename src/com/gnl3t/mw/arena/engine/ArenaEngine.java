package com.gnl3t.mw.arena.engine;


import com.gnl3t.mw.arena.gui.ArenaJPanel;
import com.gnl3t.mw.arena.handler.ArenaEngineTimer;
import com.gnl3t.mw.data.construct.KnightData;
import com.gnl3t.mw.data.enums.MenuType;
import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.ArenaScaleUtil;
import com.gnl3t.mw.util.constants.ArenaConst;
import com.gnl3t.mw.util.constants.StringConst;
import org.apache.log4j.Logger;


public class ArenaEngine {
	private final static Logger log = Logger.getLogger(ArenaEngine.class);
	private Environment env;
	private ArenaEngineTimer arenaEngineTimer;


	public void init(Environment env) {
		log.trace("---");
		this.env = env;

		arenaEngineTimer = new ArenaEngineTimer();
		arenaEngineTimer.init(env);
	}


	public void stepCadre() {
		log.trace("---");

		printDbgStats();
		if(!(env.menuData.currentMenuType == MenuType.MAIN ||
				env.menuData.currentMenuType == MenuType.SETTINGS ||
				env.menuData.currentMenuType == MenuType.SOUND_SETTINGS)) {
			if(env.k2.playerDead) {
				if(env.menuData.currentMenuType != MenuType.VICTORY) env.arenaMenuEngine.goToArenaVictoryMenu();
			}
			if(env.k1.playerDead) {
				if(env.menuData.currentMenuType != MenuType.DEFEAT) env.arenaMenuEngine.goToArenaDefeatMenu();
			}
		}
		if(env.arenaJFrameEngine.pauseFlag) return;
//		log.debug("stepCadre");

		arenaEngineTimer.calcTimePassed();

//		if(env.arenaRender.preparedNextActionFlag) {
//			env.knightMovementEngine.calcMoving();
//			env.knightAttackEngine.calcAttack();
//			env.arenaBotEngine.move();
//
//			env.arenaRender.preparedNextActionFlag = false;
//		}
		env.knightMovementEngine.calcMoving();
//		if(!env.gameControlsData.playerLeft) {
//			env.k1.body.x = env.gameControlsReceivedData.x1;
//			env.k2.body.x = env.gameControlsReceivedData.x2;
//		}
		env.knightAttackEngine.calcAttack();
		env.arenaBotEngine.move();


//		if(!env.gameControlsData.playerLeft) {
//		if(true) {
//			env.k1.x = env.gameControlsReceivedData.x1;
//			env.k2.x = env.gameControlsReceivedData.x2;
//			env.k1.body.x = env.gameControlsReceivedData.x1;
//			env.k2.body.x = env.gameControlsReceivedData.x2;
//			env.k1.healthPointsBar.healthPoints = env.gameControlsReceivedData.hp1;
//			env.k2.healthPointsBar.healthPoints = env.gameControlsReceivedData.hp2;
//			env.k1.staminaBar.staminaPoints = env.gameControlsReceivedData.st1;
//			env.k2.staminaBar.staminaPoints = env.gameControlsReceivedData.st2;
//
//
//			if(env.k1.healthPointsBar.healthPoints <= 0) {
//				env.k1.playerDead = true;
//			}
//			if(env.k2.healthPointsBar.healthPoints <= 0) {
//				env.k2.playerDead = true;
//			}
//		}

		env.arenaRender.preparedNextActionFlag = false;
	}

	public void initKnights() {
		log.trace("---");

		env.k1 = new KnightData(false, env);
		env.k2 = new KnightData(true, env);


		env.knightAttackEngine.init(env);
		env.knightHealthEngine.init(env);
		env.knightMovementEngine.init(env);
		env.knightShieldEngine.init(env);
		env.knightStaminaEngine.init(env);
		env.knightWeaponEngine.init(env);

		setKnight2ConfigValues();

		if(env.gameControlsData.playerLeft) {
//			env.playerLeftString = "playerLeft\n";
			env.playerLeftString = "playerLeft";
		} else {
//			env.playerLeftString = "playerRight\n";
			env.playerLeftString = "playerRight";
		}
	}

	public void reset() {
		log.trace("---");

		initKnights();

//		env.k1.x = ArenaScaleUtil.reScale(1, env) + ArenaConst.ARENA_X_LIMIT_LEFT;
//		env.k2.x = ArenaScaleUtil.reScale(200, env) + ArenaConst.ARENA_X_LIMIT_LEFT;

		env.k1.x = ArenaScaleUtil.reScale(50, env) + ArenaConst.ARENA_X_LIMIT_LEFT;
		env.k2.x = ArenaScaleUtil.reScale(ArenaConst.ARENA_X_LIMIT_RIGHT, env);

		env.arenaJFrameEngine.setFrameTitle(ArenaJPanel.class.getSimpleName());
//		env.arenaJFrameEngine.pauseFlag = false;
	}

	public void setKnight2ConfigValues() {
		env.userArmoryEngine.setDefaultArmoryData();
		if(!ArenaJFrameEngine.reduceDamageFlag) {
			env.k2.weapon.damage = getIntKnight2ConfigValue(".weapon.damage");
			env.k2.shield.armor = getIntKnight2ConfigValue(".shield.armor");
		} else {
			env.k2.weapon.damage = 1;
			env.k2.shield.armor = 1;
		}
		env.arenaImagesEngine.initOpponentImages();
	}

	public int getIntKnight2ConfigValue(String text) {
		return Integer.parseInt(env.knightTypesConfig.getProperty(env.currentKnightType2.name().toLowerCase() + text));
	}

	private void printDbgStats() {
		log.trace("---");

		if(!env.arenaJFrameEngine.showKnightsStatsFlag) return;

		env.arenaJFrameEngine.k1xLabel.setText(StringConst.EMPTY_STRING + env.k1.body.x);
		env.arenaJFrameEngine.k1SpeedLabel.setText(StringConst.EMPTY_STRING + env.k1.xSpeed);
		env.arenaJFrameEngine.k1CollisionLabel.setText(StringConst.EMPTY_STRING + env.k1.collisionFlag);
		env.arenaJFrameEngine.k1DamageLabel.setText(StringConst.EMPTY_STRING + env.k1.weapon.damage);
		env.arenaJFrameEngine.k1ShieldArmorLabel.setText(StringConst.EMPTY_STRING + env.k1.shield.armor);
		env.arenaJFrameEngine.k1HealthLabel.setText(StringConst.EMPTY_STRING + env.k1.healthPointsBar.healthPoints);
		env.arenaJFrameEngine.k1StaminaLabel.setText(StringConst.EMPTY_STRING + env.k1.staminaBar.staminaPoints);
		env.arenaJFrameEngine.k1ShieldCoverLabel.setText(StringConst.EMPTY_STRING + env.k1.shieldCoverFlag);

		env.arenaJFrameEngine.k2xLabel.setText(StringConst.EMPTY_STRING + env.k2.body.x);
		env.arenaJFrameEngine.k2SpeedLabel.setText(StringConst.EMPTY_STRING + env.k2.xSpeed);
		env.arenaJFrameEngine.k2CollisionLabel.setText(StringConst.EMPTY_STRING + env.k2.collisionFlag);
		env.arenaJFrameEngine.k2DamageLabel.setText(StringConst.EMPTY_STRING + env.k2.weapon.damage);
		env.arenaJFrameEngine.k2ShieldArmorLabel.setText(StringConst.EMPTY_STRING + env.k2.shield.armor);
		env.arenaJFrameEngine.k2HealthLabel.setText(StringConst.EMPTY_STRING + env.k2.healthPointsBar.healthPoints);
		env.arenaJFrameEngine.k2StaminaLabel.setText(StringConst.EMPTY_STRING + env.k2.staminaBar.staminaPoints);
		env.arenaJFrameEngine.k2ShieldCoverLabel.setText(StringConst.EMPTY_STRING + env.k2.shieldCoverFlag);
	}
}
