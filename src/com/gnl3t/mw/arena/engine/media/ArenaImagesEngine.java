package com.gnl3t.mw.arena.engine.media;


import com.gnl3t.mw.data.enums.KnightType;
import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.ArenaScaleUtil;
import com.gnl3t.mw.util.Log4jUtil;
import com.gnl3t.mw.util.constants.ArenaConst;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.Image;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class ArenaImagesEngine {
	final static Logger log = Logger.getLogger(ArenaImagesEngine.class);
	private Environment env;

	public int lengthScaled60 = 60;
	public int lengthScaled50 = 50;

	public Image imageStand;
	public Image imageAttack;
	public Image imageShield;

	public Image imageScaledStand;
	public Image imageScaledLegsStand;
	public Image imageScaledLegsStep1;
	public Image imageScaledLegsStep2;
//	public Image imageScaledLegsStep3;
//	public Image imageScaledLegsStep4;
	public Image imageScaledAttack;
	public Image imageScaledShield;

	public Image image2Stand;
	public Image image2Attack;
	public Image image2Shield;

	public Image image2ScaledStand;
	public Image image2ScaledAttack;
	public Image image2ScaledShield;

	public Image background50;
	public Image background60;
	public Image background50Scaled;
	public Image background60Scaled;

	public List<Image> knightTypesImagesList;


	public void init(Environment env) {
		log.trace("---");
		this.env = env;
		//logger.info("Working Directory = " + System.getProperty("user.dir"));

		lengthScaled60 = ArenaScaleUtil.reScale(60, env);
		lengthScaled50 = ArenaScaleUtil.reScale(50, env);

		try {
			imageStand = ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "k1_stand" + ".png"));
//			imageAttack = ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "k1_attack" + ".png"));
//			imageShield = ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "k1_shield" + ".png"));
//			imageStand = ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "body2" + ".png"));
			imageAttack = ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "k1_attack" + ".png"));
			imageShield = ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "k1_shield" + ".png"));

			/*
//			imageScaledStand = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "k1_stand" + ".png")));
//			imageScaledAttack = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "k1_attack" + ".png")));
//			imageScaledShield = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "k1_shield" + ".png")));
//			imageScaledLegsStand = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "legs_stand1" + ".png")));
//			imageScaledLegsStep1 = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "legs_step1" + ".png")));
//			imageScaledLegsStep2 = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "legs_step2" + ".png")));
//			imageScaledLegsStep3 = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "legs_step3" + ".png")));
//			imageScaledLegsStep4 = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "legs_step4" + ".png")));
//			imageScaledLegsStep3 = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "swordsman" + "_legs_step3_grey" + ".png")));
//			imageScaledLegsStep3 = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "swordsman" + "_legs_step3_grey" + ".png")));
//			imageScaledLegsStep4 = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "swordsman" + "_legs_step2_grey" + ".png")));
			*/
//			imageScaledLegsStand = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "swordsman" + "_legs_stand_grey" + ".png")));
//			imageScaledLegsStep1 = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "swordsman" + "_legs_step1_grey" + ".png")));
//			imageScaledLegsStep2 = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "swordsman" + "_legs_step2_grey" + ".png")));


//			imageScaledLegsStand = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "swordsman" + "_legs_stand_grey" + ".png")));
//			imageScaledLegsStep1 = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "swordsman" + "_legs_step1_grey" + ".png")));
//			imageScaledLegsStep2 = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "swordsman" + "_legs_step2_grey" + ".png")));

//			imageScaledLegsStand = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "legioner" + "_legs_stand_grey" + ".png")));
//			imageScaledLegsStep1 = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "legioner" + "_legs_step1_grey" + ".png")));
//			imageScaledLegsStep2 = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "legioner" + "_legs_step2_grey" + ".png")));

//			imageScaledLegsStand = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "spartan" + "_legs_stand_grey" + ".png")));
//			imageScaledLegsStep1 = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "spartan" + "_legs_step1_grey" + ".png")));
//			imageScaledLegsStep2 = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "spartan" + "_legs_step2_grey" + ".png")));
//
			imageScaledLegsStand = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "longswordsman" + "_legs_stand_grey" + ".png")));
			imageScaledLegsStep1 = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "longswordsman" + "_legs_step1_grey" + ".png")));
			imageScaledLegsStep2 = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "longswordsman" + "_legs_step2_grey" + ".png")));


			imageScaledStand = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "body2" + ".png")));
			imageScaledAttack = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "k1_attack" + ".png")));
			imageScaledShield = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "k1_shield" + ".png")));

			background50 = ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "background50" + ".png"));
			background60 = ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "background60" + ".png"));
			background50Scaled = reScale50(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "background60" + ".png")));
			background60Scaled = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "background60" + ".png")));

			initOpponentImages();

//			loadKnightTypesImages();
		} catch (Exception e) {
			Log4jUtil.logException(log, e);
//			throw e;
		}
	}

	public void initOpponentImages() {
		log.trace("---");
		//logger.info("Working Directory = " + System.getProperty("user.dir"));

		try {
			String knightType;
			if(env.currentKnightType2 == KnightType.SWORDSMAN) {
				knightType = "k2";
			} else {
				knightType = env.currentKnightType2.name().toLowerCase() + "2";
			}
//			log.debug("knightType=" + knightType);
			image2Stand = ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + knightType + "_stand" + ".png"));
//			image2Stand = ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + knightType + "_stand_dbg" + ".png"));
			image2Attack = ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + knightType + "_attack" + ".png"));
			image2Shield = ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + knightType + "_shield" + ".png"));

			image2ScaledStand = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + knightType + "_stand" + ".png")));
			image2ScaledAttack = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + knightType + "_attack" + ".png")));
			image2ScaledShield = reScale60(ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + knightType + "_shield" + ".png")));
		} catch (Exception e) {
			Log4jUtil.logException(log, e);
		}
	}

	public Image reScale60(Image src) {
		return src.getScaledInstance(lengthScaled60, lengthScaled50, Image.SCALE_DEFAULT);
	}

	public Image reScale50(Image src) {
		return src.getScaledInstance(lengthScaled50, lengthScaled50, Image.SCALE_DEFAULT);
	}

	private void loadKnightTypesImages() throws Exception {
		log.trace("---");
		try {
			knightTypesImagesList = new ArrayList<>();
			for(KnightType e : KnightType.values()) {
//				String path = ArenaConst.BASIC_PATH_IMG + "knight_types/" + e.name().toLowerCase() + "_50.png";
//				log.debug("path=" + path);
				Image newImage = ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "knight_types/" + e.name().toLowerCase() + "_50.png"));
				knightTypesImagesList.add(newImage);
			}
//			Image newImage = ImageIO.read(new File("./img/knight_types/swordsman_50.png"));

//			imageScaledStand = ImageIO.read(new File(ArenaConst.BASIC_PATH_IMG + "k" + imageId + "_0.03" + ".png"));
		} catch (Exception e) {
			Log4jUtil.logException(log, e);
			throw e;
		}
	}
}
