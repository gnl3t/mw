package com.gnl3t.mw.arena.engine.media;


import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.Log4jUtil;
import com.gnl3t.mw.util.constants.ArenaConst;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import org.apache.log4j.Logger;

import java.io.File;


public class ArenaAudioEngine {
	final static Logger log = Logger.getLogger(ArenaAudioEngine.class);
	public Environment env;


	public double soundLevel = ArenaConst.SOUND_VOLUME_LEVEL;

	private MediaPlayer bodyHitMediaPlayer;
	private MediaPlayer shieldHitMediaPlayer;
	private MediaPlayer swordSwingMediaPlayer;


	public void init(Environment env) {
		log.trace("---");
		this.env = env;

		try {
			com.sun.javafx.application.PlatformImpl.startup(()->{});
		} catch (Exception e) {
			Log4jUtil.logException(log, e);
			throw e;
		}
	}

	public void changeSoundLevel(double newSoundLevel) {
		log.trace("---");

		soundLevel = newSoundLevel;
		playBodyHit();
	}

	private MediaPlayer initMediaPlayer(String pathToFile) {
		log.trace("---");

//		return new MediaPlayer(new Media(new File(pathToFile).toURI().toString()));
		MediaPlayer mediaPlayer = new MediaPlayer(new Media(new File(pathToFile).toURI().toString()));
		mediaPlayer.setVolume(soundLevel);

		return mediaPlayer;
	}

	public void playBodyHit() {
		log.trace("---");
		if(!env.arenaJFrameEngine.enableSoundsFlag) return;

		try {
			bodyHitMediaPlayer = initMediaPlayer("./resources/sound/body_hit1.mp3");
			bodyHitMediaPlayer.play();
		} catch (Exception e) {
			Log4jUtil.logException(log, e);
		}
	}

	public void playShieldHit() {
		log.trace("---");
		if(!env.arenaJFrameEngine.enableSoundsFlag) return;

		try {
			shieldHitMediaPlayer = initMediaPlayer("./resources/sound/shield_hit1.mp3");
			shieldHitMediaPlayer.play();
		} catch (Exception e) {
			Log4jUtil.logException(log, e);
		}
	}

	public void playSwordSwing() {
		log.trace("---");
		if(!env.arenaJFrameEngine.enableSoundsFlag) return;

		try {
			swordSwingMediaPlayer = initMediaPlayer("./resources/sound/sword_swing1.mp3");
			swordSwingMediaPlayer.play();
		} catch (Exception e) {
			Log4jUtil.logException(log, e);
		}
	}
}
