package com.gnl3t.mw.arena.engine;


import com.gnl3t.mw.arena.tcp.client.SocketChannelClient;
import com.gnl3t.mw.arena.tcp.client.TcpRequestsPollerRunnable;
import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.DataPacketValidatorUtil;
import com.gnl3t.mw.util.Log4jUtil;
import com.gnl3t.mw.util.constants.StringConst;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;


public class ArenaTcpClientEngine {
	private final static Logger log = Logger.getLogger(ArenaTcpClientEngine.class);
	private Environment env;
	private SocketChannelClient tcpClient;
	public BlockingQueue<String> inputPacketsQueue;
	public BlockingQueue<String> outputPacketsQueue;
	public BlockingQueue<String> parsedPacketsQueue;
	private Thread tcpRequestsPollerRunnableRunnable;
	private String currentMessage = StringConst.EMPTY_STRING;


	public void init(Environment env) {
		log.trace("---");
		this.env = env;
	}

	public void initTcpClient() {
		log.trace("---");

//		String host = "localhost";
		String host = "164.90.171.227";
		int port = 8090;
//		log.debug("host");
		tcpClient = new SocketChannelClient(host, port);
		inputPacketsQueue = new LinkedBlockingDeque<>();
        outputPacketsQueue = new LinkedBlockingDeque<>();
		parsedPacketsQueue = new LinkedBlockingDeque<>();
		try {
			tcpClient.connect();
//			while(true) {
//				env.arenaOnlinePacketsEngine.sendRequest(); //TODO - rework to separate thread
//			}
			//TODO - implement possibility to stop the thread
//			Thread tcpRequestsPollerRunnableRunnable = new Thread(new TcpRequestsPollerRunnable(env));
			tcpRequestsPollerRunnableRunnable = new Thread(new TcpRequestsPollerRunnable(env));
			tcpRequestsPollerRunnableRunnable.start();
		} catch (IOException e) {
			Log4jUtil.logException(log, e);
		}
	}

	public synchronized void putMessage(String contentString) {
		log.trace("---");

        try {
            outputPacketsQueue.put(contentString);
        } catch (InterruptedException e) {
            Log4jUtil.logException(log, e);
        }
    }

	public synchronized void send() {
		log.trace("---");

//        try {
////        	if(outputPacketsQueue.size() > 0) {
////				currentMessage = outputPacketsQueue.poll(10, TimeUnit.MILLISECONDS);
////				sendMessages(makePacket(currentMessage));
////			}
////			outputPacketsQueue.put("request");
//			outputPacketsQueue.put("request");
////			outputPacketsQueue.put("request");
//        	for(int i = 0; i < outputPacketsQueue.size(); i++) {
////				currentMessage = outputPacketsQueue.poll(1, TimeUnit.MILLISECONDS);
////				currentMessage = outputPacketsQueue.poll(10, TimeUnit.MICROSECONDS);
////				currentMessage = outputPacketsQueue.poll(1, TimeUnit.MICROSECONDS);
//				currentMessage = outputPacketsQueue.poll(1, TimeUnit.NANOSECONDS);
////				sendMessages(makePacket(currentMessage));
//				sendMessages(currentMessage);
//			}
//        } catch (InterruptedException e) {
//            Log4jUtil.logException(log, e);
//        }
	}

	public synchronized void send(String contentString) {
		log.trace("---");
		if(tcpClient == null) return;

//		log.debug("contentString=" + contentString);
//		String response = tcpClient.sendMessages(contentString);
//		tcpClient.sendMessages(contentString);
//		tcpClient.sendMessages(makePacket(contentString));
		handleResponse(tcpClient.sendMessage(makePacket(contentString)));
//		log.debug("response=" + response);
	}

	private synchronized String makePacket(String contentString) {
		log.trace("---");
//		return StringConst.BEGIN + contentString + StringConst.END;
		return StringConst.BEGIN + env.playerLeftString + contentString + StringConst.END;
	}

	private synchronized void handleResponse(String contentString) {
		log.trace("---");

		log.debug("DataPacketValidatorUtil.validate(contentString)=" + DataPacketValidatorUtil.validate(contentString));
		if(DataPacketValidatorUtil.validate(contentString)) {
			log.debug("DataPacketValidatorUtil.validate(contentString)=true");
//			env.arenaOnlinePacketsEngine.handleResponse(contentString);
			if(contentString.length() == 0) return;
			if(contentString.contains(StringConst.LOG_SEPARATOR)) return;

			try {
				inputPacketsQueue.put(contentString);
			} catch (InterruptedException e) {
//				e.printStackTrace();
				Log4jUtil.logException(log, e);
			}
		} else {
			log.debug("DataPacketValidatorUtil.validate(contentString)=false");
		}
	}
}
