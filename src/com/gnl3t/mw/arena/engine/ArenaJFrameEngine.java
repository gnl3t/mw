package com.gnl3t.mw.arena.engine;


import com.gnl3t.mw.data.enums.KnightType;
import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.ArenaScaleUtil;
import com.gnl3t.mw.util.SwingUtil;
import com.gnl3t.mw.util.constants.ArenaConst;
import com.gnl3t.mw.util.constants.StringConst;
import org.apache.log4j.Logger;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class ArenaJFrameEngine {
	private final static Logger log = Logger.getLogger(ArenaJFrameEngine.class);
	private Environment env;
	public JPanel arenaJPanel;
	public JPanel currentMenuJPanel = new JPanel();
	public CustomEventHandler customEventHandler;
	public CustomChangeListener customChangeListener;


	public boolean pauseFlag = true;
	public boolean onlineFlag = false;
	public boolean onlineP2PFlag = false;
	public static boolean localHostFlag = true;
	public static boolean reduceDamageFlag = false;
	public static boolean use2ClientsFlag = false;
	public static boolean goToOnlineInstantlyFlag = false;

	public boolean showKeyboardPressedKeysFlag = true;
	public boolean showKnightsStatsFlag = true;
	public boolean enableBotFlag = false;
	public boolean enableSoundsFlag = true;

	public JLabel scaleLabel;
	public JLabel fpsLabel;

	public JLabel k1xLabel;
	public JLabel k1SpeedLabel;
	public JLabel k1CollisionLabel;
	public JLabel k1DamageLabel;
	public JLabel k1HealthLabel;
	public JLabel k1StaminaLabel;
	public JLabel k1ShieldCoverLabel;
	public JLabel k1ShieldArmorLabel;

	public JLabel k2xLabel;
	public JLabel k2SpeedLabel;
	public JLabel k2CollisionLabel;
	public JLabel k2DamageLabel;
	public JLabel k2HealthLabel;
	public JLabel k2StaminaLabel;
	public JLabel k2ShieldCoverLabel;
	public JLabel k2ShieldArmorLabel;

	public JLabel vkDLabel = SwingUtil.createTextLabel("D");
	public JLabel vkFLabel = SwingUtil.createTextLabel("F");
	public JLabel vkLeftLabel = SwingUtil.createTextLabel("Left");
	public JLabel vkRightLabel = SwingUtil.createTextLabel("Right");
	public JLabel vkCLabel = SwingUtil.createTextLabel("C");
	public JLabel vkBLabel = SwingUtil.createTextLabel("B");
	public JLabel vkBACKSPACELabel = SwingUtil.createTextLabel("BACKSPACE");
	public JLabel vkDownLabel = SwingUtil.createTextLabel("Down");
	public JLabel vkSPACELabel = SwingUtil.createTextLabel("SPACE");
	public JLabel vkENTERLabel = SwingUtil.createTextLabel("ENTER");

//    public JLabel dbgLog1;
    public JTextArea nickNameTextArea;
    private JTextArea dbgLog1;
    private JTextArea dbgLog2;

	public String debugNickName = "abc";
	private String debugText1 = "dbgLog1\n";
	private String debugText2 = "dbgLog2\n";
	private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("HH:mm:ss.SSS ");


	public void initFrame(JPanel jPanel, Environment env) {
		log.trace("---");
		this.env = env;
		arenaJPanel = jPanel;


		customEventHandler = new CustomEventHandler();
		customChangeListener = new CustomChangeListener();
		jPanel.setFocusable(true);
		jPanel.requestFocusInWindow();


		if(env.arenaFrame == null) {
			env.arenaFrame = new JFrame("");
			env.arenaFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			env.frameWidth = 400 + ArenaScaleUtil.reScale(ArenaConst.ARENA_WIDTH, env);
			env.frameHeight = 500 + ArenaScaleUtil.reScale(ArenaConst.ARENA_HEIGHT, env);
			env.arenaFrame.setSize(env.frameWidth, env.frameHeight);
		}
		env.arenaFrame.add(jPanel);


		addControlPanel();
		addLeftStatsPanel();
		addRightStatsPanel();
		addBottomPanel();

		setLocation(env);
	}


	public void setLocation(Environment env) {
		if(!env.arenaJFrameEngine.use2ClientsFlag) {
			env.arenaFrame.setLocationRelativeTo(null);//center
		}
		else {
			if(env.gameControlsData.playerLeft) {
				env.arenaFrame.setLocation(50, 100);
			} else {
				env.arenaFrame.setLocation(950, 100);
			}
		}

		env.arenaFrame.setVisible(true);
	}

	private void addControlPanel() {
		JPanel panel = new JPanel();
		int columnsQuantity = 7;
		int rowsQuantity = 4;
		panel.setPreferredSize(new Dimension(env.frameWidth, 100));
		panel.setLayout(new GridLayout(rowsQuantity, columnsQuantity));

//		addButton("restart", panel);
//		addButton("pause", panel);
		scaleLabel = SwingUtil.createTextLabel("scale=" + env.screenDensity);
		panel.add(scaleLabel);
		addButton("connect", panel);
		fpsLabel = SwingUtil.createTextLabel("fps=0");
		panel.add(fpsLabel);
//		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
//		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		addButton("next opponent", panel);
		for(int x = 5; x < columnsQuantity - 2; x++) {
			panel.add(SwingUtil.createTextLabel(debugText(1 + "_dbg_" + x), null, Color.GRAY));
		}
		addCheckBox("fields", env.arenaRender.allowFields, panel);
		addCheckBox("show keys", showKeyboardPressedKeysFlag, panel);


		addButton("scale 1.0", panel);
		addButton("scale 1.2", panel);
		addButton("scale 2.0", panel);
		addButton("scale 2.5", panel);
		addButton("scale 5.0", panel);
//		addButton("scale 4.0", panel);
		for(int x = 5; x < columnsQuantity - 2; x++) {
			panel.add(SwingUtil.createTextLabel(debugText(1 + "_dbg_" + x), null, Color.GRAY));
		}
		addCheckBox("images", env.arenaRender.allowImages, panel);
		addCheckBox("knights stats", showKnightsStatsFlag, panel);

		addButton("swordsman", panel);
		addButton("viking", panel);
		addButton("saracen", panel);
		addButton("legioner", panel);
		addButton("spartan", panel);
//		for(int x = 5; x < columnsQuantity - 2; x++) {
//			panel.add(SwingUtil.createTextLabel(debugText(1 + "_dbg_" + x), null, Color.GRAY));
//		}
		addCheckBox("sounds", enableSoundsFlag, panel);
		addCheckBox("bot enable", enableBotFlag, panel);

		addButton("longswordsman", panel);
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
//		addButton("samurai", panel);
//		addButton("knight", panel);
        panel.add(SwingUtil.createTextLabel("nick="));
//        panel.add(SwingUtil.createTextLabel("abc"));
        nickNameTextArea = new JTextArea(debugNickName);
        panel.add(nickNameTextArea);
//        nickNameTextArea.setLineWrap(true);
//        JScrollPane dbgNickNameScrollPane = new JScrollPane(nickNameTextArea);
//        nickNameTextArea.setCaretPosition(debugNickName.length());
//        panel.add(dbgNickNameScrollPane);
		for(int x = 5; x < columnsQuantity; x++) {
			panel.add(SwingUtil.createTextLabel(debugText(1 + "_dbg_" + x), null, Color.GRAY));
		}

		env.arenaFrame.add(panel, BorderLayout.NORTH);
	}

	private void addLeftStatsPanel() {
		JPanel panel = new JPanel();
		int columnsQuantity = 3;
		int rowsQuantity = 10;
		int usedRowsQuantity = 9;
		panel.setPreferredSize(new Dimension(200, env.frameHeight));
		panel.setLayout(new GridLayout(rowsQuantity, columnsQuantity));

		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));

		panel.add(SwingUtil.createTextLabel("x"));
		k1xLabel = SwingUtil.createTextLabel(StringConst.EMPTY_STRING);
		panel.add(k1xLabel);
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));

		panel.add(SwingUtil.createTextLabel("speed"));
		k1SpeedLabel = SwingUtil.createTextLabel(StringConst.EMPTY_STRING);
		panel.add(k1SpeedLabel);
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));

		panel.add(SwingUtil.createTextLabel("collision"));
		k1CollisionLabel = SwingUtil.createTextLabel(StringConst.EMPTY_STRING);
		panel.add(k1CollisionLabel);
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));

		panel.add(SwingUtil.createTextLabel("damage"));
		k1DamageLabel = SwingUtil.createTextLabel(StringConst.EMPTY_STRING);
		panel.add(k1DamageLabel);
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));

		panel.add(SwingUtil.createTextLabel("armor"));
		k1ShieldArmorLabel = SwingUtil.createTextLabel(StringConst.EMPTY_STRING);
		panel.add(k1ShieldArmorLabel);
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));

		panel.add(SwingUtil.createTextLabel("health"));
		k1HealthLabel = SwingUtil.createTextLabel(StringConst.EMPTY_STRING);
		panel.add(k1HealthLabel);
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));

		panel.add(SwingUtil.createTextLabel("stamina"));
		k1StaminaLabel = SwingUtil.createTextLabel(StringConst.EMPTY_STRING);
		panel.add(k1StaminaLabel);
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));

		panel.add(SwingUtil.createTextLabel("shieldUp"));
		k1ShieldCoverLabel = SwingUtil.createTextLabel(StringConst.EMPTY_STRING);
		panel.add(k1ShieldCoverLabel);
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));

		for(int y = 0; y < rowsQuantity - usedRowsQuantity; y++) {
			for(int x = 0; x < columnsQuantity; x++) {
				panel.add(SwingUtil.createTextLabel(debugText(1 + "_dbg_" + x), null, Color.GRAY));
			}
		}

		env.arenaFrame.add(panel, BorderLayout.WEST);
	}

	private void addRightStatsPanel() {
		JPanel panel = new JPanel();
		int columnsQuantity = 3;
		int rowsQuantity = 10;
		int usedRowsQuantity = 9;
		panel.setPreferredSize(new Dimension(200, env.frameHeight));
		panel.setLayout(new GridLayout(rowsQuantity, columnsQuantity));

		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));

		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel("x"));
		k2xLabel = SwingUtil.createTextLabel(StringConst.EMPTY_STRING);
		panel.add(k2xLabel);

		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel("speed"));
		k2SpeedLabel = SwingUtil.createTextLabel(StringConst.EMPTY_STRING);
		panel.add(k2SpeedLabel);

		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel("collision"));
		k2CollisionLabel = SwingUtil.createTextLabel(StringConst.EMPTY_STRING);
		panel.add(k2CollisionLabel);

		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel("damage"));
		k2DamageLabel = SwingUtil.createTextLabel(StringConst.EMPTY_STRING);
		panel.add(k2DamageLabel);

		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel("armor"));
		k2ShieldArmorLabel = SwingUtil.createTextLabel(StringConst.EMPTY_STRING);
		panel.add(k2ShieldArmorLabel);

		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel("health"));
		k2HealthLabel = SwingUtil.createTextLabel(StringConst.EMPTY_STRING);
		panel.add(k2HealthLabel);

		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel("stamina"));
		k2StaminaLabel = SwingUtil.createTextLabel(StringConst.EMPTY_STRING);
		panel.add(k2StaminaLabel);

		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel("shieldUp"));
		k2ShieldCoverLabel = SwingUtil.createTextLabel(StringConst.EMPTY_STRING);
		panel.add(k2ShieldCoverLabel);

		for(int y = 0; y < rowsQuantity - usedRowsQuantity; y++) {
			for(int x = 0; x < columnsQuantity; x++) {
				panel.add(SwingUtil.createTextLabel(debugText(1 + "_dbg_" + x), null, Color.GRAY));
			}
		}

		env.arenaFrame.add(panel, BorderLayout.EAST);
	}

	public void addBottomPanel() {
		JPanel panel = new JPanel();
		int columnsQuantity = 1;
		int rowsQuantity = 2;
		panel.setPreferredSize(new Dimension(env.frameWidth, 400));
		panel.setLayout(new GridLayout(rowsQuantity, columnsQuantity));

		panel.add(createBottomStatsPanel());
		panel.add(createMenuContainerPanel());


		env.arenaFrame.add(panel, BorderLayout.SOUTH);
	}

	private JPanel createMenuContainerPanel() {
		JPanel panel = new JPanel();
		int columnsQuantity = 3;
		int rowsQuantity = 1;
		panel.setPreferredSize(new Dimension(env.frameWidth, 100));
		panel.setLayout(new GridLayout(rowsQuantity, columnsQuantity));

//		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
//        dbgLog1 = SwingUtil.createTextLabel("dbgLog1");
//        panel.add(dbgLog1);
		dbgLog1 = new JTextArea(debugText1);
		dbgLog1.setLineWrap(true);
        JScrollPane dbgLog1ScrollPane = new JScrollPane(dbgLog1);
		dbgLog1.setCaretPosition(debugText1.length());
        panel.add(dbgLog1ScrollPane);

		env.arenaMenuEngine.calcMenu();
		panel.add(currentMenuJPanel);

//		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		dbgLog2 = new JTextArea(debugText2);
		dbgLog2.setLineWrap(true);
		JScrollPane dbgLog2ScrollPane = new JScrollPane(dbgLog2);
//		dbgLog2ScrollPane.getVerticalScrollBar().setValue(dbgLog2ScrollPane.getVerticalScrollBar().getMaximum());
//		JScrollBar scrollBar2 = dbgLog2ScrollPane.getVerticalScrollBar();
//		scrollBar2.setValue(scrollBar2.getMaximum());

//		dbgLog2.setCaretPosition(dbgLog2.getDocument().getLength());
		dbgLog2.setCaretPosition(debugText2.length());

		panel.add(dbgLog2ScrollPane);

		return panel;
	}

	private JPanel createBottomStatsPanel() {
		JPanel panel = new JPanel();
		int columnsQuantity = 8;
		int rowsQuantity = 7;
		int usedRowsQuantity = 4;
		panel.setPreferredSize(new Dimension(env.frameWidth, 100));
		panel.setLayout(new GridLayout(rowsQuantity, columnsQuantity));

		panel.add(vkDLabel);
		panel.add(vkFLabel);
		for(int x = 2; x < columnsQuantity - 2; x++) {
			panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		}
		panel.add(vkLeftLabel);
		panel.add(vkRightLabel);

		panel.add(vkCLabel);
		panel.add(vkBLabel);
		for(int x = 2; x < columnsQuantity - 2; x++) {
			panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		}
		panel.add(vkBACKSPACELabel);
		panel.add(vkDownLabel);

		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(vkSPACELabel);
		for(int x = 2; x < columnsQuantity - 2; x++) {
			panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		}
		panel.add(vkENTERLabel);
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));

		for(int x = 0; x < columnsQuantity; x++) {
			panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		}

		for(int y = 0; y < rowsQuantity - usedRowsQuantity; y++) {
			for(int x = 0; x < columnsQuantity; x++) {
				panel.add(SwingUtil.createTextLabel(debugText(1 + "_dbg_" + x), null, Color.GRAY));
			}
		}

		return panel;
	}

	public void setFrameTitle(String text) {
		log.trace("---");
		env.arenaFrame.setTitle(text);
	}

//	public void reset() {
//		log.trace("---");
//
////		env.arenaMenuEngine.goToArenaButtonsMenu();
////		env.arenaEngine.reset();
////		pauseFlag = false;
//	}

//	public void pause() {
//		log.trace("---");
//		pauseFlag = !pauseFlag;
//	}

	public void nextOpponent() {
		log.trace("---");

		if(env.arenaJFrameEngine.onlineFlag) {
			env.arenaMenuEngine.goToArenaButtonsMenu();
			return;
		}

		env.arenaMenuEngine.goToArenaButtonsMenu();
		if(env.k1.playerDead) {
			swordsman();
//			reset();
			return;
		}

		int nextOpponentOrdinal = env.currentKnightType2.ordinal() + 1;
		if(nextOpponentOrdinal == KnightType.values.length) nextOpponentOrdinal = 0;
		env.currentKnightType2 = KnightType.values[nextOpponentOrdinal];
		env.arenaEngine.setKnight2ConfigValues();

		int knight1HealthPoints = env.k1.healthPointsBar.healthPoints;

//		reset();
//		handleKnight1HealthBar(knight1HealthPoints);
	}

	public void scale(String srcText) {
		log.trace("---");

		float scale = Float.valueOf(srcText.replaceAll("scale ", StringConst.EMPTY_STRING));
//		dbg("" + scale);
//		scaleLabel.setText("scale=" + scale);
		env.screenDensity = scale;
		if(scale == 1.0f) {
			env.arenaRender.allowScaledFields = false;
			env.arenaRender.allowScaledImages = false;
		} else {
			env.arenaRender.allowScaledFields = true;
			env.arenaRender.allowScaledImages = true;
		}
		env.arenaMenuEngine.goToArenaButtonsMenu();
//		initFrame(arenaJPanel, env);
		env.arenaImagesEngine.init(env);
//		env.arenaAudioEngine.init(env);
	}

	public void reloadJFrame() {
		log.trace("---");

		JPanel contentPane = (JPanel) env.arenaFrame.getContentPane();
		contentPane.removeAll();
		contentPane.revalidate();
		contentPane.repaint();

		initFrame(arenaJPanel, env);
	}

	private void handleKnight1HealthBar(int knight1HealthPoints) {
		log.trace("---");
		if(knight1HealthPoints < env.k1.healthPointsBar.healthPointsStartValue) {
			env.k1.healthPointsBar.healthPoints = knight1HealthPoints + (env.k1.healthPointsBar.healthPointsStartValue - knight1HealthPoints) / 2;
			if(env.k1.healthPointsBar.healthPoints > env.k1.healthPointsBar.healthPointsStartValue) {
				env.k1.healthPointsBar.healthPoints = env.k1.healthPointsBar.healthPointsStartValue;
			}
		}
	}

	public void swordsman() {
		log.trace("---");
		env.currentKnightType2 = KnightType.SWORDSMAN;
		env.arenaEngine.setKnight2ConfigValues();
	}

	public void viking() {
		log.trace("---");
		env.currentKnightType2 = KnightType.VIKING;
		env.arenaEngine.setKnight2ConfigValues();
	}

	public void saracen() {
		log.trace("---");
		env.currentKnightType2 = KnightType.SARACEN;
		env.arenaEngine.setKnight2ConfigValues();
	}

	public void legioner() {
		log.trace("---");
		env.currentKnightType2 = KnightType.LEGIONER;
		env.arenaEngine.setKnight2ConfigValues();
	}

	public void spartan() {
		log.trace("---");
		env.currentKnightType2 = KnightType.SPARTAN;
		env.arenaEngine.setKnight2ConfigValues();
	}

	public void longswordsman() {
		log.trace("---");
		env.currentKnightType2 = KnightType.LONGSWORDSMAN;
		env.arenaEngine.setKnight2ConfigValues();
	}

//	public void samurai() {
//		log.trace("---");
//		env.currentKnightType2 = KnightType.SAMURAI;
//		env.arenaEngine.setKnight2ConfigValues();
//	}

//	public void fullArmorKnight() {
//		log.trace("---");
//		env.currentKnightType2 = KnightType.FULL_ARMOR_KNIGHT;
//		env.arenaEngine.setKnight2ConfigValues();
//	}

	private void allowFields() {
		log.trace("---");
		env.arenaRender.allowFields = !env.arenaRender.allowFields;
		env.arenaEngine.setKnight2ConfigValues();
	}

	private void allowImages() {
		log.trace("---");
		env.arenaRender.allowImages = !env.arenaRender.allowImages;
		env.arenaEngine.setKnight2ConfigValues();
	}

	private void allowSounds() {
		log.trace("---");
		enableSoundsFlag = !enableSoundsFlag;
	}

	private void showKeyboardPressedKeys() {
		log.trace("---");
		showKeyboardPressedKeysFlag = !showKeyboardPressedKeysFlag;
		env.arenaEngine.setKnight2ConfigValues();
	}

	private void showKnightsStats() {
		log.trace("---");
		showKnightsStatsFlag = !showKnightsStatsFlag;
		env.arenaEngine.setKnight2ConfigValues();
	}

	private void enableBot() {
		log.trace("---");
		enableBotFlag = !enableBotFlag;
		if(!enableBotFlag) {
			env.arenaKeyListener.handleVK_LEFTReleased();
		}
	}

	private void addButton(String text, JPanel panel) {
		JButton button = new JButton(text);
		button.setFocusable(false);
		button.addActionListener(customEventHandler);
		panel.add(button);
	}

	private void addCheckBox(String text, boolean value, JPanel panel) {
		JCheckBox checkBox = new JCheckBox(text);
		checkBox.setFocusable(false);
		checkBox.setSelected(value);
		checkBox.addActionListener(customEventHandler);
		panel.add(checkBox);
	}

	private String debugText(String text) {
		return env.debugFlag ? text : "";
	}


//==================================================================ActionListener


	private class CustomEventHandler implements ActionListener {
		public void actionPerformed(ActionEvent e){
//			log.debug("actionPerformed");
			Object o = e.getSource();

			if(o instanceof JButton) {
				JButton component = (JButton) o;
				String componentText = component.getText();
				String componentTooltipText = component.getToolTipText();
				if(componentTooltipText == null) componentTooltipText = "";
				dbg(componentText + componentTooltipText + " pressed");

//==================================================================control buttons
				if(componentText.equalsIgnoreCase("restart round")) {
					env.arenaMenuEngine.goToArenaButtonsMenu();
				} else if(componentText.equalsIgnoreCase("pause")) {
					env.arenaMenuEngine.goToArenaPauseMenu();
				} else if(componentText.equalsIgnoreCase("resume")) {
					env.arenaMenuEngine.resume();
				} else if(componentText.equalsIgnoreCase("next opponent")) {
					nextOpponent();
				} else if(componentText.contains("scale")) {
					scale(componentText);
				} else if(componentText.contains("connect")) {
//					env.arenaTcpClientEngine.initTcpClient();
					env.arenaUdpClientEngine.initUdpClient();
				}

//==================================================================menu
				else if(componentText.equalsIgnoreCase("Arena")) {
					env.arenaMenuEngine.goToArenaButtonsMenu();
				} else if(componentText.equalsIgnoreCase("Main menu")) {
					env.arenaMenuEngine.goToMainMenu();
				} else if(componentText.equalsIgnoreCase("Settings")) {
					env.arenaMenuEngine.goToSettingsMenu();
				} else if(componentText.equalsIgnoreCase("Online(beta)")) {
					env.arenaMenuEngine.goToOnlineBetaMenu();
				} else if(componentText.equalsIgnoreCase("back")) {
					env.arenaMenuEngine.backToPreviousMenu();
				}

//==================================================================online_beta
				else if(componentText.equalsIgnoreCase("Connect to server")) {
					env.arenaOnlineBetaEngine.connectToServer();
				} else if(componentText.equalsIgnoreCase("get opponent info")) {
					env.arenaOnlineBetaEngine.getOpponentInfo();
				} else if(componentText.equalsIgnoreCase("get client addresses")) {
					env.arenaOnlineBetaEngine.getClientAddresses();
				}

//==================================================================knight types
				else if(componentText.equalsIgnoreCase("swordsman")) {
					swordsman();
				} else if(componentText.equalsIgnoreCase("viking")) {
					viking();
				} else if(componentText.equalsIgnoreCase("saracen")) {
					saracen();
				} else if(componentText.equalsIgnoreCase("legioner")) {
					legioner();
				} else if(componentText.equalsIgnoreCase("spartan")) {
					spartan();
				} else if(componentText.equalsIgnoreCase("longswordsman")) {
					longswordsman();
				} else if(componentText.equalsIgnoreCase("samurai")) {
//					samurai();
				} else if(componentText.equalsIgnoreCase("knight")) {
//					fullArmorKnight();
				}
			} else if(o instanceof JCheckBox) {
				JCheckBox component = (JCheckBox) o;
				String componentText = component.getText();
				String componentTooltipText = component.getToolTipText();
				if(componentTooltipText == null) componentTooltipText = "";
				dbg(componentText + componentTooltipText + " pressed");

				if(componentText.equalsIgnoreCase("show keys")) {
					showKeyboardPressedKeys();
				} else if(componentText.equalsIgnoreCase("knights stats")) {
					showKnightsStats();
				} else if(componentText.equalsIgnoreCase("bot enable")) {
					enableBot();
				} else if(componentText.equalsIgnoreCase("fields")) {
					allowFields();
				} else if(componentText.equalsIgnoreCase("images")) {
					allowImages();
				} else if(componentText.equalsIgnoreCase("sounds")) {
					allowSounds();
				}
			}
		}
	}



//==================================================================ChangeListener

	private class CustomChangeListener implements ChangeListener {

		public void stateChanged(ChangeEvent e){
			JSlider jSlider = (JSlider) e.getSource();
			if (!jSlider.getValueIsAdjusting()) {
				if(jSlider.getName().equalsIgnoreCase("sound")) {
					double soundLevel = Double.valueOf(jSlider.getValue()) / 100;
//					log.debug("soundLevel=" + soundLevel);
					env.arenaAudioEngine.changeSoundLevel(soundLevel);
				}
			}
		}
	}

	public void dbg(String text) {
		log.debug(text);
//		dbg1(text);
		dbg2(text);
	}

	public void dbg1(String text) {
		debugText1 += LocalDateTime.now().format(dateFormatter) + text + "\n";
		dbgLog1.setText(debugText1);
	}

	public void dbg2(String text) {
		debugText2 += LocalDateTime.now().format(dateFormatter) + text + "\n";
		dbgLog2.setText(debugText2);
	}

}
