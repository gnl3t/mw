package com.gnl3t.mw.arena.engine;


import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.constants.StringConst;
import org.apache.log4j.Logger;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;


public class ArenaP2POnlinePacketsEngine {
	private final static Logger log = Logger.getLogger(ArenaOnlinePacketsEngine.class);
	private Environment env;
	private BlockingQueue<String> packetsQueue;
	private String currentMessage = StringConst.EMPTY_STRING;
	private String sendMessage = StringConst.EMPTY_STRING;

	private boolean bothPlayersConnectedFlag = false;
	private boolean player1ConnectedFlag = false;
	private boolean player2ConnectedFlag = false;
	private boolean player1FinalizedFlag = false;
	private boolean player2FinalizedFlag = false;

	private boolean player1SentFlag = true;
	private boolean player2SentFlag = false;

	public int counter;


	public void init(Environment env) {
		log.trace("---");
		this.env = env;
		packetsQueue = new LinkedBlockingDeque<>();
	}

    public synchronized String handlePacket(String contentString) throws InterruptedException {
		if(!bothPlayersConnectedFlag) if(checkBothPlayersConnected(contentString)) return StringConst.LOG_SEPARATOR;
		if(!contentString.contains("request")) return putMessageToQueue(contentString);


		if(player2SentFlag && contentString.contains("playerLeft")) {
			currentMessage = sendMessage;
			player1SentFlag = true;
			player2SentFlag = false;
		} else if(player1SentFlag && contentString.contains("playerRight")) {
            handlePlayer2MessageLogic();

			sendMessage = currentMessage;
			player1SentFlag = false;
            player2SentFlag = true;
		} else {
			currentMessage = StringConst.LOG_SEPARATOR;
		}
		return currentMessage;
    }

    private void handlePlayer2MessageLogic() throws InterruptedException {
        if(packetsQueue.size() > 0) {
            currentMessage = StringConst.EMPTY_STRING;
            counter = 0;
            for(int i = 0; i < packetsQueue.size(); i++) {
                if(counter++ < 5) {
                    currentMessage += packetsQueue.poll(1, TimeUnit.NANOSECONDS) + StringConst.DELIMITER_SYMBOL;
                }
            }
            currentMessage = currentMessage.substring(0, currentMessage.length() - 3);
        } else {
            currentMessage = StringConst.LOG_SEPARATOR;
        }
	}

    private String putMessageToQueue(String contentString) throws InterruptedException {
		packetsQueue.put(contentString);
		checkBothPlayersFinalized(contentString);
        return StringConst.LOG_SEPARATOR;
    }

    public void clearQueue() throws InterruptedException {
		dbg("packetsQueue.size()=" + packetsQueue.size());
        packetsQueue.clear();
		dbg("packetsQueue.size()=" + packetsQueue.size());
		releaseButtons();
		releaseButtons();
		releaseButtons();
    }

	private synchronized boolean checkBothPlayersConnected(String contentString) throws InterruptedException {
		log.trace("---");

		if(!player1ConnectedFlag) player1ConnectedFlag = contentString.contains("playerLeft");
		if(!player2ConnectedFlag) player2ConnectedFlag = contentString.contains("playerRight");
		if(player1ConnectedFlag && player2ConnectedFlag) {
			bothPlayersConnectedFlag = true;
			player1FinalizedFlag = false;
			player2FinalizedFlag = false;
			clearQueue();
		}

		dbg("bothPlayersConnectedFlag=" + bothPlayersConnectedFlag);

		return bothPlayersConnectedFlag;
	}

	private synchronized void checkBothPlayersFinalized(String contentString) {
		log.trace("---");

		if(!player1FinalizedFlag) {
			player1FinalizedFlag = contentString.contains("playerLeft") && contentString.contains("finalized");
		}
		if(!player2FinalizedFlag) {
			player2FinalizedFlag = contentString.contains("playerRight") && contentString.contains("finalized");
		}
		if(player1FinalizedFlag && player2FinalizedFlag) {
			player1FinalizedFlag = false;
			player2FinalizedFlag = false;
			player1ConnectedFlag = false;
			player2ConnectedFlag = false;
			bothPlayersConnectedFlag = false;
//			clearQueue();

			dbg("bothPlayersConnectedFlag=" + bothPlayersConnectedFlag);
		}
	}

	private void releaseButtons() throws InterruptedException {
		putMessageToQueue("BEGIN" +
				"right1Released" +
				"left1Released" +
				"weapon1Released" +
				"shield1Released" +
				"right2Released" +
				"left2Released" +
				"weapon2Released" +
				"shield2Released" +
				"END");
	}


//==================================================================util methods

	private void dbg(String sourceString) {
		log.debug(sourceString);
	}
}
