package com.gnl3t.mw.arena.engine;


import com.gnl3t.mw.data.arena.GameControlsReceivedData;
import com.gnl3t.mw.data.enums.MenuType;
import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.SwingUtil;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.*;


public class ArenaMenuEngine {
	private final static Logger log = Logger.getLogger(ArenaMenuEngine.class);
	private Environment env;


	public void init(Environment env) {
		log.trace("---");
		this.env = env;
	}

	public void calcMenu() {
//		if(env.arenaUdpClientEngine.udpRequestsPollerRunnable != null) env.arenaUdpClientEngine.udpRequestsPollerRunnable.finalizeRun(env);

		if(env.menuData.currentMenuType == MenuType.MAIN) {
			createMainMenuPanel();
		} else if(env.menuData.currentMenuType == MenuType.ARENA_BUTTONS) {
			createArenaButtonsMenuPanel();
		}
//		else if(env.menuData.currentMenuType == MenuType.ARENA_BUTTONS_ONLINE_BETA) {
//			createArenaButtonsMenuPanel(); //same as for ARENA_BUTTONS
//		}
		else if(env.menuData.currentMenuType == MenuType.VICTORY) {
			createArenaVictoryMenuPanel();
		} else if(env.menuData.currentMenuType == MenuType.DEFEAT) {
			createArenaDefeatMenuPanel();
		} else if(env.menuData.currentMenuType == MenuType.ARENA_PAUSE) {
			createArenaPauseMenuPanel();
		} else if(env.menuData.currentMenuType == MenuType.SETTINGS) {
			createSettingsMenuPanel();
		} else if(env.menuData.currentMenuType == MenuType.ONLINE_BETA) {
			createOnlineBetaMenuPanel();
		}
		env.arenaAudioEngine.playShieldHit();
		env.arenaJFrameEngine.arenaJPanel.requestFocus();
//        env.arenaJFrameEngine.nickNameTextArea.setText(env.arenaJFrameEngine.debugNickName);
//        env.arenaJFrameEngine.nickNameTextArea.setText("abc");
	}

	private void createMainMenuPanel() {
		JPanel panel = new JPanel();
		int columnsQuantity = 1;
		int rowsQuantity = 8;
		panel.setLayout(new GridLayout(rowsQuantity, columnsQuantity));

		JButton arenaMenuButton = new JButton("Arena");
		arenaMenuButton.setFocusable(false);
		arenaMenuButton.addActionListener(env.arenaJFrameEngine.customEventHandler);
		panel.add(arenaMenuButton);
//		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
//		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
//		JButton weaponsMenuButton = new JButton("Weapons");
//		weaponsMenuButton.setFocusable(false);
//		weaponsMenuButton.addActionListener(env.arenaJFrameEngine.customEventHandler);
//		panel.add(weaponsMenuButton);
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
//		JButton shopMenuButton = new JButton("Shop");
//		shopMenuButton.setFocusable(false);
//		shopMenuButton.addActionListener(env.arenaJFrameEngine.customEventHandler);
//		panel.add(shopMenuButton);
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));

//		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
		JButton onlineBetaMenuButton = new JButton("Online(beta)");
		onlineBetaMenuButton.setFocusable(false);
		onlineBetaMenuButton.addActionListener(env.arenaJFrameEngine.customEventHandler);
		panel.add(onlineBetaMenuButton);


		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
		JButton settingsMenuButton = new JButton("Settings");
		settingsMenuButton.setFocusable(false);
		settingsMenuButton.addActionListener(env.arenaJFrameEngine.customEventHandler);
		panel.add(settingsMenuButton);
//		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));

		env.arenaJFrameEngine.currentMenuJPanel = panel;
	}

	private void createArenaButtonsMenuPanel() {
		JPanel panel = new JPanel();
		int columnsQuantity = 1;
		int rowsQuantity = 8;
		panel.setLayout(new GridLayout(rowsQuantity, columnsQuantity));

		JButton arenaPauseButton = new JButton("pause");
		arenaPauseButton.setFocusable(false);
		arenaPauseButton.addActionListener(env.arenaJFrameEngine.customEventHandler);
		panel.add(arenaPauseButton);
//		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));

		env.arenaJFrameEngine.currentMenuJPanel = panel;
	}

	private void createOnlineBetaMenuPanel() {
		JPanel panel = new JPanel();
		int columnsQuantity = 1;
		int rowsQuantity = 8;
		panel.setLayout(new GridLayout(rowsQuantity, columnsQuantity));

		JButton connectToServerButton = new JButton("Connect to server");
		connectToServerButton.setFocusable(false);
		connectToServerButton.addActionListener(env.arenaJFrameEngine.customEventHandler);
		panel.add(connectToServerButton);
//		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));

//		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
		JButton getOpponentInfoButton = new JButton("get opponent info");
		getOpponentInfoButton.setFocusable(false);
		getOpponentInfoButton.addActionListener(env.arenaJFrameEngine.customEventHandler);
		panel.add(getOpponentInfoButton);


		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
//		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
		JButton getClientAddressesButton = new JButton("get client addresses");
		getClientAddressesButton.setFocusable(false);
		getClientAddressesButton.addActionListener(env.arenaJFrameEngine.customEventHandler);
		panel.add(getClientAddressesButton);


		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
//		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
		JButton arenaPauseButton = new JButton("pause");
		arenaPauseButton.setFocusable(false);
		arenaPauseButton.addActionListener(env.arenaJFrameEngine.customEventHandler);
		panel.add(arenaPauseButton);


//		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		JButton mainMenuButton = new JButton("Main menu");
		mainMenuButton.setFocusable(false);
		mainMenuButton.addActionListener(env.arenaJFrameEngine.customEventHandler);
		panel.add(mainMenuButton);

		env.arenaJFrameEngine.currentMenuJPanel = panel;
	}

	private void createArenaVictoryMenuPanel() {
//        env.arenaJFrameEngine.nickNameTextArea.setText(env.arenaJFrameEngine.debugNickName);
		JPanel panel = new JPanel();
		int columnsQuantity = 1;
		int rowsQuantity = 8;
		panel.setLayout(new GridLayout(rowsQuantity, columnsQuantity));

		JButton arenaPauseButton = new JButton("next opponent");
		arenaPauseButton.setFocusable(false);
		arenaPauseButton.addActionListener(env.arenaJFrameEngine.customEventHandler);
		panel.add(arenaPauseButton);
//		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
		JButton mainMenuButton = new JButton("Main menu");
		mainMenuButton.setFocusable(false);
		mainMenuButton.addActionListener(env.arenaJFrameEngine.customEventHandler);
		panel.add(mainMenuButton);
//		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));

		env.arenaJFrameEngine.currentMenuJPanel = panel;
	}

	private void createArenaDefeatMenuPanel() {
//        env.arenaJFrameEngine.nickNameTextArea.setText(env.arenaJFrameEngine.debugNickName);
//        env.arenaJFrameEngine.nickNameTextArea.setText("abc");
		JPanel panel = new JPanel();
		int columnsQuantity = 1;
		int rowsQuantity = 8;
		panel.setLayout(new GridLayout(rowsQuantity, columnsQuantity));

		JButton restartRoundButton = new JButton("restart round");
		restartRoundButton.setFocusable(false);
		restartRoundButton.addActionListener(env.arenaJFrameEngine.customEventHandler);
		panel.add(restartRoundButton);
//		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
		JButton mainMenuButton = new JButton("Main menu");
		mainMenuButton.setFocusable(false);
		mainMenuButton.addActionListener(env.arenaJFrameEngine.customEventHandler);
		panel.add(mainMenuButton);
//		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));

		env.arenaJFrameEngine.currentMenuJPanel = panel;
	}

	private void createArenaPauseMenuPanel() {
		JPanel panel = new JPanel();
		int columnsQuantity = 1;
		int rowsQuantity = 8;
		panel.setLayout(new GridLayout(rowsQuantity, columnsQuantity));

		JButton arenaResumeButton = new JButton("resume");
		arenaResumeButton.setFocusable(false);
		arenaResumeButton.addActionListener(env.arenaJFrameEngine.customEventHandler);
		panel.add(arenaResumeButton);
//		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
		JButton restartRoundButton = new JButton("restart round");
		restartRoundButton.setFocusable(false);
		restartRoundButton.addActionListener(env.arenaJFrameEngine.customEventHandler);
		panel.add(restartRoundButton);
//		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));

		JButton settingsMenuButton = new JButton("Settings");
		settingsMenuButton.setFocusable(false);
		settingsMenuButton.addActionListener(env.arenaJFrameEngine.customEventHandler);
		panel.add(settingsMenuButton);
//		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
		JButton mainMenuButton = new JButton("Main menu");
		mainMenuButton.setFocusable(false);
		mainMenuButton.addActionListener(env.arenaJFrameEngine.customEventHandler);
		panel.add(mainMenuButton);
//		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));

		env.arenaJFrameEngine.currentMenuJPanel = panel;
	}

	private void createSettingsMenuPanel() {
		JPanel panel = new JPanel();
		int columnsQuantity = 1;
		int rowsQuantity = 5;
		panel.setLayout(new GridLayout(rowsQuantity, columnsQuantity));

		JButton arenaResumeButton = new JButton("back");
		arenaResumeButton.setFocusable(false);
		arenaResumeButton.addActionListener(env.arenaJFrameEngine.customEventHandler);
		panel.add(arenaResumeButton);
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));

		panel.add(SwingUtil.createTextLabel("sound", null));

		JSlider soundLevelSlider = new JSlider(JSlider.HORIZONTAL, 0, 100, (int) (env.arenaAudioEngine.soundLevel * 100));
		soundLevelSlider.setMajorTickSpacing(20);
		soundLevelSlider.setMinorTickSpacing(1);
		soundLevelSlider.setPaintTicks(true);
		soundLevelSlider.setPaintLabels(true);

		soundLevelSlider.addChangeListener(env.arenaJFrameEngine.customChangeListener);
		soundLevelSlider.setFocusable(false);
		soundLevelSlider.setName("sound");
		panel.add(soundLevelSlider);

//		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null));
		panel.add(SwingUtil.createTextLabel(debugText("_dbg_"), null, Color.GRAY));

		env.arenaJFrameEngine.currentMenuJPanel = panel;
	}


//==================================================================methods


	public void goToMainMenu() {
		log.trace("---");

		env.menuData.currentMenuType = MenuType.MAIN;
		env.arenaJFrameEngine.reloadJFrame();
		env.arenaJFrameEngine.pauseFlag = true;
		env.arenaJFrameEngine.swordsman();
	}

	public void goToOnlineBetaMenu() {
		log.trace("---");

		env.menuData.previousMenuType = env.menuData.currentMenuType;

		env.menuData.currentMenuType = MenuType.ONLINE_BETA;
		env.arenaJFrameEngine.reloadJFrame();
		env.arenaJFrameEngine.pauseFlag = true;
	}

	public void goToArenaButtonsMenu() {
		log.trace("---");

//		if(env.arenaJFrameEngine.onlineFlag) env.arenaTcpClientEngine.initTcpClient();
//		if(env.arenaJFrameEngine.onlineFlag && env.arenaUdpClientEngine.udpClient == null) env.arenaUdpClientEngine.initUdpClient();

//		env.arenaJFrameEngine.reset();
		env.arenaEngine.reset();
		resume();

		if(env.arenaJFrameEngine.onlineFlag) {
			env.gameControlsReceivedData = new GameControlsReceivedData();
			env.arenaUdpClientEngine.initUdpClient();
		}

//		env.arenaEngine.stepCadre();
	}

	public void goToArenaPauseMenu() {
		log.trace("---");

		env.menuData.currentMenuType = MenuType.ARENA_PAUSE;
		env.arenaJFrameEngine.reloadJFrame();
		env.arenaJFrameEngine.pauseFlag = true;
	}

	public void goToArenaVictoryMenu() {
		log.trace("---");

		env.menuData.currentMenuType = MenuType.VICTORY;
		env.arenaJFrameEngine.reloadJFrame();
		env.arenaJFrameEngine.pauseFlag = true;
	}

	public void goToArenaDefeatMenu() {
		log.trace("---");

		env.menuData.currentMenuType = MenuType.DEFEAT;
		env.arenaJFrameEngine.reloadJFrame();
		env.arenaJFrameEngine.pauseFlag = true;
	}

	public void goToSettingsMenu() {
		log.trace("---");

		env.menuData.previousMenuType = env.menuData.currentMenuType;

		env.menuData.currentMenuType = MenuType.SETTINGS;
		env.arenaJFrameEngine.reloadJFrame();
		env.arenaJFrameEngine.pauseFlag = true;
	}

	public void resume() {
		log.trace("---");

		env.menuData.currentMenuType = MenuType.ARENA_BUTTONS;
		env.arenaJFrameEngine.reloadJFrame();
		env.arenaJFrameEngine.pauseFlag = false;
	}

	public void backToPreviousMenu() {
		log.trace("---");

		env.menuData.currentMenuType = env.menuData.previousMenuType;
		env.arenaJFrameEngine.reloadJFrame();
	}

	private void msg(String text) {
		JOptionPane.showMessageDialog(null,
				text,
				"TITLE",
				JOptionPane.WARNING_MESSAGE);
	}

	private String debugText(String text) {
		return env.debugFlag ? text : "";
	}

	private void dbg(String text) {
		log.debug(text);
	}
}
