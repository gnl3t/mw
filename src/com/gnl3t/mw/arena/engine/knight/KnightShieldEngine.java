package com.gnl3t.mw.arena.engine.knight;


import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.constants.ArenaConst;
import org.apache.log4j.Logger;


public class KnightShieldEngine {
	private final static Logger log = Logger.getLogger(KnightShieldEngine.class);
	private Environment env;


	public void init(Environment env) {
		log.trace("---");
		this.env = env;
	}

	public void calcShieldCoordinatesKnight1() {
		env.k1.shield.x = env.k1.x + env.k1.body.bodyRectangleWidthStartValue + env.k1.shield.shieldRectangleWidthStartValue;
		if(env.k1.shieldCoverFlag) {
			env.k1.shield.y = ArenaConst.SCENE_LEVEL;
		} else {
			env.k1.shield.y = ArenaConst.SCENE_LEVEL + env.k1.shield.shieldRectangleHeightStartValue;
		}
	}

	public void calcShieldCoordinatesKnight2() {
		env.k2.shield.x = env.k2.x;
		if(env.k2.shieldCoverFlag) {
			env.k2.shield.y = ArenaConst.SCENE_LEVEL;
		} else {
			env.k2.shield.y = ArenaConst.SCENE_LEVEL + env.k2.shield.shieldRectangleHeightStartValue;
		}
	}
}
