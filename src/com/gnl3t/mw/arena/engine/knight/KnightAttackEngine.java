package com.gnl3t.mw.arena.engine.knight;


import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.ArenaScaleUtil;
import com.gnl3t.mw.util.LogUtil;
import org.apache.log4j.Logger;


public class KnightAttackEngine {
	final static Logger log = Logger.getLogger(KnightAttackEngine.class);
	private Environment env;


	public void init(Environment env) {
		log.trace("---");
		this.env = env;
	}

	public void calcAttack() {
		log.trace("---");

		calcAttackKnight1();
		calcAttackKnight2();
	}

	private boolean checkWeaponReachToBody() {
		log.trace("---");
		return (env.k2.x - env.k1.x) <= ArenaScaleUtil.reScale(23, env);
	}

	private boolean checkWeaponReachToShield() {
		log.trace("---");
		return (env.k2.x - env.k1.x) <= ArenaScaleUtil.reScale(41, env);
	}

	private void calcAttackKnight1() {
		log.trace("---");

		if(env.k1.attackAttemptStartFlag &&
				!env.k1.shieldCoverFlag &&
				env.k1.staminaBar.staminaPoints > 0) {

			dealDamageToKnight2();
		}

		env.knightStaminaEngine.calcStaminaLossKnight1();
	}

	private void calcAttackKnight2() {
		log.trace("---");

		if(env.k2.attackAttemptStartFlag &&
				!env.k2.shieldCoverFlag &&
				env.k2.staminaBar.staminaPoints > 0) {

			dealDamageToKnight1();
		}

		env.knightStaminaEngine.calcStaminaLossKnight2();
	}

	private void dealDamageToKnight1() {
		log.trace("---");
		if(!env.k2.damageDone) {
			env.k2.damageDone = true;
			if(env.k1.shieldCoverFlag && checkWeaponReachToShield()) {
				if(env.gameControlsData.playerLeft) {
					int damageToDeal = env.k2.weapon.damage - env.k1.shield.armor;
					if(damageToDeal <= 0) damageToDeal = 1;
					env.k1.healthPointsBar.healthPoints = env.k1.healthPointsBar.healthPoints - damageToDeal;
				}
				env.arenaAudioEngine.playShieldHit();
			} else if(checkWeaponReachToBody()) {
//				if(env.gameControlsData.playerLeft) env.k1.healthPointsBar.healthPoints = env.k1.healthPointsBar.healthPoints - env.k2.weapon.damage;
				env.k1.healthPointsBar.healthPoints = env.k1.healthPointsBar.healthPoints - env.k2.weapon.damage;
//				dbg1("env.k1.healthPointsBar.healthPoints=" + env.k1.healthPointsBar.healthPoints);
				env.arenaAudioEngine.playBodyHit();
			} else {
				env.arenaAudioEngine.playSwordSwing();
			}
	        if(env.k1.healthPointsBar.healthPoints <= 0) {
			    env.k1.playerDead = true;
	        }
		}
	}

	private void dealDamageToKnight2() {
		log.trace("---");
		if(!env.k1.damageDone) {
			env.k1.damageDone = true;
			if(env.k2.shieldCoverFlag && checkWeaponReachToShield()) {
				if(env.gameControlsData.playerLeft) {
					int damageToDeal = env.k1.weapon.damage - env.k2.shield.armor;
					if(damageToDeal <= 0) damageToDeal = 1;
					env.k2.healthPointsBar.healthPoints = env.k2.healthPointsBar.healthPoints - damageToDeal;
				}
				env.arenaAudioEngine.playShieldHit();
			} else if(checkWeaponReachToBody()) {
//				if(env.gameControlsData.playerLeft) env.k2.healthPointsBar.healthPoints = env.k2.healthPointsBar.healthPoints - env.k1.weapon.damage;
				env.k2.healthPointsBar.healthPoints = env.k2.healthPointsBar.healthPoints - env.k1.weapon.damage;
				env.arenaAudioEngine.playBodyHit();
			} else {
				env.arenaAudioEngine.playSwordSwing();
			}
	        if(env.k2.healthPointsBar.healthPoints <= 0) {
			    env.k2.playerDead = true;
	        }
		}
	}

	public void handleAttackStartKnight1() {
		if(!env.k1.shieldCoverFlag) env.k1.attackAttemptStartFlag = true;
	}

	public void handleAttackStartKnight2() {
		if(!env.k2.shieldCoverFlag) env.k2.attackAttemptStartFlag = true;
	}

	public void handleAttackStopKnight1() {
		env.k1.attackAttemptStartFlag = false;
		env.k1.damageDone = false;
		env.k1.staminaUsed = false;
	}

	public void handleAttackStopKnight2() {
		env.k2.attackAttemptStartFlag = false;
		env.k2.damageDone = false;
		env.k2.staminaUsed = false;
	}



//==================================================================dbg_print

	private void dbg(String text) {
		log.debug(text);
	}

	private void dbg(Exception e) {
		dbg(LogUtil.buildStackTraceAsString(e));
	}

	private void dbg1(String text) {
		dbg(text);
		env.arenaJFrameEngine.dbg1(text);
	}

	private void dbg2(String text) {
		dbg(text);
		env.arenaJFrameEngine.dbg(text);
	}

	private void dbg2(Exception e) {
		dbg1(e.getMessage());
		dbg2(LogUtil.buildStackTraceAsString(e));
	}
}
