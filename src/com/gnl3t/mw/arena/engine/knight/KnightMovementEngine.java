package com.gnl3t.mw.arena.engine.knight;


import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.ArenaScaleUtil;
import com.gnl3t.mw.util.constants.ArenaConst;
import org.apache.log4j.Logger;


public class KnightMovementEngine {
	private final static Logger log = Logger.getLogger(KnightMovementEngine.class);
	private Environment env;


	public void init(Environment env) {
		log.trace("---");
		this.env = env;
	}

	public void calcMoving() {
		log.trace("---");
		calcMovingKnight1();
		calcMovingKnight2();
	}

	private void calcMovingKnight1() {
		log.trace("---");

		if(env.k1.xSpeed < 0 && env.k1.x < ArenaScaleUtil.reScale(ArenaConst.ARENA_X_LIMIT_LEFT, env)) return;
		if(env.k1.xSpeed > 0 && env.k1.x > ArenaScaleUtil.reScale(ArenaConst.ARENA_X_LIMIT_RIGHT, env)) return;
		if(!env.k1.collisionFlag && !env.k1.attackAttemptStartFlag) env.k1.x += env.k1.xSpeed;
//		if(!env.k1.collisionFlag && !env.k1.attackAttemptStartFlag) {
//			env.k1.x += env.k1.xSpeed;
//			env.k1.traveledDistance += 1;
//		}

		if(checkActiveCollisionKnight1()) {
			if(!env.k1.collisionFlag && (env.k1.xSpeed > 0)) env.k1.x -= ArenaConst.X_SPEED_VALUE;
			env.k1.collisionFlag = true;

			env.k1.stepNumber = 0;
			env.k1.traveledDistance = 0;
		} else {
			env.k1.collisionFlag = false;

			if(env.k1.xSpeed > 0) {
				env.k1.traveledDistance += env.k1.xSpeed;
			} else {
				env.k1.traveledDistance += -1 * env.k1.xSpeed;
			}
		}

//		env.k1.body.x = env.k1.x - env.k1.body.halfBodyRectangleWidthStartValue;
		env.k1.body.x = env.k1.x;

//		env.knightWeaponEngine.calcWeaponCoordinatesKnight1();
//		env.knightShieldEngine.calcShieldCoordinatesKnight1();
	}

	private void calcMovingKnight2() {
		log.trace("---");

		if(env.k2.xSpeed < 0 && env.k2.x < ArenaScaleUtil.reScale(ArenaConst.ARENA_X_LIMIT_LEFT, env)) return;
		if(env.k2.xSpeed > 0 && env.k2.x > ArenaScaleUtil.reScale(ArenaConst.ARENA_X_LIMIT_RIGHT, env)) return;

		if(!env.k2.collisionFlag && !env.k2.attackAttemptStartFlag) env.k2.x += env.k2.xSpeed;

		if(checkActiveCollisionKnight2()) {
			if(!env.k2.collisionFlag && (env.k2.xSpeed < 0)) env.k2.x += ArenaConst.X_SPEED_VALUE;
			env.k2.collisionFlag = true;
		} else {
			env.k2.collisionFlag = false;
		}

//		env.k2.body.x = env.k2.x + env.k2.body.bodyRectangleWidthStartValue + env.k2.body.halfBodyRectangleWidthStartValue;
		env.k2.body.x = env.k2.x;

//		env.knightWeaponEngine.calcWeaponCoordinatesKnight2();
//		env.knightShieldEngine.calcShieldCoordinatesKnight2();
	}

	private boolean checkActiveCollisionKnight1() {
		log.trace("---");
//		return env.k1.body.getBodyRectangle().intersects(env.k2.weapon.getWeaponRectangle());
//		return false;
		return (env.k2.x - env.k1.x) <= ArenaScaleUtil.reScale(13, env);
	}

	private boolean checkActiveCollisionKnight2() {
		log.trace("---");
//		return env.k2.body.getBodyRectangle().intersects(env.k1.weapon.getWeaponRectangle());
//		return false;
		return checkActiveCollisionKnight1();
	}

//	public void enableCollisionFlags() {
//		log.trace("---");
//		env.k1.collisionFlag = true;
//		env.k2.collisionFlag = true;
//	}

	public void resetCollisionFlags() {
		log.trace("---");
		env.k1.collisionFlag = false;
		env.k2.collisionFlag = false;
	}
}
