package com.gnl3t.mw.arena.engine.knight;


import com.gnl3t.mw.environment.Environment;
import org.apache.log4j.Logger;


public class KnightStaminaEngine {
	final static Logger log = Logger.getLogger(KnightStaminaEngine.class);
	private Environment env;


	public void init(Environment env) {
		log.trace("---");
		this.env = env;
	}

	public void calcStaminaLossKnight1() {
		if(env.k1.attackAttemptStartFlag && !env.k1.staminaUsed && env.k1.staminaBar.staminaPoints > 0) {
			env.k1.staminaUsed = true;
			env.k1.staminaBar.staminaPoints = env.k1.staminaBar.staminaPoints - env.k1.staminaBar.staminaLoss;

//			if(!env.k1.damageDone && !env.k2.collisionFlag) env.arenaAudioEngine.playSwordSwing();
//			if(!env.k1.damageDone && env.k2.collisionFlag) {
//				if(env.k2.shieldCoverFlag) {
//					env.arenaAudioEngine.playShieldHit();
//				} else {
//					env.arenaAudioEngine.playBodyHit();
//				}
//			}
		}
	}

	public void calcStaminaLossKnight2() {
		if(env.k2.attackAttemptStartFlag && !env.k2.staminaUsed && env.k2.staminaBar.staminaPoints > 0) {
			env.k2.staminaUsed = true;
			env.k2.staminaBar.staminaPoints = env.k2.staminaBar.staminaPoints - env.k2.staminaBar.staminaLoss;

//			if(!env.k2.damageDone && !env.k1.collisionFlag) env.arenaAudioEngine.playSwordSwing();
//			if(!env.k2.damageDone && env.k1.collisionFlag) {
//				if(env.k1.shieldCoverFlag) {
//					env.arenaAudioEngine.playShieldHit();
//				} else {
//					env.arenaAudioEngine.playBodyHit();
//				}
//			}
		}
	}

	public void calcStaminaRegenKnight1() {
		log.trace("---");
		if(env.k1.playerDead) return;

		if(!env.k1.attackAttemptStartFlag) {
			if(env.k1.staminaBar.staminaPoints < 0) {
				env.k1.staminaBar.staminaPoints = 10;
			}
			if(env.k1.staminaBar.staminaPoints < env.k1.staminaBar.staminaPointsStartValue) {
				env.k1.staminaBar.staminaPoints += env.k1.staminaBar.staminaPointsRegenSpeed;
				if(env.k1.staminaBar.staminaPoints > env.k1.staminaBar.staminaPointsStartValue) {
					env.k1.staminaBar.staminaPoints = env.k1.staminaBar.staminaPointsStartValue;
				}
			}
		}
	}

	public void calcStaminaRegenKnight2() {
		log.trace("---");
		if(env.k1.playerDead) return;

		if(!env.k2.attackAttemptStartFlag) {
			if(env.k2.staminaBar.staminaPoints < 0) {
				env.k2.staminaBar.staminaPoints = 10;
			}
			if(env.k2.staminaBar.staminaPoints < env.k2.staminaBar.staminaPointsStartValue) {
				env.k2.staminaBar.staminaPoints += env.k2.staminaBar.staminaPointsRegenSpeed;
				if(env.k2.staminaBar.staminaPoints > env.k2.staminaBar.staminaPointsStartValue) {
					env.k2.staminaBar.staminaPoints = env.k2.staminaBar.staminaPointsStartValue;
				}
			}
		}
	}

}
