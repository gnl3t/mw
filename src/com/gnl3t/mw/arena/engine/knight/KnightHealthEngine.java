package com.gnl3t.mw.arena.engine.knight;


import com.gnl3t.mw.environment.Environment;
import org.apache.log4j.Logger;


public class KnightHealthEngine {
	private final static Logger log = Logger.getLogger(KnightHealthEngine.class);
	private Environment env;


	public void init(Environment env) {
		log.trace("---");
		this.env = env;
	}

	public void calcHealthPointsRegenKnight1() {
		log.trace("---");
		if(env.k1.healthPointsBar.healthPoints < env.k1.healthPointsBar.healthPointsStartValue) {
			env.k1.healthPointsBar.healthPoints += env.k1.healthPointsBar.healthPointsRegenSpeed;
			if(env.k1.healthPointsBar.healthPoints > env.k1.healthPointsBar.healthPointsStartValue) {
				env.k1.healthPointsBar.healthPoints = env.k1.healthPointsBar.healthPointsStartValue;
			}
		}
	}

	public void calcHealthPointsRegenKnight2() {
		log.trace("---");
		if(env.k2.healthPointsBar.healthPoints < env.k2.healthPointsBar.healthPointsStartValue) {
			env.k2.healthPointsBar.healthPoints += env.k2.healthPointsBar.healthPointsRegenSpeed;
			if(env.k2.healthPointsBar.healthPoints > env.k2.healthPointsBar.healthPointsStartValue) {
				env.k2.healthPointsBar.healthPoints = env.k2.healthPointsBar.healthPointsStartValue;
			}
		}
	}

}
