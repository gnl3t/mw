package com.gnl3t.mw.arena.engine;


import com.gnl3t.mw.arena.udp.client.UdpClient;
import com.gnl3t.mw.arena.udp.client.UdpRequestsPollerEmulatorRunnable;
import com.gnl3t.mw.arena.udp.client.UdpRequestsPollerRunnable;
import com.gnl3t.mw.data.arena.GameControlsData;
import com.gnl3t.mw.data.arena.GameControlsReceivedData;
import com.gnl3t.mw.data.construct.KnightData;
import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.ArenaScaleUtil;
import com.gnl3t.mw.util.DataPacketValidatorUtil;
import com.gnl3t.mw.util.Log4jUtil;
import com.gnl3t.mw.util.RequestUtils;
import com.gnl3t.mw.util.constants.ArenaConst;
import com.gnl3t.mw.util.constants.StringConst;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.TreeMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;


public class ArenaUdpClientEngine {
	private final static Logger log = Logger.getLogger(ArenaUdpClientEngine.class);
	private Environment env;


	public UdpClient udpClient;
	public boolean initialized = false;
	public boolean enableSecondPlayerEmulatorFlag = true;


	public BlockingQueue<String> inputPacketsQueue;
	public BlockingQueue<String> outputPacketsQueue;
	public BlockingQueue<String> parsedPacketsQueue;

	public UdpRequestsPollerRunnable udpRequestsPollerRunnable;
	private Thread udpRequestsPollerRunnableRunnableThread;

	private String currentMessage = StringConst.EMPTY_STRING;
	private String polledMessage = StringConst.EMPTY_STRING;
	private String[] currentMessageSplitted;

	private int counter;
	private int messageId = 0;


	public void init(Environment env) {
		log.trace("---");
		this.env = env;
	}

	public void initGameControlsReceivedData() {
		env.gameControlsReceivedData.x1 = env.k1.x;
		env.gameControlsReceivedData.x2 = env.k2.x;
		env.gameControlsReceivedData.hp1 = env.k1.healthPointsBar.healthPoints;
		env.gameControlsReceivedData.hp2 = env.k2.healthPointsBar.healthPoints;
		env.gameControlsReceivedData.st1 = env.k1.staminaBar.staminaPoints;
		env.gameControlsReceivedData.st2 = env.k2.staminaBar.staminaPoints;
	}

	public void initUdpClient() {
		log.trace("---");

		initialized = false;

//		String host = "localhost";
//		String host = "164.90.171.227";
		String host = ArenaJFrameEngine.localHostFlag ? "localhost" : "164.90.171.227";
//		int tcpPort = 8090;
//		int port = 9001;
		int tcpPort = env.gameControlsData.playerLeft ? 8090 : 8091;
		int port = env.gameControlsData.playerLeft ? 9001 : 9002;
//		log.debug("host");
		udpClient = new UdpClient(host, port, env);
		inputPacketsQueue = new LinkedBlockingDeque<>();
        outputPacketsQueue = new LinkedBlockingDeque<>();
		parsedPacketsQueue = new LinkedBlockingDeque<>();
		try {
			udpClient.punchHole(tcpPort);
			udpClient.connect();
//			ThreadUtil.sleep(3000);
//			while(true) {
//				env.arenaOnlinePacketsEngine.sendRequest(); //TODO - rework to separate thread
//			}
			//TODO - implement possibility to stop the thread
//			Thread udpRequestsPollerRunnableRunnableThread = new Thread(new TcpRequestsPollerRunnable(env));
			udpRequestsPollerRunnableRunnableThread = new Thread(new UdpRequestsPollerRunnable(env));
			udpRequestsPollerRunnableRunnableThread.start();
		} catch (IOException e) {
			Log4jUtil.logException(log, e);
		}
		initialized = true;
	}

	public void initUdpClient(String response) {
		log.trace("---");
		if(!response.contains("foundOpponent=true")) return;
		if(!response.contains("matchId")) return;
		initialized = false;

		env.arenaEngine.reset();
		initGameControlsReceivedData();

//		env.k1 = new KnightData(false, env);
//		env.k2 = new KnightData(true, env);
//		env.k1.x = ArenaScaleUtil.reScale(50, env) + ArenaConst.ARENA_X_LIMIT_LEFT;
//		env.k2.x = ArenaScaleUtil.reScale(ArenaConst.ARENA_X_LIMIT_RIGHT, env);
//		env.gameControlsData = new GameControlsData();
//		env.gameControlsReceivedData = new GameControlsReceivedData();

		TreeMap<String, String> responseTree = RequestUtils.parseToTreeMap(response);
//		for (Map.Entry<String, String> entry : responseTree.entrySet()) {
////			dbg("key=" + entry.getKey() + ", value=" + entry.getValue());
//			dbg(entry.getKey() + "=" + entry.getValue());
//		}
		env.matchId = responseTree.get("matchId");
		boolean playerLeft = Boolean.parseBoolean(responseTree.get("playerLeft"));
		env.gameControlsData.playerLeft = playerLeft;
//		String playerLeftString = playerLeft ? "playerLeft" : "playerRight";
//		env.playerLeftString = playerLeftString;
		env.playerLeftString = playerLeft ? "playerLeft" : "playerRight";;
		String host = responseTree.get("duelHandlerHost");
		int tcpPort = -1;
		if(playerLeft) {
			tcpPort = Integer.parseInt(responseTree.get("duelHandlerTcpPort1"));
		} else {
			tcpPort = Integer.parseInt(responseTree.get("duelHandlerTcpPort2"));
		}
		int udpPort = Integer.parseInt(responseTree.get("duelHandlerUdpPort"));


		udpClient = new UdpClient(host, udpPort, env);
		inputPacketsQueue = new LinkedBlockingDeque<>();
        outputPacketsQueue = new LinkedBlockingDeque<>();
		parsedPacketsQueue = new LinkedBlockingDeque<>();
		try {
			udpClient.punchHole(tcpPort);
			log.debug("connect udp player");
			udpClient.connect();

			udpRequestsPollerRunnable = new UdpRequestsPollerRunnable(env);
			udpRequestsPollerRunnableRunnableThread = new Thread(udpRequestsPollerRunnable);
			udpRequestsPollerRunnableRunnableThread.start();

			enableSecondPlayerEmulator(response);

//			env.k1.healthPointsBar.healthPoints = env.k1.healthPointsBar.healthPointsStartValue;
//			env.k2.healthPointsBar.healthPoints = env.k2.healthPointsBar.healthPointsStartValue;
//			env.k1.playerDead = false;
//			env.k2.playerDead = false;
			env.arenaJFrameEngine.onlineFlag = true;
			env.arenaJFrameEngine.pauseFlag = false;
			env.arenaJFrameEngine.arenaJPanel.requestFocusInWindow();
		} catch (IOException e) {
			Log4jUtil.logException(log, e);
		}
		initialized = true;
	}

	private void enableSecondPlayerEmulator(String response) {
		if(!enableSecondPlayerEmulatorFlag) return;
//		UdpRequestsPollerEmulatorRunnable emulatorSecondPlayer = new UdpRequestsPollerEmulatorRunnable(env);
//		emulatorSecondPlayer.initRequest(response);
		env.udpRequestsPollerEmulatorRunnable = new UdpRequestsPollerEmulatorRunnable(env);
//		env.udpRequestsPollerEmulatorRunnable.initRequest(response);
		Thread udpRequestsPollerEmulatorRunnableThread = new Thread(env.udpRequestsPollerEmulatorRunnable);
		udpRequestsPollerEmulatorRunnableThread.start();
	}

	public synchronized void putMessage(String contentString) {
		log.trace("---");

        try {
        	if(contentString == null || contentString.trim().length() == 0) return;
        	if(outputPacketsQueue.size() < 10) {
				outputPacketsQueue.put(contentString);
			}
        } catch (InterruptedException e) {
            Log4jUtil.logException(log, e);
        }
    }

	public synchronized void sendMessages() {
		log.trace("---");

        try {
        	if(outputPacketsQueue.size() > 1) {
				currentMessage = StringConst.EMPTY_STRING;
				counter = 0;
				for(int i = 0; i < outputPacketsQueue.size(); i++) {
					polledMessage = outputPacketsQueue.poll(1, TimeUnit.NANOSECONDS);
					if(!currentMessage.contains(polledMessage)) {
						if(counter++ < 10) {
							currentMessage += polledMessage + StringConst.DELIMITER_SYMBOL;
						}
					}
				}
			} else {
				currentMessage = outputPacketsQueue.poll(1, TimeUnit.NANOSECONDS);
			}
			sendMessage(currentMessage);
        } catch (InterruptedException e) {
            Log4jUtil.logException(log, e);
        }
	}

	public synchronized void sendMessage(String contentString) {
		log.trace("---");
		if(udpClient == null || !initialized) return;

//		log.debug("contentString=" + contentString);
//		String response = udpClient.sendMessages(contentString);
//		udpClient.sendMessages(contentString);
//		udpClient.sendMessages(makePacket(contentString));
		handleResponse(udpClient.sendMessage(makePacket(contentString)));
//		log.debug("response=" + response);
	}

	private synchronized String makePacket(String contentString) {
		log.trace("---");
//		return StringConst.BEGIN + contentString + StringConst.END;
//		return StringConst.BEGIN + env.playerLeftString + contentString + StringConst.END;
		return StringConst.BEGIN +
				env.playerLeftString + StringConst.BACK_SLASH_N +
				"matchId=" + env.matchId + StringConst.BACK_SLASH_N +
				contentString +
				StringConst.END;
//		return StringConst.BEGIN + env.playerLeftString + "_id_" + messageId++ + StringConst.BACK_SLASH_N + contentString + StringConst.END;
	}

	private synchronized void handleResponse(String contentString) {
		log.trace("---");

		if(contentString == null) return;
		dbg("DataPacketValidatorUtil.validate(contentString)=" + DataPacketValidatorUtil.validate(contentString));
		if(contentString.contains(StringConst.BEGIN)) contentString = contentString.substring(4);
		if(DataPacketValidatorUtil.validate(contentString)) {
			dbg("DataPacketValidatorUtil.validate(contentString)=true");
//			env.arenaOnlinePacketsEngine.handleResponse(contentString);
			if(contentString.length() == 0) return;
			if(contentString.contains(StringConst.LOG_SEPARATOR)) return;

			try {
//				inputPacketsQueue.put(contentString);
				if(contentString.contains(StringConst.SPLIT_FLAG)) {
					currentMessageSplitted = contentString.split(StringConst.DELIMITER_SYMBOL);
					for (String splitted : currentMessageSplitted) {
						inputPacketsQueue.put(splitted);
					}
				} else {
					inputPacketsQueue.put(contentString);
				}
			} catch (InterruptedException e) {
//				e.printStackTrace();
				Log4jUtil.logException(log, e);
			}
		} else {
			dbg("DataPacketValidatorUtil.validate(contentString)=false");
		}
	}

	public void dbg(String sourceString) {
//		if(debugFlag) log.debug(sourceString);
		if(env.gameControlsData.playerLeft) {
//				log.debug(sourceString);
		} else {
//			log.debug(sourceString);
		}
	}
}
