package com.gnl3t.mw.arena.engine;


import com.gnl3t.mw.arena.http.client.CustomHttpClient;
import com.gnl3t.mw.arena.udp.client.UdpClient;
import com.gnl3t.mw.arena.udp.client.UdpRequestsPollerEmulatorRunnable;
import com.gnl3t.mw.arena.udp.client.UdpRequestsPollerRunnable;
import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.LogUtil;
import com.gnl3t.mw.util.RequestUtils;
import com.gnl3t.mw.util.ThreadUtil;
import com.gnl3t.mw.util.constants.StringConst;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;


public class ArenaOnlineBetaEngine {
	private final static Logger log = Logger.getLogger(ArenaOnlineBetaEngine.class);
	private Environment env;


	public void init(Environment env) {
		log.trace("---");
		this.env = env;
	}

//==================================================================public

	public void t1() {
//		11
	}

	public void connectToServer() {
		dbg1("connectToServer");
		try {
			String pathString = "/hello/world";
			String httpString = ArenaJFrameEngine.localHostFlag ? "http://localhost:8000" + pathString : "http://164.90.171.227:8000" + pathString;
	//        String httpString = "http://localhost:8000/command";
	//        String httpString = "http://164.90.171.227:8000/command";

			CustomHttpClient customHttpClient;
			String request = "type=command\n" +
					"commandName=getClientAddresses";
			customHttpClient = new CustomHttpClient();


			dbg1("httpString=" + httpString);
			dbg1("request=" + request);

	        String response = customHttpClient.sendRequest(httpString, request);
//			return customHttpClient.sendRequest(httpString, request);
			dbg1("response=" + response);
		} catch (Exception e) {
			dbg2(e);
		}
	}

	public void getClientAddresses() {
		dbg1("connectToServer");
		try {
			String pathString = "/command";
			String httpString = ArenaJFrameEngine.localHostFlag ? "http://localhost:8000" + pathString : "http://164.90.171.227:8000" + pathString;
	//        String httpString = "http://localhost:8000/command";
	//        String httpString = "http://164.90.171.227:8000/command";

			CustomHttpClient customHttpClient;
			String request = "type=command\n" +
					"commandName=getClientAddresses";
			customHttpClient = new CustomHttpClient();


			dbg1("httpString=" + httpString);
			dbg1("request=" + request);

	        String response = customHttpClient.sendRequest(httpString, request);
//			return customHttpClient.sendRequest(httpString, request);
			dbg1("response=" + response);
		} catch (Exception e) {
			dbg2(e);
		}
	}

	public void getOpponentInfo() {
		dbg1("getOpponentInfo");
		try {
			String pathString = "/command";
			String httpString = ArenaJFrameEngine.localHostFlag ? "http://localhost:8000" + pathString : "http://164.90.171.227:8000" + pathString;
            CustomHttpClient customHttpClient = new CustomHttpClient();

			TreeMap<String, String> messageTree = RequestUtils.createEmptyMessageTree();
			messageTree.put("type", "command");
			messageTree.put("commandName", "getOpponentInfo");
			messageTree.put("nickName", env.arenaJFrameEngine.nickNameTextArea.getText());

			messageTree.put("weaponType", StringConst.EMPTY_STRING + env.userArmoryData.weapons.get(env.userArmoryData.equippedWeaponIndex).knightType);
			messageTree.put("weaponLevelType", StringConst.EMPTY_STRING + env.userArmoryData.weapons.get(env.userArmoryData.equippedWeaponIndex).knightLevelType);
			messageTree.put("armorType", StringConst.EMPTY_STRING + env.userArmoryData.armors.get(env.userArmoryData.equippedArmorIndex).knightType);
			messageTree.put("armorLevelType", StringConst.EMPTY_STRING + env.userArmoryData.armors.get(env.userArmoryData.equippedArmorIndex).knightLevelType);
			messageTree.put("helmetType", StringConst.EMPTY_STRING + env.userArmoryData.helmets.get(env.userArmoryData.equippedHelmetIndex).knightType);
			messageTree.put("helmetLevelType", StringConst.EMPTY_STRING + env.userArmoryData.helmets.get(env.userArmoryData.equippedHelmetIndex).knightLevelType);
			messageTree.put("shieldType", StringConst.EMPTY_STRING + env.userArmoryData.shields.get(env.userArmoryData.equippedShieldIndex).knightType);
			messageTree.put("shieldLevelType", StringConst.EMPTY_STRING + env.userArmoryData.shields.get(env.userArmoryData.equippedShieldIndex).knightLevelType);

            messageTree.put("equipmentTotalCost", "1500");
            messageTree.put("ping", "123456");
            messageTree.put("nFactorial", StringConst.EMPTY_STRING + calculateFactorial(10, 1005001));

			String request = RequestUtils.serializeMessageTree(messageTree);


			dbg1("httpString=" + httpString);
			dbg1("request=\n" + request);

	        String response = customHttpClient.sendRequest(httpString, request);
			dbg1("response=\n" + response);

			punchConnection2(response);
		} catch (Exception e) {
			dbg2(e);
		}
	}

	public synchronized void notifyServerMatchIsOver(String playerLeftString) {
		dbg("notifyServerMatchIsOver");
		try {
			String pathString = "/command";
			String httpString = ArenaJFrameEngine.localHostFlag ? "http://localhost:8000" + pathString : "http://164.90.171.227:8000" + pathString;
            CustomHttpClient customHttpClient = new CustomHttpClient();

			TreeMap<String, String> messageTree = RequestUtils.createEmptyMessageTree();
			messageTree.put("type", "command");
			messageTree.put("commandName", "matchFinishNotification");
			messageTree.put("matchId", env.matchId);
			messageTree.put("playerLeftString", playerLeftString);

			String request = RequestUtils.serializeMessageTree(messageTree);


			dbg("httpString=" + httpString);
			dbg("request=\n" + request);

	        String response = customHttpClient.sendRequest(httpString, request);
			dbg("response=\n" + response);

			punchConnection2(response);
		} catch (Exception e) {
			dbg2(e);
		}
	}

	public void getOpponentInfo2() {
//		try {
//			String httpString = ArenaJFrameEngine.localHostFlag ? "http://localhost:8000/command" : "http://164.90.171.227:8000/command";
//	//        String httpString = "http://localhost:8000/command";
//	//        String httpString = "http://164.90.171.227:8000/command";
//
//			CustomHttpClient customHttpClient;
//			String request = "type=command\n" +
//					"commandName=getClientAddresses";
//			customHttpClient = new CustomHttpClient();
//	//        String response = customHttpClient.sendRequest("http://localhost:8000/command", request);
//			return customHttpClient.sendRequest(httpString, request);
//		} catch (Exception e) {
//			dbg2(e);
//		}
	}



//==================================================================private

	private void t2() {
//		11
	}

	private void getOpponentForEmulator() {
		dbg1("getOpponentForEmulator");
		try {
			String pathString = "/command";
			String httpString = ArenaJFrameEngine.localHostFlag ? "http://localhost:8000" + pathString : "http://164.90.171.227:8000" + pathString;
			CustomHttpClient customHttpClient = new CustomHttpClient();

			TreeMap<String, String> messageTree = RequestUtils.createEmptyMessageTree();
			messageTree.put("type", "command");
			messageTree.put("commandName", "getOpponentInfo");
//			messageTree.put("nickName", env.arenaJFrameEngine.nickNameTextArea.getText());
			messageTree.put("nickName", "abc");

			messageTree.put("weaponType", StringConst.EMPTY_STRING + env.userArmoryData.weapons.get(env.userArmoryData.equippedWeaponIndex).knightType);
			messageTree.put("weaponLevelType", StringConst.EMPTY_STRING + env.userArmoryData.weapons.get(env.userArmoryData.equippedWeaponIndex).knightLevelType);
			messageTree.put("armorType", StringConst.EMPTY_STRING + env.userArmoryData.armors.get(env.userArmoryData.equippedArmorIndex).knightType);
			messageTree.put("armorLevelType", StringConst.EMPTY_STRING + env.userArmoryData.armors.get(env.userArmoryData.equippedArmorIndex).knightLevelType);
			messageTree.put("helmetType", StringConst.EMPTY_STRING + env.userArmoryData.helmets.get(env.userArmoryData.equippedHelmetIndex).knightType);
			messageTree.put("helmetLevelType", StringConst.EMPTY_STRING + env.userArmoryData.helmets.get(env.userArmoryData.equippedHelmetIndex).knightLevelType);
			messageTree.put("shieldType", StringConst.EMPTY_STRING + env.userArmoryData.shields.get(env.userArmoryData.equippedShieldIndex).knightType);
			messageTree.put("shieldLevelType", StringConst.EMPTY_STRING + env.userArmoryData.shields.get(env.userArmoryData.equippedShieldIndex).knightLevelType);

			messageTree.put("equipmentTotalCost", "1500");
			messageTree.put("ping", "123456");
			messageTree.put("nFactorial", StringConst.EMPTY_STRING + calculateFactorial(10, 1005001));

			String request = RequestUtils.serializeMessageTree(messageTree);


			dbg1("httpString=" + httpString);
			dbg1("request=\n" + request);

			String response = customHttpClient.sendRequest(httpString, request);
			dbg1("response=\n" + response);

//			punchConnection2(response);
		} catch (Exception e) {
			dbg2(e);
		}
	}

	private void punchConnection2(String response) {
        env.arenaUdpClientEngine.initUdpClient(response);
//		env.arenaOnlineBetaEngine.getClientAddresses();
//		getOpponentForEmulator();
	}

	private void punchConnection1(String response) {
		if(!response.contains("foundOpponent=true")) return;
		int sleepTime = ArenaJFrameEngine.localHostFlag ? 50 : 5;

		TreeMap<String, String> responseTree = RequestUtils.parseToTreeMap(response);
//		for (Map.Entry<String, String> entry : responseTree.entrySet()) {
////			dbg("key=" + entry.getKey() + ", value=" + entry.getValue());
//			dbg(entry.getKey() + "=" + entry.getValue());
//		}
		boolean playerLeft = Boolean.parseBoolean(responseTree.get("playerLeft"));
		String playerLeftString = playerLeft ? "playerLeft" : "playerRight";
		String host = responseTree.get("duelHandlerHost");
        int tcpPort = -1;
		if(playerLeft) {
            tcpPort = Integer.parseInt(responseTree.get("duelHandlerTcpPort1"));
        } else {
            tcpPort = Integer.parseInt(responseTree.get("duelHandlerTcpPort2"));
        }
		int udpPort = Integer.parseInt(responseTree.get("duelHandlerUdpPort"));

		UdpClient udpClient = new UdpClient(host, udpPort);
		try {
			udpClient.punchHole(tcpPort);
			udpClient.connect();
			while(true) {
				udpClient.sendMessage("BEGIN" + playerLeftString + "\n" +
						"request\n" +
						"END");
				ThreadUtil.sleep(sleepTime);
			}
		} catch (Exception e) {
			dbg(e);
		}
	}

    private long calculateFactorial(int base, int repeat) {
        long startTime = System.currentTimeMillis();
        for(int i = 1; i <= repeat; i++) {
            calculateFactorial(base);
        }

        return System.currentTimeMillis() - startTime;
    }

    private long calculateFactorial(int base) {
        long startTime = System.currentTimeMillis();
        int result = 1;
        for(int i = 1; i <= base; i++) {
            result = result * i;
//            dbg("i=" + i);
//            dbg("result=" + result);
        }
//        dbg("result=" + result);

        return System.currentTimeMillis() - startTime;
    }



//==================================================================dbg_print

	private void dbg(String text) {
		log.debug(text);
	}

	private void dbg(Exception e) {
		dbg(LogUtil.buildStackTraceAsString(e));
	}

	private void dbg1(String text) {
	    dbg(text);
		env.arenaJFrameEngine.dbg1(text);
	}

	private void dbg2(String text) {
        dbg(text);
		env.arenaJFrameEngine.dbg(text);
	}

	private void dbg2(Exception e) {
		dbg1(e.getMessage());
		dbg2(LogUtil.buildStackTraceAsString(e));
	}
}
