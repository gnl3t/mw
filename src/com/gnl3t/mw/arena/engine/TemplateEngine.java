package com.gnl3t.mw.arena.engine;


import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.LogUtil;
import org.apache.log4j.Logger;


public class TemplateEngine {
	private final static Logger log = Logger.getLogger(TemplateEngine.class);
	private Environment env;


	public void init(Environment env) {
		log.trace("---");
		this.env = env;
	}

//==================================================================public

	public void t1() {
//		11
	}



//==================================================================private

	private void t2() {
//		11
	}



//==================================================================dbg_print

	private void dbg1(String text) {
		env.arenaJFrameEngine.dbg1(text);
	}

	private void dbg2(String text) {
		env.arenaJFrameEngine.dbg(text);
	}

	private void dbg2(Exception e) {
		dbg2(LogUtil.buildStackTraceAsString(e));
	}
}
