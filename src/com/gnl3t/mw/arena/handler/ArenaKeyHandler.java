package com.gnl3t.mw.arena.handler;


import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.constants.ArenaConst;
import org.apache.log4j.Logger;

import java.awt.*;


public class ArenaKeyHandler {
	private final static Logger log = Logger.getLogger(ArenaKeyHandler.class);
	private Environment env;


	public void init(Environment env) {
		log.trace("---");
		this.env = env;
	}


//==================================================================key pressed


	public void handleVK_FPressed() {
		//log.info("KeyEvent.VK_F" + "Pressed");
		if(env.arenaJFrameEngine.showKeyboardPressedKeysFlag) env.arenaJFrameEngine.vkFLabel.setForeground(Color.pink);
		env.k1.xSpeed = ArenaConst.X_SPEED_VALUE;
	}

	public void handleVK_DPressed() {
		//log.info("KeyEvent.VK_D" + "Pressed");
		if(env.arenaJFrameEngine.showKeyboardPressedKeysFlag) env.arenaJFrameEngine.vkDLabel.setForeground(Color.pink);
		env.k1.xSpeed = - ArenaConst.X_SPEED_VALUE;
		env.knightMovementEngine.resetCollisionFlags();
	}

	public void handleVK_CPressed() {
		//log.info("KeyEvent.VK_C" + "Pressed");
		if(env.arenaJFrameEngine.showKeyboardPressedKeysFlag) env.arenaJFrameEngine.vkCLabel.setForeground(Color.pink);
	}

	public void handleVK_LEFTPressed() {
		//log.info("KeyEvent.VK_LEFT" + "Pressed");
		if(env.arenaJFrameEngine.showKeyboardPressedKeysFlag) env.arenaJFrameEngine.vkLeftLabel.setForeground(Color.pink);
		env.k2.xSpeed = - ArenaConst.X_SPEED_VALUE;
	}

	public void handleVK_RIGHTPressed() {
		//log.info("KeyEvent.VK_RIGHT" + "Pressed");
		if(env.arenaJFrameEngine.showKeyboardPressedKeysFlag) env.arenaJFrameEngine.vkRightLabel.setForeground(Color.pink);
		env.k2.xSpeed = ArenaConst.X_SPEED_VALUE;
		env.knightMovementEngine.resetCollisionFlags();
	}

	public void handleVK_DOWNPressed() {
		//log.info("KeyEvent.VK_DOWN" + "Pressed");
		if(env.arenaJFrameEngine.showKeyboardPressedKeysFlag) env.arenaJFrameEngine.vkDownLabel.setForeground(Color.pink);
	}

	public void handleVK_SPACEPressed() {
		//log.info("KeyEvent.VK_SPACE" + "Pressed");
		if(env.arenaJFrameEngine.showKeyboardPressedKeysFlag) env.arenaJFrameEngine.vkSPACELabel.setForeground(Color.pink);
		env.knightAttackEngine.handleAttackStartKnight1();
	}

	public void handleVK_ENTERPressed() {
		//log.info("KeyEvent.VK_ENTER" + "Pressed");
		if(env.arenaJFrameEngine.showKeyboardPressedKeysFlag) env.arenaJFrameEngine.vkENTERLabel.setForeground(Color.pink);
		env.knightAttackEngine.handleAttackStartKnight2();
	}

	public void handleVK_BPressed() {
		//log.info("KeyEvent.VK_B" + "Pressed");
		if(env.arenaJFrameEngine.showKeyboardPressedKeysFlag) env.arenaJFrameEngine.vkBLabel.setForeground(Color.pink);
		env.k1.shieldCoverFlag = true;
	}

	public void handleVK_BACK_SPACEPressed() {
		//log.info("KeyEvent.VK_BACK_SPACE" + "Pressed");
		if(env.arenaJFrameEngine.showKeyboardPressedKeysFlag) env.arenaJFrameEngine.vkBACKSPACELabel.setForeground(Color.pink);
		env.k2.shieldCoverFlag = true;
	}


//==================================================================key released


	public void handleVK_FReleased() {
		//log.info("KeyEvent.VK_F" + "Released");
		if(env.arenaJFrameEngine.showKeyboardPressedKeysFlag) env.arenaJFrameEngine.vkFLabel.setForeground(Color.black);

		env.k1.xSpeed = 0;
	}

	public void handleVK_DReleased() {
		//log.info("KeyEvent.VK_D" + "Released");
		if(env.arenaJFrameEngine.showKeyboardPressedKeysFlag) env.arenaJFrameEngine.vkDLabel.setForeground(Color.black);

		env.k1.xSpeed = 0;
		//resetCollisionFlags();
	}

	public void handleVK_CReleased() {
		//log.info("KeyEvent.VK_C" + "Released");
		if(env.arenaJFrameEngine.showKeyboardPressedKeysFlag) env.arenaJFrameEngine.vkCLabel.setForeground(Color.black);
	}

	public void handleVK_LEFTReleased() {
		//log.info("KeyEvent.VK_LEFT" + "Released");
		if(env.arenaJFrameEngine.showKeyboardPressedKeysFlag) env.arenaJFrameEngine.vkLeftLabel.setForeground(Color.black);

		env.k2.xSpeed = 0;
	}

	public void handleVK_RIGHTReleased() {
		//log.info("KeyEvent.VK_RIGHT" + "Released");
		if(env.arenaJFrameEngine.showKeyboardPressedKeysFlag) env.arenaJFrameEngine.vkRightLabel.setForeground(Color.black);

		env.k2.xSpeed = 0;
		//resetCollisionFlags();
	}

	public void handleVK_DOWNReleased() {
		//log.info("KeyEvent.VK_DOWN" + "Released");
		if(env.arenaJFrameEngine.showKeyboardPressedKeysFlag) env.arenaJFrameEngine.vkDownLabel.setForeground(Color.black);
	}

	public void handleVK_SPACEReleased() {
		//log.info("KeyEvent.VK_SPACE" + "Released");
		env.arenaJFrameEngine.vkSPACELabel.setForeground(Color.black);

		env.knightAttackEngine.handleAttackStopKnight1();
	}

	public void handleVK_ENTERReleased() {
		//log.info("KeyEvent.VK_ENTER" + "Released");
		if(env.arenaJFrameEngine.showKeyboardPressedKeysFlag) env.arenaJFrameEngine.vkENTERLabel.setForeground(Color.black);

		env.knightAttackEngine.handleAttackStopKnight2();
	}

	public void handleVK_BReleased() {
		//log.info("KeyEvent.VK_B" + "Released");
		if(env.arenaJFrameEngine.showKeyboardPressedKeysFlag) env.arenaJFrameEngine.vkBLabel.setForeground(Color.black);

		env.k1.shieldCoverFlag = false;
	}

	public void handleVK_BACK_SPACEReleased() {
		//log.info("KeyEvent.VK_BACK_SPACE" + "Released");
		if(env.arenaJFrameEngine.showKeyboardPressedKeysFlag) env.arenaJFrameEngine.vkBACKSPACELabel.setForeground(Color.black);

		env.k2.shieldCoverFlag = false;
	}

	public void handleVK_ESCAPEReleased() {
		//log.info("KeyEvent.VK_ESCAPE" + "Released");
//					env.initialize = true;
//					env.arenaEngine.initKnights(env);
		env.arenaEngine.reset();
	}

	public void handleVK_F8Released() {
		//log.info("KeyEvent.VK_F8" + "Released");
//		env.arenaJFrameEngine.pause();
		env.arenaMenuEngine.goToArenaPauseMenu();
	}
}
