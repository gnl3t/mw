package com.gnl3t.mw.arena.handler;


import com.gnl3t.mw.data.enums.MenuType;
import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.ArenaScaleUtil;
import com.gnl3t.mw.util.constants.ArenaConst;
import org.apache.log4j.Logger;

import java.awt.*;
import java.awt.geom.Rectangle2D;


public class ArenaRender {
	private final static Logger log = Logger.getLogger(ArenaRender.class);

	public boolean allowFields = false;
	public boolean allowImages = true;
	public boolean allowScaledFields = false;
	public boolean allowScaledImages = false;
	public boolean allowBackgroundImages = false;

	public int lastFpsCounter = 0;
	public int fpsCounter = 0;

	public boolean playedLastActionFlag = false;
	public boolean preparedNextActionFlag = false;



	private Rectangle2D.Double arenaRectangle = new Rectangle2D.Double();
	private Rectangle2D.Double arenaRectangle2 = new Rectangle2D.Double();


	public void render(Graphics2D g2, Environment env) {
		log.trace("---");

//		renderDbgFields(g2, env);

//		if(env.arenaJFrameEngine.onlineFlag && !env.arenaJFrameEngine.pauseFlag) handleNetwork(env);
//		if(env.arenaJFrameEngine.onlineFlag) handleNetwork(env);

		if(env.menuData.currentMenuType == MenuType.MAIN ||
				env.menuData.currentMenuType == MenuType.SETTINGS ||
//				env.menuData.currentMenuType == MenuType.ONLINE_BETA ||
				env.menuData.currentMenuType == MenuType.SOUND_SETTINGS) return;

		if(env.arenaJFrameEngine.onlineFlag) handleNetwork(env);
		renderBars(g2, env);
		if(allowImages) renderImages(g2, env);
		if(allowScaledImages) renderScaledImages(g2, env);
//		if(allowFields) renderFields(g2, env);
//		if(allowScaledFields) renderScaledFields(g2, env);
		renderStrings(g2, env);

		++fpsCounter;
		playedLastActionFlag = true;
//		env.arenaJFrameEngine.fpsLabel.setText("fps=" + fpsCounter++);
//		handleFps(env);
	}

	private void handleNetwork(Environment env) {
//		env.k1.xSpeed = 0;
//		env.k2.xSpeed = 0;

//		if(!env.gameControlsData.playerLeft) {
//		if(true) {
//			env.k1.x = env.gameControlsReceivedData.x1;
//			env.k2.x = env.gameControlsReceivedData.x2;
//			env.k1.body.x = env.gameControlsReceivedData.x1;
//			env.k2.body.x = env.gameControlsReceivedData.x2;
//			env.k1.healthPointsBar.healthPoints = env.gameControlsReceivedData.hp1;
//			env.k2.healthPointsBar.healthPoints = env.gameControlsReceivedData.hp2;
//			env.k1.staminaBar.staminaPoints = env.gameControlsReceivedData.st1;
//			env.k2.staminaBar.staminaPoints = env.gameControlsReceivedData.st2;
//		}


//		if(env.k1.playerDead) env.k1.healthPointsBar.healthPoints = 0;
//		if(env.k2.playerDead) env.k2.healthPointsBar.healthPoints = 0;

//		if(playedLastActionFlag) return;
//		if(env.arenaJFrameEngine.onlineFlag) {
//			env.k1.xSpeed = 0;
//			env.k2.xSpeed = 0;
//		}
//		clearReceivedData(env);

//		env.arenaOnlinePacketsEngine.putMessages();
//		env.arenaOnlinePacketsEngine.sendRequest(); //TODO - rework to separate thread
//		env.arenaOnlinePacketsEngine.handlePackets();
	}

//	private void clearReceivedData(Arenenvironment env) {
//		env.gameControlsReceivedData.right1Pressed = false;
//		env.gameControlsReceivedData.left1Pressed = false;
//		env.gameControlsReceivedData.weapon1Pressed = false;
//		env.gameControlsReceivedData.shield1Pressed = false;
//
//		env.gameControlsReceivedData.right2Pressed = false;
//		env.gameControlsReceivedData.left2Pressed = false;
//		env.gameControlsReceivedData.weapon2Pressed = false;
//		env.gameControlsReceivedData.shield2Pressed = false;
//
//		env.gameControlsReceivedData.right1Released = false;
//		env.gameControlsReceivedData.left1Released = false;
//		env.gameControlsReceivedData.weapon1Released = false;
//		env.gameControlsReceivedData.shield1Released = false;
//
//		env.gameControlsReceivedData.right2Released = false;
//		env.gameControlsReceivedData.left2Released = false;
//		env.gameControlsReceivedData.weapon2Released = false;
//		env.gameControlsReceivedData.shield2Released = false;
//	}

	private void renderDbgFields(Graphics2D g2, Environment env) {
		arenaRectangle.setFrame(0, 0, ArenaConst.ARENA_WIDTH, ArenaConst.ARENA_HEIGHT);
		arenaRectangle2.setFrame(ArenaConst.ARENA_WIDTH, ArenaConst.ARENA_HEIGHT, ArenaConst.ARENA_WIDTH, ArenaConst.ARENA_HEIGHT);
		g2.setColor(Color.GRAY);
		g2.draw(arenaRectangle);
		g2.draw(arenaRectangle2);
	}

	private void renderFields(Graphics2D g2, Environment env) {
//		g2.setColor(Color.GRAY);
//		g2.fill(env.k1.shield.getShieldRectangle());
//		g2.fill(env.k2.shield.getShieldRectangle());
		g2.setColor(Color.GREEN);
		g2.draw(env.k1.body.getBodyRectangle());
		g2.draw(env.k2.body.getBodyRectangle());
		//g2.setColor(Color.GRAY);
//		g2.setColor(Color.red);
//		g2.draw(env.k1.weapon.getWeaponRectangle());
//		g2.draw(env.k2.weapon.getWeaponRectangle());
	}

	private void renderScaledFields(Graphics2D g2, Environment env) {
//		g2.setColor(Color.GRAY);
//		g2.fill(ArenaScaleUtil.reScale(env.k1.shield.getShieldRectangle(), env));
//		g2.fill(env.k2.shield.getShieldRectangle());
		g2.setColor(Color.GREEN);
		g2.draw(ArenaScaleUtil.reScaleWithoutX(env.k1.body.getBodyRectangle(), env));
		g2.draw(ArenaScaleUtil.reScaleWithoutX(env.k2.body.getBodyRectangle(), env));
		//g2.setColor(Color.GRAY);
//		g2.setColor(Color.red);
//		g2.draw(ArenaScaleUtil.reScaleWithoutX(env.k1.weapon.getWeaponRectangle(), env));
//		g2.draw(ArenaScaleUtil.reScaleWithoutX(env.k2.weapon.getWeaponRectangle(), env));
	}

	private void renderImages(Graphics2D g2, Environment env) {
		if(allowBackgroundImages) {
			g2.drawImage(env.arenaImagesEngine.background60, env.k2.body.x, env.k2.body.y, null);
			g2.drawImage(env.arenaImagesEngine.background60, env.k1.body.x, env.k1.body.y, null);
		}

		if(env.arenaJFrameEngine.pauseFlag) {
			if(!env.k2.playerDead) g2.drawImage(env.arenaImagesEngine.image2Stand, env.k2.body.x, env.k2.body.y, null);
			if(!env.k1.playerDead) g2.drawImage(env.arenaImagesEngine.imageStand, env.k1.body.x, env.k1.body.y, null);
			return;
		}

		if (env.k2.shieldCoverFlag) {
			g2.drawImage(env.arenaImagesEngine.image2Shield, env.k2.body.x, env.k2.body.y, null);
		} else if (env.k2.attackAttemptStartFlag && env.k2.staminaBar.staminaPoints > 0) {
			g2.drawImage(env.arenaImagesEngine.image2Attack, env.k2.body.x, env.k2.body.y, null);
		} else {
			g2.drawImage(env.arenaImagesEngine.image2Stand, env.k2.body.x, env.k2.body.y, null);
		}

		if (env.k1.shieldCoverFlag) {
			g2.drawImage(env.arenaImagesEngine.imageShield, env.k1.body.x, env.k1.body.y, null);
		} else if (env.k1.attackAttemptStartFlag && env.k1.staminaBar.staminaPoints > 0) {
			g2.drawImage(env.arenaImagesEngine.imageAttack, env.k1.body.x, env.k1.body.y, null);
		} else {
			g2.drawImage(env.arenaImagesEngine.imageStand, env.k1.body.x, env.k1.body.y, null);
		}

//		g2.drawImage(env.arenaImagesEngine.knightTypesImagesList.get(env.currentKnightType1.ordinal()), 3, 45, env.arenaJFrameEngine.jPanel);
//		g2.drawImage(env.arenaImagesEngine.knightTypesImagesList.get(env.currentKnightType2.ordinal()), 281, 45, env.arenaJFrameEngine.jPanel);
	}

	private void renderScaledImages(Graphics2D g2, Environment env) {
		if(allowBackgroundImages) {
			g2.drawImage(env.arenaImagesEngine.background60Scaled, env.k2.body.x, ArenaScaleUtil.reScale(env.k2.body.y, env), null);
			g2.drawImage(env.arenaImagesEngine.background60Scaled, env.k1.body.x, ArenaScaleUtil.reScale(env.k1.body.y, env), null);
		}
		if(env.arenaJFrameEngine.pauseFlag) {
			if(!env.k2.playerDead) g2.drawImage(env.arenaImagesEngine.image2ScaledStand, env.k2.body.x, ArenaScaleUtil.reScale(env.k2.body.y, env), null);
			if(!env.k1.playerDead) g2.drawImage(env.arenaImagesEngine.imageScaledStand, env.k1.body.x, ArenaScaleUtil.reScale(env.k1.body.y, env), null);
			return;
		}

		if(env.k2.shieldCoverFlag) {
			g2.drawImage(env.arenaImagesEngine.image2ScaledShield, env.k2.body.x, ArenaScaleUtil.reScale(env.k2.body.y, env), null);
		} else if(env.k2.attackAttemptStartFlag && env.k2.staminaBar.staminaPoints > 0) {
			g2.drawImage(env.arenaImagesEngine.image2ScaledAttack, env.k2.body.x, ArenaScaleUtil.reScale(env.k2.body.y, env), null);
		} else {
			g2.drawImage(env.arenaImagesEngine.image2ScaledStand, env.k2.body.x, ArenaScaleUtil.reScale(env.k2.body.y, env), null);
		}

		if(env.k1.shieldCoverFlag) {
			g2.drawImage(env.arenaImagesEngine.imageScaledShield, env.k1.body.x, ArenaScaleUtil.reScale(env.k1.body.y, env), null);
		} else if(env.k1.attackAttemptStartFlag && env.k1.staminaBar.staminaPoints > 0) {
			g2.drawImage(env.arenaImagesEngine.imageScaledAttack, env.k1.body.x, ArenaScaleUtil.reScale(env.k1.body.y, env), null);
		} else {
//			g2.drawImage(env.arenaImagesEngine.imageScaledStand, env.k1.body.x, ArenaScaleUtil.reScale(env.k1.body.y, env), null);
//			g2.drawImage(env.arenaImagesEngine.imageScaledLegsStand, env.k1.body.x, env.k1.body.y + 3, null);
			renderLegs(g2, env);
			g2.drawImage(env.arenaImagesEngine.imageScaledStand, env.k1.body.x, env.k1.body.y, null);
		}
	}

	private void renderLegs(Graphics2D g2, Environment env) {
//		if(env.k1.collisionFlag) {
//			env.k1.stepNumber = 0;
//			env.k1.traveledDistance = 0;
//			g2.drawImage(env.arenaImagesEngine.imageScaledLegsStand, env.k1.body.x, env.k1.body.y + 3, null);
//		} else {
//		}

		if(env.k1.body.x == ArenaConst.ARENA_X_LIMIT_LEFT || env.k1.body.x == ArenaConst.ARENA_X_LIMIT_RIGHT) {
			env.k1.stepNumber = 0;
			env.k1.traveledDistance = 0;
			g2.drawImage(env.arenaImagesEngine.imageScaledLegsStand, env.k1.body.x, env.k1.body.y + 3, null);
		} else {
			if(env.k1.xSpeed != 0) {
				if(env.k1.traveledDistance > 9) {
					env.k1.traveledDistance = 0;
//				env.k1.stepNumber += 1;
					calcStepNumber(env);
//				g2.drawImage(env.arenaImagesEngine.imageScaledLegsStep2, env.k1.body.x, env.k1.body.y + 3, null);
				}
//			else {
//				g2.drawImage(env.arenaImagesEngine.imageScaledLegsStep1, env.k1.body.x, env.k1.body.y + 3, null);
//			}
				if(env.k1.stepNumber == 1) {
					g2.drawImage(env.arenaImagesEngine.imageScaledLegsStep1, env.k1.body.x, env.k1.body.y + 3, null);
				} else if(env.k1.stepNumber == 2) {
					g2.drawImage(env.arenaImagesEngine.imageScaledLegsStep2, env.k1.body.x, env.k1.body.y + 3, null);
				}
//				else if(env.k1.stepNumber == 3) {
//					g2.drawImage(env.arenaImagesEngine.imageScaledLegsStep3, env.k1.body.x, env.k1.body.y + 3, null);
//				} else if(env.k1.stepNumber == 4) {
//					g2.drawImage(env.arenaImagesEngine.imageScaledLegsStep4, env.k1.body.x, env.k1.body.y + 3, null);
//				}
				else {
					g2.drawImage(env.arenaImagesEngine.imageScaledLegsStep1, env.k1.body.x, env.k1.body.y + 3, null);
				}
			}
			else {
				env.k1.stepNumber = 0;
				env.k1.traveledDistance = 0;
				g2.drawImage(env.arenaImagesEngine.imageScaledLegsStand, env.k1.body.x, env.k1.body.y + 3, null);
			}
		}
	}

	private void calcStepNumber(Environment env) {
		if(env.k1.xSpeed > 0) {
			env.k1.stepNumber += 1;
			if(env.k1.stepNumber > 2) {
				env.k1.stepNumber = 1;
			}
		} else if(env.k1.xSpeed < 0) {
			env.k1.stepNumber -= 1;
			if(env.k1.stepNumber < 1) {
				env.k1.stepNumber = 2;
			}
		}

	}

	private void renderBars(Graphics2D g2, Environment env) {
		g2.setColor(Color.red);
		g2.draw(env.k1.healthPointsBar.getHealthPointsBarBoundsScaled());
		g2.draw(env.k2.healthPointsBar.getHealthPointsBarBoundsScaled());
		g2.fill(env.k1.healthPointsBar.getHealthPointsBarScaled());
		g2.fill(env.k2.healthPointsBar.getHealthPointsBarScaled());
		g2.setColor(Color.GRAY);
		g2.draw(env.k1.staminaBar.getStaminaBarBoundsScaled());
		g2.draw(env.k2.staminaBar.getStaminaBarBoundsScaled());
		g2.fill(env.k1.staminaBar.getStaminaBarScaled());
		g2.fill(env.k2.staminaBar.getStaminaBarScaled());

//		g2.setColor(Color.red);
//		if(env.k1.playerDead) {
//			g2.fill(env.k1.body.getBodyRectangle());
//		}
//		if(env.k2.playerDead) {
//			g2.fill(env.k2.body.getBodyRectangle());
//		}
	}

	private void renderStrings(Graphics2D g2, Environment env) {
		if(!env.k1.playerDead && !env.k2.playerDead && !env.arenaJFrameEngine.pauseFlag) return;

		int empiricDiffWidth = ArenaScaleUtil.reScale(ArenaConst.ARENA_WIDTH / 2 - 40, env);
		int empiricDiffHeight = ArenaScaleUtil.reScale(ArenaConst.ARENA_HEIGHT / 2 - 40, env);
		int empiricFontSize = ArenaScaleUtil.reScale(20, env);
		if(env.k1.playerDead) {
//            env.k1.healthPointsBar.healthPoints = 0;
			if(env.gameControlsData.playerLeft) {
                renderDefeat(g2, env);
            } else {
                renderVictory(g2, env);
            }
		} else if(env.k2.playerDead) {
//            env.k2.healthPointsBar.healthPoints = 0;
            if(env.gameControlsData.playerLeft) {
                renderVictory(g2, env);
            } else {
                renderDefeat(g2, env);
            }
		} else if(env.arenaJFrameEngine.pauseFlag) {
			g2.setFont(new Font("TimesRoman", Font.BOLD, empiricFontSize));
			g2.setColor(Color.gray);
			g2.drawString("PAUSE", empiricDiffWidth, empiricDiffHeight);
		}
	}

	private void renderVictory(Graphics2D g2, Environment env) {
        int empiricDiffWidth = ArenaScaleUtil.reScale(ArenaConst.ARENA_WIDTH / 2 - 40, env);
        int empiricDiffHeight = ArenaScaleUtil.reScale(ArenaConst.ARENA_HEIGHT / 2 - 40, env);
        int empiricFontSize = ArenaScaleUtil.reScale(20, env);

        g2.setFont(new Font("TimesRoman", Font.BOLD, empiricFontSize));
        g2.setColor(Color.green);
        g2.drawString("VICTORY", empiricDiffWidth, empiricDiffHeight);
	}

	private void renderDefeat(Graphics2D g2, Environment env) {
        int empiricDiffWidth = ArenaScaleUtil.reScale(ArenaConst.ARENA_WIDTH / 2 - 40, env);
        int empiricDiffHeight = ArenaScaleUtil.reScale(ArenaConst.ARENA_HEIGHT / 2 - 40, env);
        int empiricFontSize = ArenaScaleUtil.reScale(20, env);

        g2.setFont(new Font("TimesRoman", Font.BOLD, empiricFontSize));
        g2.setColor(Color.red);
        g2.drawString("DEFEAT", empiricDiffWidth, empiricDiffHeight);
	}

	public void handleFps(Environment env) {
		env.arenaJFrameEngine.fpsLabel.setText("fps=" + lastFpsCounter++);
		lastFpsCounter = fpsCounter;
		fpsCounter = 0;
	}
}
