package com.gnl3t.mw.arena.handler;


import com.gnl3t.mw.environment.Environment;
import org.apache.log4j.Logger;


public class ArenaEngineTimer {
	private final static Logger log = Logger.getLogger(ArenaEngineTimer.class);
	private Environment env;

	private long firstSecondTimeMark = System.currentTimeMillis();
	private long firstCustomPeriodTimeMark = firstSecondTimeMark;
	private long firstBotDelayBetweenAttacksMark = firstSecondTimeMark;
	private long firstHalfSecondTimeMark = firstSecondTimeMark;
	private long firstTwoSecondsTimeMark = firstSecondTimeMark;
	private long currentTimeMark;


	public void init(Environment env) {
		log.trace("---");
		this.env = env;
	}


	public void calcTimePassed() {
		log.trace("---");
		currentTimeMark = System.currentTimeMillis();

		if((currentTimeMark - firstCustomPeriodTimeMark) > 60)  customPeriodTimePassed();
		if((currentTimeMark - firstBotDelayBetweenAttacksMark) > 150) botDelayBetweenAttacksPassed();
		if((currentTimeMark - firstHalfSecondTimeMark) > 500) oneHalfSecondPassed();
		if((currentTimeMark - firstSecondTimeMark) > 1000) oneSecondPassed();
		if((currentTimeMark - firstTwoSecondsTimeMark) > 2000) twoSecondsPassed();
	}

	private void customPeriodTimePassed() {
//		log.info("customPeriodTimePassed");
		firstCustomPeriodTimeMark = currentTimeMark;
	}

	private void botDelayBetweenAttacksPassed() {
//		log.info("botDelayBetweenAttacksPassed");
		firstBotDelayBetweenAttacksMark = currentTimeMark;

		env.arenaBotEngine.attack();
	}

	private void oneHalfSecondPassed() {
//		log.info("oneHalfSecondPassed");
		firstHalfSecondTimeMark = currentTimeMark;

//		try {
//			env.arenaUdpClientEngine.udpClient.punchHole();
//		} catch (IOException e) {
//			Log4jUtil.logException(log, e);
//		}
	}

	private void oneSecondPassed() {
//		log.info("oneSecondPassed");
		firstSecondTimeMark = currentTimeMark;

//		if(env.gameControlsData.playerLeft) {
//			env.knightStaminaEngine.calcStaminaRegenKnight1();
//			env.knightStaminaEngine.calcStaminaRegenKnight2();
//		}
		env.knightStaminaEngine.calcStaminaRegenKnight1();
		env.knightStaminaEngine.calcStaminaRegenKnight2();

//		env.arenaJFrameEngine.fpsLabel.setText("fps=" + env.arenaRender.lastFpsCounter++);
//		env.arenaRender.lastFpsCounter = env.arenaRender.fpsCounter;
//		env.arenaRender.fpsCounter = 0;
		env.arenaRender.handleFps(env);
	}

	private void twoSecondsPassed() {
//		log.info("twoSecondsPassed");
		firstTwoSecondsTimeMark = currentTimeMark;

		env.knightHealthEngine.calcHealthPointsRegenKnight1();
		env.knightHealthEngine.calcHealthPointsRegenKnight2();
	}

}
