package com.gnl3t.mw.arena.handler;


import com.gnl3t.mw.environment.Environment;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


public class ArenaKeyListener {
	private final static Logger log = Logger.getLogger(ArenaKeyListener.class);
	private Environment env;


	public void init(Environment env) {
		log.trace("---");
		this.env = env;
	}

	private void send(String contentString) {
		log.trace("---");
		if(this.env == null || this.env.arenaTcpClientEngine == null) return;

//		this.env.arenaTcpClientEngine.sendMessages(contentString);
//		this.env.arenaTcpClientEngine.putMessage(contentString);
		this.env.arenaUdpClientEngine.putMessage(contentString);
	}


	public void initKeyListener(JPanel jPanel) {
		log.trace("---");


		jPanel.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
//				sendMessages("Pressed" + e.getKeyCode());
//				log.info("Key Pressed: " + e.getKeyChar());
				if(env.k1.playerDead || env.k2.playerDead) {
					return;
				}
				if (e.getKeyCode() == KeyEvent.VK_F) {
					handleVK_FPressed();
				} if (e.getKeyCode() == KeyEvent.VK_D) {
					handleVK_DPressed();
				} if (e.getKeyCode() == KeyEvent.VK_C) {
					handleVK_CPressed();
				} if (e.getKeyCode() == KeyEvent.VK_LEFT) {
					handleVK_LEFTPressed();
				} if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
					handleVK_RIGHTPressed();
				} if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					handleVK_DOWNPressed();
				} if (e.getKeyCode() == KeyEvent.VK_SPACE) {
					handleVK_SPACEPressed();
				} if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					handleVK_ENTERPressed();
				} if (e.getKeyCode() == KeyEvent.VK_B) {
					handleVK_BPressed();
				} if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
					handleVK_BACK_SPACEPressed();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) {
//				sendMessages("Released" + e.getKeyCode());
//        	  log.info("Key Released: " + e.getKeyChar());
				if (e.getKeyCode() == KeyEvent.VK_F) {
					handleVK_FReleased();
				} if (e.getKeyCode() == KeyEvent.VK_D) {
					handleVK_DReleased();
				} if (e.getKeyCode() == KeyEvent.VK_C) {
					handleVK_CReleased();
				} if (e.getKeyCode() == KeyEvent.VK_LEFT) {
					handleVK_LEFTReleased();
				} if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
					handleVK_RIGHTReleased();
				} if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					handleVK_DOWNReleased();
				} if (e.getKeyCode() == KeyEvent.VK_SPACE) {
					handleVK_SPACEReleased();
				} if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					handleVK_ENTERReleased();
				} if (e.getKeyCode() == KeyEvent.VK_B) {
					handleVK_BReleased();
				} if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
					handleVK_BACK_SPACEReleased();
				} if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					handleVK_ESCAPEReleased();
				} if (e.getKeyCode() == KeyEvent.VK_F8) {
					handleVK_F8Released();
				}
			}
			@Override
			public void keyTyped(KeyEvent e) {
				//log.info("Key Typed: " + e.getKeyChar() + " " + KeyEvent.getKeyModifiersText(e.getModifiers()));
			}
		});
	}


//==================================================================key pressed


	public void handleVK_FPressed() {
		//log.info("KeyEvent.VK_F" + "Pressed");
		if(env.arenaJFrameEngine.onlineFlag) {
			env.gameControlsData.right1Pressed = true;
		} else {
			env.arenaKeyHandler.handleVK_FPressed();
		}
	}

	public void handleVK_DPressed() {
		//log.info("KeyEvent.VK_D" + "Pressed");
		if(env.arenaJFrameEngine.onlineFlag) {
			env.gameControlsData.left1Pressed = true;
		} else {
			env.arenaKeyHandler.handleVK_DPressed();
		}
	}

	public void handleVK_CPressed() {
		//log.info("KeyEvent.VK_C" + "Pressed");
		if(env.arenaJFrameEngine.showKeyboardPressedKeysFlag) env.arenaJFrameEngine.vkCLabel.setForeground(Color.pink);
	}

	public void handleVK_LEFTPressed() {
		//log.info("KeyEvent.VK_LEFT" + "Pressed");
		if(env.arenaJFrameEngine.onlineFlag) {
			env.gameControlsData.left2Pressed = true;
		} else {
			env.arenaKeyHandler.handleVK_LEFTPressed();
		}
	}

	public void handleVK_RIGHTPressed() {
		//log.info("KeyEvent.VK_RIGHT" + "Pressed");
		if(env.arenaJFrameEngine.onlineFlag) {
			env.gameControlsData.right2Pressed = true;
		} else {
			env.arenaKeyHandler.handleVK_RIGHTPressed();
		}
	}

	public void handleVK_DOWNPressed() {
		//log.info("KeyEvent.VK_DOWN" + "Pressed");
		if(env.arenaJFrameEngine.showKeyboardPressedKeysFlag) env.arenaJFrameEngine.vkDownLabel.setForeground(Color.pink);
	}

	public void handleVK_SPACEPressed() {
		//log.info("KeyEvent.VK_SPACE" + "Pressed");
		if(env.arenaJFrameEngine.onlineFlag) {
			env.gameControlsData.weapon1Pressed = true;
		} else {
			env.arenaKeyHandler.handleVK_SPACEPressed();
		}
	}

	public void handleVK_ENTERPressed() {
		//log.info("KeyEvent.VK_ENTER" + "Pressed");
		if(env.arenaJFrameEngine.onlineFlag) {
			env.gameControlsData.weapon2Pressed = true;
		} else {
			env.arenaKeyHandler.handleVK_ENTERPressed();
		}
	}

	public void handleVK_BPressed() {
		//log.info("KeyEvent.VK_B" + "Pressed");
		if(env.arenaJFrameEngine.onlineFlag) {
			env.gameControlsData.shield1Pressed = true;
		} else {
			env.arenaKeyHandler.handleVK_BPressed();
		}
	}

	public void handleVK_BACK_SPACEPressed() {
		//log.info("KeyEvent.VK_BACK_SPACE" + "Pressed");
		if(env.arenaJFrameEngine.onlineFlag) {
			env.gameControlsData.shield2Pressed = true;
		} else {
			env.arenaKeyHandler.handleVK_BACK_SPACEPressed();
		}
	}


//==================================================================key released


	public void handleVK_FReleased() {
		//log.info("KeyEvent.VK_F" + "Released");
		if(env.arenaJFrameEngine.onlineFlag) {
			env.gameControlsData.right1Pressed = false;
			env.gameControlsData.right1Released = true;
		} else {
			env.arenaKeyHandler.handleVK_FReleased();
		}

	}

	public void handleVK_DReleased() {
		//log.info("KeyEvent.VK_D" + "Released");
		if(env.arenaJFrameEngine.onlineFlag) {
			env.gameControlsData.left1Pressed = false;
			env.gameControlsData.left1Released = true;
		} else {
			env.arenaKeyHandler.handleVK_DReleased();
		}
	}

	public void handleVK_CReleased() {
		//log.info("KeyEvent.VK_C" + "Released");
		if(env.arenaJFrameEngine.showKeyboardPressedKeysFlag) env.arenaJFrameEngine.vkCLabel.setForeground(Color.black);
	}

	public void handleVK_LEFTReleased() {
		//log.info("KeyEvent.VK_LEFT" + "Released");
		if(env.arenaJFrameEngine.onlineFlag) {
			env.gameControlsData.left2Pressed = false;
			env.gameControlsData.left2Released = true;
		} else {
			env.arenaKeyHandler.handleVK_LEFTReleased();
		}
	}

	public void handleVK_RIGHTReleased() {
		//log.info("KeyEvent.VK_RIGHT" + "Released");
		if(env.arenaJFrameEngine.onlineFlag) {
			env.gameControlsData.right2Pressed = false;
			env.gameControlsData.right2Released = true;
		} else {
			env.arenaKeyHandler.handleVK_RIGHTReleased();
		}
	}

	public void handleVK_DOWNReleased() {
		//log.info("KeyEvent.VK_DOWN" + "Released");
		if(env.arenaJFrameEngine.showKeyboardPressedKeysFlag) env.arenaJFrameEngine.vkDownLabel.setForeground(Color.black);
	}

	public void handleVK_SPACEReleased() {
		//log.info("KeyEvent.VK_SPACE" + "Released");
		if(env.arenaJFrameEngine.onlineFlag) {
			env.gameControlsData.weapon1Pressed = false;
			env.gameControlsData.weapon1Released = true;
		} else {
			env.arenaKeyHandler.handleVK_SPACEReleased();
		}
	}

	public void handleVK_ENTERReleased() {
		//log.info("KeyEvent.VK_ENTER" + "Released");
		if(env.arenaJFrameEngine.onlineFlag) {
			env.gameControlsData.weapon2Pressed = false;
			env.gameControlsData.weapon2Released = true;
		} else {
			env.arenaKeyHandler.handleVK_ENTERReleased();
		}
	}

	public void handleVK_BReleased() {
		//log.info("KeyEvent.VK_B" + "Released");
		if(env.arenaJFrameEngine.onlineFlag) {
			env.gameControlsData.shield1Pressed = false;
			env.gameControlsData.shield1Released = true;
		} else {
			env.arenaKeyHandler.handleVK_BReleased();
		}
	}

	public void handleVK_BACK_SPACEReleased() {
		//log.info("KeyEvent.VK_BACK_SPACE" + "Released");
		if(env.arenaJFrameEngine.onlineFlag) {
			env.gameControlsData.shield2Pressed = false;
			env.gameControlsData.shield2Released = true;
		} else {
			env.arenaKeyHandler.handleVK_BACK_SPACEReleased();
		}
	}

	public void handleVK_ESCAPEReleased() {
		//log.info("KeyEvent.VK_ESCAPE" + "Released");
//					env.initialize = true;
//					env.arenaEngine.initKnights(env);
		env.arenaEngine.reset();
	}

	public void handleVK_F8Released() {
		//log.info("KeyEvent.VK_F8" + "Released");
//		env.arenaJFrameEngine.pause();
		env.arenaMenuEngine.goToArenaPauseMenu();
	}
}
