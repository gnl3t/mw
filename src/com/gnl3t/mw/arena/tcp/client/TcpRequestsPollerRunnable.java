package com.gnl3t.mw.arena.tcp.client;


import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.Log4jUtil;
import org.apache.log4j.Logger;


public class TcpRequestsPollerRunnable implements Runnable {
	private final static Logger log = Logger.getLogger(TcpRequestsPollerRunnable.class);
	private Environment env;


	public TcpRequestsPollerRunnable(Environment env) {
		init(env);
	}

	public void init(Environment env) {
		log.trace("---");
		this.env = env;
	}


	@Override
	public void run() {
		log.trace("---");

		while(true) {
//			env.arenaOnlinePacketsEngine.sendRequest();
			env.arenaTcpClientEngine.send();
//			env.arenaTcpClientEngine.sendMessages();

			try {
//				wait(10);
//				Thread.sleep(10);
				Thread.sleep(1);
			} catch (InterruptedException e) {
//				e.printStackTrace();
				Log4jUtil.logException(log, e);
			}
		}
	}


}
