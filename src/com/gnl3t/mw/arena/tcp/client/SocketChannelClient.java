package com.gnl3t.mw.arena.tcp.client;


import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;


public class SocketChannelClient {
	final static Logger log = Logger.getLogger(SocketChannelClient.class);

	boolean debugFlag = true;

	private static SocketChannel socketChannel;
	private static ByteBuffer byteBuffer;
	private static SocketChannelClient instance;


	static int requestsHandledNumber = 0;
	static final int REQUESTS_HANDLED_NUMBER_LIMIT = 50;
	public static int byteBufferSize = 128000;

    public String HOST;
	public int PORT;

	public SocketChannelClient start() {
		if (instance == null)
			instance = new SocketChannelClient(HOST, PORT);

		return instance;
	}

	public void stop() {
		try {
			if(socketChannel != null) {
				socketChannel.close();
				dbg("disconnected from " + getHostPortString());
			}
		} catch (IOException e) {
			//TODO - log all exceptions with log4j
			e.printStackTrace();
		}
		byteBuffer = null;
	}

	public String getHostPortString() {
		return HOST + ":" + PORT;
	}

	public void connect() throws IOException {
//			HOST = "localhost";
//			PORT = port;
		socketChannel = SocketChannel.open(new InetSocketAddress(HOST, PORT));
//			socketChannel = SocketChannel.open(new InetSocketAddress("localhost", port));
//			socketChannel = SocketChannel.open(new InetSocketAddress("bimmgw1.co.uk", port));//centos
//			socketChannel = SocketChannel.open(new InetSocketAddress("157.230.116.123", port));//centos
//			socketChannel = SocketChannel.open(new InetSocketAddress("192.168.1.178", port));

		byteBuffer = ByteBuffer.allocate(byteBufferSize);

		dbg("connected to " + getHostPortString());
	}

	public SocketChannelClient(String host, int port) {
		HOST = host;
		PORT = port;
	}

	public synchronized void putMessage(String message) {
		String response = null;
	}

	public synchronized String sendMessage(String message) {
		String response = null;
		try {
			if(socketChannel == null) {
				return "not initialized";
			}
			putSocketMessage(socketChannel, message);
			dbg("message=" + message);
			response = getSocketMessage(socketChannel);
//			dbg("response=" + response);
			dbg("response=\n" + response);
		} catch (Exception e) {
			e.printStackTrace();
			response = "Exception: " + e.getMessage();
		}
		callGarbageCollector();
		return response;
	}






//==================================================================

	private synchronized void putSocketMessage(SocketChannel socketChannel, String message) throws Exception {
		byteBuffer = ByteBuffer.wrap(message.getBytes());
		if(socketChannel == null) {
			//log.debug("not initialized");
//			dbg("not initialized");
//			return;
			throw new Exception("not initialized");
		}
		socketChannel.write(byteBuffer);
	}

	private synchronized String getSocketMessage(SocketChannel socketChannel) throws IOException {
		byteBuffer = ByteBuffer.allocate(byteBufferSize);
		socketChannel.read(byteBuffer);
		return new String(byteBuffer.array()).trim();
	}

	private void callGarbageCollector() {
		if (++requestsHandledNumber > REQUESTS_HANDLED_NUMBER_LIMIT) {
			requestsHandledNumber = 0;
			System.gc();
		}
	}

	public void dbg(String sourceString) {
		if(debugFlag) log.debug(sourceString);
	}
}
