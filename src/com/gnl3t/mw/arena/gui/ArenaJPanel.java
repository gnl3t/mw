package com.gnl3t.mw.arena.gui;


import com.gnl3t.mw.environment.Environment;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class ArenaJPanel extends JPanel implements ActionListener {
	private final static Logger log = Logger.getLogger(ArenaJPanel.class);
	private Environment env;
	public Timer redrawTimer;


	public ArenaJPanel(Environment env) {
		log.trace("---");
		this.env = env;
		init();
	}

	private void init() {
		log.trace("---");

		env.arenaEngine.initKnights();
		env.arenaKeyListener.initKeyListener(this);
		env.arenaJFrameEngine.initFrame(this, env);
		initRedrawTimer();


		dbgInfoPrint();
	}

	private void initRedrawTimer() {
		log.trace("---");
		redrawTimer = new Timer(20, this);
		redrawTimer.setInitialDelay(190);
		redrawTimer.start();
	}

	private void dbgInfoPrint() {
		log.trace("---");

//		log.debug(env.k1.getDebugPrint());
//		log.debug(env.k1.body.getDebugPrint());
//		log.debug(env.k2.getDebugPrint());
//		log.debug(env.k2.body.getDebugPrint());
	}


//==================================================================overrides


	public void paint(Graphics g) {
		log.trace("---");

	    super.paintComponent(g);
	    Graphics2D g2 = (Graphics2D) g;
	    if(env.initialize) {
	    	env.arenaEngine.reset();
		    env.initialize = false;
	    }
	    if(!env.arenaJFrameEngine.onlineFlag) env.arenaEngine.stepCadre();
//	    env.arenaEngine.stepCadre();
	    env.arenaRender.render(g2, env);
	}

	public void actionPerformed(ActionEvent e) {
		log.trace("---");
        repaint();
	}


}
