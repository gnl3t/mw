package com.gnl3t.mw.environment;


import com.gnl3t.mw.arena.engine.*;
import com.gnl3t.mw.arena.engine.knight.*;
import com.gnl3t.mw.arena.engine.media.ArenaAudioEngine;
import com.gnl3t.mw.arena.engine.media.ArenaImagesEngine;
import com.gnl3t.mw.arena.handler.ArenaKeyHandler;
import com.gnl3t.mw.arena.handler.ArenaKeyListener;
import com.gnl3t.mw.arena.handler.ArenaRender;
import com.gnl3t.mw.arena.udp.client.UdpRequestsPollerEmulatorRunnable;
import com.gnl3t.mw.arena.udp.client.UdpRequestsPollerRunnable;
import com.gnl3t.mw.data.arena.*;
import com.gnl3t.mw.data.construct.KnightData;
import com.gnl3t.mw.data.enums.KnightType;
import com.gnl3t.mw.util.Log4jUtil;
import com.gnl3t.mw.util.PropertiesUtil;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.util.Properties;


public class Environment {
	private final static Logger log = Logger.getLogger(Environment.class);
	public Properties config;
	public Properties knightTypesConfig;


	public boolean debugFlag = false;


//==================================================================variables
	public String playerLeftString = "playerLeft";
    public String matchId = "-1";
	public JFrame arenaFrame;
	public JFrame menuFrame;
	public int frameWidth;
	public int frameHeight;
	public float screenDensity = 1.0f;
//	public float screenDensity = 1.2f;
//	public float screenDensity = 2.0f;
//	public float screenDensity = 2.5f;
//	public float screenDensity = 3.0f;
//	public float screenDensity = 4.0f;
	public KnightType currentKnightType1 = KnightType.SWORDSMAN;
	public KnightType currentKnightType2 = KnightType.SWORDSMAN;

//==================================================================flags
	public boolean initialize = true;

//==================================================================data
	public ArenaProgressData arenaProgressData;
	public BotData botData;
	public MenuData menuData;
	public GameControlsData gameControlsData = new GameControlsData();
	public GameControlsReceivedData gameControlsReceivedData = new GameControlsReceivedData();
	public long lastFactorial = -1;
	public KnightData k1;
	public KnightData k2;
    public UserArmoryData userArmoryData = new UserArmoryData();

//==================================================================engines
	public ArenaAudioEngine arenaAudioEngine;
	public ArenaBotEngine arenaBotEngine;
	public ArenaEngine arenaEngine;
	public ArenaImagesEngine arenaImagesEngine;
	public ArenaJFrameEngine arenaJFrameEngine;
	public ArenaMenuEngine arenaMenuEngine;
	public ArenaProgressEngine arenaProgressEngine;
	public UserArmoryEngine userArmoryEngine = new UserArmoryEngine();

//==================================================================network engines
	public ArenaOnlineBetaEngine arenaOnlineBetaEngine = new ArenaOnlineBetaEngine();
    public ArenaOnlinePacketsEngine arenaOnlinePacketsEngine = new ArenaOnlinePacketsEngine();
	public ArenaTcpClientEngine arenaTcpClientEngine = new ArenaTcpClientEngine();
	public ArenaUdpClientEngine arenaUdpClientEngine = new ArenaUdpClientEngine();
//	public ArenaUdpClientEngine arenaUdpClientEngine2 = new ArenaUdpClientEngine();
//	public UdpRequestsPollerRunnable udpRequestsPollerRunnable;
	public UdpRequestsPollerEmulatorRunnable udpRequestsPollerEmulatorRunnable;

//==================================================================handlers
	public ArenaKeyListener arenaKeyListener;
	public ArenaRender arenaRender;
	public ArenaKeyHandler arenaKeyHandler = new ArenaKeyHandler();

//==================================================================knight engines
	public KnightAttackEngine knightAttackEngine;
	public KnightHealthEngine knightHealthEngine;
	public KnightMovementEngine knightMovementEngine;
	public KnightShieldEngine knightShieldEngine;
	public KnightStaminaEngine knightStaminaEngine;
	public KnightWeaponEngine knightWeaponEngine;


	public Environment(Properties config) throws Exception {
		try {
			log.trace("---");
			init(config);
		} catch(Exception e) {
			Log4jUtil.logException(log, e);
			throw e;
		}
	}

	private void init(Properties config) throws Exception {
		log.trace("---");
		try {
			this.config = config;
			knightTypesConfig = PropertiesUtil.readConfig("./config/knight_types.properties");

			arenaProgressData = new ArenaProgressData();
			botData = new BotData();
			menuData = new MenuData();

			arenaAudioEngine = new ArenaAudioEngine();
			arenaBotEngine = new ArenaBotEngine();
			arenaEngine = new ArenaEngine();
			arenaImagesEngine = new ArenaImagesEngine();
			arenaMenuEngine = new ArenaMenuEngine();
			arenaJFrameEngine = new ArenaJFrameEngine();
			arenaProgressEngine = new ArenaProgressEngine();

			arenaKeyListener = new ArenaKeyListener();
			arenaRender = new ArenaRender();

			knightAttackEngine = new KnightAttackEngine();
			knightHealthEngine = new KnightHealthEngine();
			knightMovementEngine = new KnightMovementEngine();
			knightShieldEngine = new KnightShieldEngine();
			knightStaminaEngine = new KnightStaminaEngine();
			knightWeaponEngine = new KnightWeaponEngine();

			arenaAudioEngine.init(this);
			arenaBotEngine.init(this);
			arenaEngine.init(this);
			arenaKeyHandler.init(this);

			arenaKeyListener.init(this);
			arenaImagesEngine.init(this);
			arenaMenuEngine.init(this);

            arenaOnlineBetaEngine.init(this);
            arenaOnlinePacketsEngine.init(this);
			arenaTcpClientEngine.init(this);
			arenaUdpClientEngine.init(this);
//			arenaUdpClientEngine2.init(this);

            userArmoryEngine.init(this);
		} catch (Exception e) {
			Log4jUtil.logException(log, e);
			throw e;
		}
    }

}
