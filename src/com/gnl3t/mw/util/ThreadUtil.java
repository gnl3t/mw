package com.gnl3t.mw.util;

import org.apache.log4j.Logger;

public class ThreadUtil {

    public static void sleep() {
        //logger.debug("sleep...");
        sleep(1000);
    }

    public static void sleep(int millisec) {
        //logger.debug("sleep...");
        try {
            Thread.sleep(millisec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void sleep(int millisec, Logger logger) {
        //logger.debug("sleep...");
        logger.debug("sleep " + millisec);
        try {
            Thread.sleep(millisec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
