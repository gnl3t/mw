package com.gnl3t.mw.util;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;


public class SwingUtil {


    public static JLabel createTextLabel(String text) {
        JLabel label = new JLabel();
        label.setText(text);
        label.setHorizontalAlignment(SwingConstants.CENTER);

        return label;
    }

    public static JLabel createTextLabel(String text, Color color) {
        JLabel label = new JLabel();
        label.setText(text);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setForeground(color);

        return createTextLabel(text, color, null);
    }

    public static JLabel createTextLabel(String text, Color colorText, Color colorBackground) {
        JLabel label = new JLabel();
        label.setText(text);
        label.setHorizontalAlignment(SwingConstants.CENTER);

        if(colorText != null) label.setForeground(colorText);
        if(colorBackground != null) {
            label.setBackground(colorBackground);
            label.setOpaque(true);
        }

        return label;
    }

    public static JPanel createEmptyPanel() {
        JPanel panel = new JPanel();
        //panel.setPreferredSize(new Dimension(frameWidth, 100));
        //setPanelsColor(panel, Color.GREEN);

        panel.setLayout(new GridLayout(1, 1));

        return panel;
    }

    public static JPanel createTextPanel(String text) {
        return createTextPanel(text, null);
    }

    public static JPanel createTextPanel(String text, Color color) {
        JPanel panel = new JPanel();
        //panel.setPreferredSize(new Dimension(frameWidth, 100));
        //setPanelsColor(panel, Color.GREEN);

        panel.setLayout(new GridLayout(1, 1));
        JLabel label = new JLabel();
        label.setText(text);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        if(color != null) {
            label.setBackground(color);
            label.setOpaque(true);
        }
        //JLabel label = new JLabel(text, SwingConstants.CENTER);
        panel.add(label);

        return panel;
    }

    public static JTextArea createTextArea(String text) {
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        JTextArea textArea = new JTextArea(text);
        textArea.setBorder(border);

        return textArea;
    }

    public static void setPanelsColor(boolean debugPanelColorsFlag, JPanel jPanel, Color color) {
        if(debugPanelColorsFlag) {
            jPanel.setBackground(color);
        }
    }
}
