package com.gnl3t.mw.util.constants;


public class StringConst {
	//strings
	public static final String FREE = "FREE";
	public static final String HDD = "HDD";
	public static final String IMPULSE = "IMPULSE";
	public static final String NULL_LOWER_CASE = "null";
	public static final String UPTIME = "UPTIME";

	//symbols
	public static final String BACK_SLASH_ESCAPED_SYMBOL = "\\";
	public static final String BACK_SLASH_N = "\n";
	public static final String COLON_SYMBOL = ":";
	public static final String COMMA_SYMBOL = ",";
	public static final String DELIMITER_SYMBOL = "\n|\n";
	public static final String DOT_SYMBOL = ".";
	public static final String DOUBLE_BACK_SLASH_ESCAPED_SYMBOL = "\\\\";
	public static final String EMPTY_STRING = "";
	public static final String EQUALS_SYMBOL = "=";
	public static final String FORWARD_SLASH_SYMBOL = "/";
	public static final String PERCENT_SYMBOL = "%";
	public static final String SPACE_SYMBOL = " ";
	public static final String UNDERSCORE_SYMBOL = "_";

	//flags
	public static final String ARROW_RIGHT = "->";
	public static final String BEGIN = "BEGIN";
	public static final String ESCAPE_DIRECTORY_SYMBOL = "%DIR%";
	public static final String SPLIT_FLAG = "SPLIT";
	public static final String END = "END";

	//algorithms
	public static final String SHA_256 = "SHA-256";


	public static final String LOG_SEPARATOR = "==================================================================";
	public static final String LOG_SEPARATOR_DOUBLE = "====================================================================================================================================";

}
