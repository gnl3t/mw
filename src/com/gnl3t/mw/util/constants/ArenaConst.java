package com.gnl3t.mw.util.constants;


import com.gnl3t.mw.arena.engine.ArenaJFrameEngine;

public class ArenaConst {
	//	public static final int ARENA_WIDTH = 350;
	public static final int ARENA_WIDTH = 500;
	public static final int ARENA_HEIGHT = 250;
	public static final int SCENE_LEVEL = 120;
	public static final int HEALTH_POINTS_BAR_LEVEL = 20;
	public static final int STAMINA_BAR_LEVEL = 34;
	public static final int HEALTH_POINTS_BAR_HEIGHT = 12;
	public static final int STAMINA_BAR_HEIGHT = 7;
//	public static final int X_SPEED_VALUE = 1;
//	public static final int X_SPEED_VALUE = 2;
	public static final int X_SPEED_VALUE = ArenaJFrameEngine.localHostFlag ? 2 : 1;

	public static final int ARENA_X_LIMIT_LEFT = 1;
	//	public static final int ARENA_X_LIMIT_RIGHT = 285;
	public static final int ARENA_X_LIMIT_RIGHT = 390;

	public static final int SHIELD_ARMOR = 8;

	public static final int IMAGE_WIDTH = 50;
	public static final int IMAGE_HEIGHT = 50;

//	public static final double SOUND_VOLUME_LEVEL = 0.08;
	public static final double SOUND_VOLUME_LEVEL = 0.02;

	public static final String BASIC_PATH = ".";
	public static final String BASIC_PATH_IMG = BASIC_PATH + "/img/";

 
}
