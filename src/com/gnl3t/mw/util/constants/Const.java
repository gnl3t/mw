package com.gnl3t.mw.util.constants;


import java.util.ArrayList;
import java.util.HashMap;


public class Const {
//	public static final int SCENE_LEVEL = 120;

    public static final String PROJECT_ABSOLUTE_PATH = System.getProperty("user.dir");
    public static final String LOG4J_PROJECT_ABSOLUTE_PATH = PROJECT_ABSOLUTE_PATH + "/config/log4j.xml";


//==================================================================methods


    public static ArrayList getNewArrayList() {
        return new ArrayList<>();
    }

    public static HashMap getNewHashMap() {
        return new HashMap<>();
    }

 
}
