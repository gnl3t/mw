package com.gnl3t.mw.util;


import com.gnl3t.mw.util.constants.StringConst;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class RequestUtils {
    final static Logger log = Logger.getLogger(RequestUtils.class);

    public static synchronized TreeMap<String, String> createEmptyMessageTree() {
        return new TreeMap<>();
    }

    public static synchronized String getProperty(String propertyName, String requestBody, String[] subStrings) {
        if(requestBody.contains(propertyName)) {
            for (String subString : subStrings) {
                if(subString.contains(propertyName)) {
                    return subString.split(StringConst.EQUALS_SYMBOL)[1];
                }
            }
        }
        return null;
    }

    public static synchronized Map<String, String> getMap(String requestBody) {
        Map<String, String> result = new HashMap<>();
        String[] subStrings = requestBody.split(StringConst.BACK_SLASH_N);
        for (String subString : subStrings) {
            String[] values = subString.split(StringConst.EQUALS_SYMBOL);
            if(values.length > 1) {
                result.put(values[0], values[1]);
            } else {
                result.put(values[0], null);
            }

        }
        return result;
    }

    public static synchronized TreeMap<String, String> parseToTreeMap(String src) {
        TreeMap<String, String> result = new TreeMap<>();

        String[] subStrings = src.split(StringConst.BACK_SLASH_N);
        for (String subString : subStrings) {
//            if(subString.contains(StringConst.EQUALS_SYMBOL)) {
//                String[] values = subString.split(StringConst.EQUALS_SYMBOL);
//                if(values.length > 0) result.put(values[0], values[1]);
//            }
            if(!subString.contains(StringConst.EQUALS_SYMBOL)) continue;

            String[] values = subString.split(StringConst.EQUALS_SYMBOL);
            if(values.length > 0) result.put(values[0], values[1]);
        }

        return result;
    }

//    public static synchronized TreeMap<String, String> parseToTreeMap2(String src) {
//        TreeMap<String, String> result = new TreeMap<>();
//
//        try {
//            String[] subStrings = src.split(StringConst.BACK_SLASH_N);
//            for (String subString : subStrings) {
//                if(subString.contains(StringConst.EQUALS_SYMBOL)) {
//                    String[] values = subString.split(StringConst.EQUALS_SYMBOL);
//                    if(values.length > 0) result.put(values[0], values[1]);
//                }
//            }
//        } catch (Exception e) {
//            dbg(e);
//        }
//
//        return result;
//    }

    public static synchronized String serializeMessageTree(TreeMap<String, String> src) {
        StringBuilder result = new StringBuilder();

        for (Map.Entry<String, String> entry : src.entrySet()) {
            result.append(entry.getKey()).append(StringConst.EQUALS_SYMBOL).append(entry.getValue()).append(StringConst.BACK_SLASH_N);
        }

        return result.toString();
    }


//==================================================================dbg_print

    private static void dbg(String text) {
        log.debug(text);
    }

    private static void dbg(Exception e) {
        dbg(LogUtil.buildStackTraceAsString(e));
    }

}
