package com.gnl3t.mw.util;


import com.gnl3t.mw.util.constants.StringConst;
import org.apache.log4j.Logger;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;


//TODO - need implement thread safety
public class FileUtil {
	private static final Logger log = Logger.getLogger(FileUtil.class);


	//TODO - need implement a faster solution than synchronized
	public static synchronized boolean createFile(String filePath, String fileExtension, List<String> contentLines) throws Exception {
		return createFile(filePath + StringConst.DOT_SYMBOL + fileExtension, contentLines);
	}

	//TODO - need implement a faster solution than synchronized
	public static synchronized boolean createFileWithMilliseconds(String filePath, String fileExtension, List<String> contentLines) throws Exception {
		return createFile(filePath + StringConst.UNDERSCORE_SYMBOL + System.currentTimeMillis() + StringConst.DOT_SYMBOL + fileExtension, contentLines);
	}

	private static synchronized boolean createFile(String filePath, List<String> contentLines) throws Exception {
		log.debug("filePath=" + filePath);
		Path file = Paths.get(filePath);
		Files.write(file, contentLines, StandardCharsets.UTF_8);

		return true;
	}
}
