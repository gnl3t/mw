package com.gnl3t.mw.util;


public class DataPacketValidatorUtil {

    public static boolean validate(String dataPacketString) {
        if(dataPacketString == null) return false;
        int dataPacketStringLength = dataPacketString.length();

        if(dataPacketStringLength < 8) return false;

        if(dataPacketString.substring(0,5).equalsIgnoreCase("BEGIN")) {
            if(dataPacketString.substring(dataPacketStringLength - 3, dataPacketStringLength).equalsIgnoreCase("END")) {
                return true;
            }
        }
        return false;
    }
}
