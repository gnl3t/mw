package com.gnl3t.mw.util;


import org.apache.log4j.Logger;


public class LogUtil {

    public static synchronized String buildStackTraceAsString(Exception e) {
        String stackTrace = "";
        for(StackTraceElement stackTraceElement : e.getStackTrace()) {
            stackTrace += "\n" + stackTraceElement.toString();
        }
        return stackTrace;
    }

}
