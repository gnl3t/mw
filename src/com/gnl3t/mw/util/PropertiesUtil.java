package com.gnl3t.mw.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class PropertiesUtil {

    public static Properties readConfig() throws IOException {
        String configFilePath = "./config/config.properties";
        return readConfig(configFilePath);
    }

    public static Properties readConfig(String configFilePath) throws IOException {
        //String configFilePath = "config.properties";
        //String configFilePath = "D:\\work\\home\\code\\idea\\wf\\config\\config.properties";
        //String configFilePath = "./config/config.properties";
        System.out.println("configFilePath=" + configFilePath);

        Properties properties = new Properties();
        InputStream is = new FileInputStream(configFilePath);
        properties.load(is);
//        try {
//            InputStream is = new FileInputStream(configFilePath);
//            properties.load(is);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        return properties;
    }

    public static void printConfig(Properties src) {
        src.list(System.out);
    }

//    public static void printConfig(Properties src, Logger log) {
//        src.list(System.out);
//    }

}
