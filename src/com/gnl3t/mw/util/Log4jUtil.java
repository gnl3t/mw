package com.gnl3t.mw.util;


import org.apache.log4j.Logger;


public class Log4jUtil {

    public static synchronized void logException(Logger log, Exception e) {
        log.error(("" + e) + buildStackTraceAsString(e));
    }

    private static synchronized String buildStackTraceAsString(Exception e) {
        String stackTrace = "";
        for(StackTraceElement stackTraceElement : e.getStackTrace()) {
            stackTrace += "\n" + stackTraceElement.toString();
        }
        return stackTrace;
    }

    public static synchronized void dbg(Logger log, String text) {
        log.debug(text);
    }

}
