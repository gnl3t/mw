package com.gnl3t.mw.util;


import com.gnl3t.mw.environment.Environment;

import java.awt.geom.Rectangle2D;


public class ArenaScaleUtil {

    public static int reScale(int src, Environment env) {
        return (int) (src * env.screenDensity);
    }

    public static Rectangle2D.Double reScale(Rectangle2D.Double src, Environment env) {
        src.setFrame(reScale((int) src.x, env), reScale((int) src.y, env), reScale((int) src.width, env), reScale((int) src.height, env));
//        src.setFrame(src.x, reScale((int) src.y, env), reScale((int) src.width, env), reScale((int) src.height, env));
        return src;
    }

    public static Rectangle2D.Double reScaleWithoutX(Rectangle2D.Double src, Environment env) {
//        src.setFrame(reScale((int) src.x, env), reScale((int) src.y, env), reScale((int) src.width, env), reScale((int) src.height, env));
        src.setFrame(src.x, reScale((int) src.y, env), reScale((int) src.width, env), reScale((int) src.height, env));
        return src;
    }
}
