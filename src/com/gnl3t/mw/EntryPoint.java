package com.gnl3t.mw;


import com.gnl3t.mw.arena.engine.ArenaJFrameEngine;
import com.gnl3t.mw.arena.gui.ArenaJPanel;
import com.gnl3t.mw.arena.http.client.CustomHttpClient;
import com.gnl3t.mw.arena.udp.client.UdpClient;
import com.gnl3t.mw.arena.udp.client.UdpRequestsPollerRunnable;
import com.gnl3t.mw.environment.Environment;
import com.gnl3t.mw.util.Log4jUtil;
import com.gnl3t.mw.util.PropertiesUtil;
import com.gnl3t.mw.util.RequestUtils;
import com.gnl3t.mw.util.ThreadUtil;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingDeque;


public class EntryPoint {
	private final static Logger log = Logger.getLogger(EntryPoint.class);
	private static Properties config;


	public static void main(String[] args) throws Exception {
		try {
			System.out.println("reading configuration...");
			if (args.length > 0 && args[0].equals( "-c" ) ){
				String configFilePath = args[1];
				config = PropertiesUtil.readConfig(configFilePath);
			}
			if (args.length > 0 && args[2].equals( "-L" ) ){
				String configFilePath = args[3];
				DOMConfigurator.configure(configFilePath);
			}
			if(config == null) {
				config = PropertiesUtil.readConfig();
//				DOMConfigurator.configure(Const.LOG4J_PROJECT_ABSOLUTE_PATH);
			}
			PropertiesUtil.printConfig(config);
			log.debug("starting...");

			new EntryPoint();
		} catch(Exception e) {
			Log4jUtil.logException(log, e);
			throw e;
		}
	}

	public EntryPoint() throws Exception {
		try {
			log.trace("---");
			init();
		} catch(Exception e) {
			Log4jUtil.logException(log, e);
			throw e;
		}
	}

	public void init() throws Exception {
		log.trace("---");
		try {
			Environment env = new Environment(config);

			if(env.arenaJFrameEngine.onlineFlag) {
				if(env.arenaJFrameEngine.onlineP2PFlag) {
					onlineViaP2P(env);
				} else {
					onlineViaServer(env);
				}
			} else {
				offline(env);
			}

			log.debug("started");
			while(true) {
				Thread.sleep(60000);
				log.debug("working...");

				System.gc();
			}
		} catch (Exception e) {
			Log4jUtil.logException(log, e);
			throw e;
		}
    }

	private void onlineViaP2P(Environment env) throws Exception {
		boolean receivedOtherClientAddressFlag = false;
//		String host = "localhost";
//		String host = "164.90.171.227";
		String host = ArenaJFrameEngine.localHostFlag ? "localhost" : "164.90.171.227";
//		int tcpPort = 8090;
//		int port = 9001;
		int tcpPort = env.gameControlsData.playerLeft ? 8090 : 8091;
		int port = env.gameControlsData.playerLeft ? 9001 : 9002;
		UdpClient udpClient = new UdpClient(host, port, env);
		try {
			udpClient.punchHole(tcpPort);
			udpClient.connect();
			udpClient.sendMessage("BEGINplayerLeft\n" +
					"getClientAddress\n" +
					"clientId=udpServer1\n" +
					"END");

//			while(true) {
//				String message = udpClient.getSocketMessage();
//				log.debug("message=" + message);
//			}
//			String opponentId = "udpServer1";
			String opponentId = "ClientB";
			while(!receivedOtherClientAddressFlag) {
				String opponentAddress = getOtherClientAddress(opponentId);
				if(opponentAddress == null) {
					ThreadUtil.sleep(5000);
				} else {
					receivedOtherClientAddressFlag = true;
				}
			}


			udpClient.startServer();
		} catch (IOException e) {
			Log4jUtil.logException(log, e);
		}
    }

	private String getOtherClientAddress(String opponentId) throws Exception {
		String response = httpRequestOtherClientAddress();
		Map<String, String> resultMap = RequestUtils.getMap(response);

		return resultMap.get(opponentId);
    }

	private String httpRequestOtherClientAddress() throws Exception {
		String httpString = ArenaJFrameEngine.localHostFlag ? "http://localhost:8000/command" : "http://164.90.171.227:8000/command";
//        String httpString = "http://localhost:8000/command";
//        String httpString = "http://164.90.171.227:8000/command";

		CustomHttpClient customHttpClient;
		String request = "type=command\n" +
				"commandName=getClientAddresses";
		customHttpClient = new CustomHttpClient();
//        String response = customHttpClient.sendRequest("http://localhost:8000/command", request);
		return customHttpClient.sendRequest(httpString, request);
    }

	private void onlineViaServer(Environment env1) throws Exception {
		if(env1.arenaJFrameEngine.use2ClientsFlag) {
			Environment env2 = new Environment(config);
			env2.gameControlsData.playerLeft = false;
			new ArenaJPanel(env2);

			if(env2.arenaJFrameEngine.goToOnlineInstantlyFlag) {
				env2.arenaJFrameEngine.enableBotFlag = true;
				env2.arenaMenuEngine.goToArenaButtonsMenu();
			}
		}

		new ArenaJPanel(env1);
		if(env1.arenaJFrameEngine.goToOnlineInstantlyFlag) {
			env1.arenaMenuEngine.goToArenaButtonsMenu();
		}
    }

	private void offline(Environment env1) throws Exception {
		new ArenaJPanel(env1);
//		env1.arenaMenuEngine.goToArenaButtonsMenu();
//		Thread arenaJPanelThread = new Thread(new ArenaJPanel(env1));
//		arenaJPanelThread.start();
    }

}
